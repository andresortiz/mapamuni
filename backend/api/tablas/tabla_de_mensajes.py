from api.tablas import EnumWithGet

SPINNER = '<i class="fa fa-circle-o-notch fa-spin"></i>'


# === tabla de mensajes de retorno relacionados al archivo login.py ===
class MsgLogin(EnumWithGet):
    WRONG_CREDENTIALS = 'El usuario solicitado no se encuentra o la contraseña no es correcta'
    LOGIN_OK = 'Bienvenid@ {}! ' + SPINNER
    LOGIN_DB_ERROR = 'Ocurrio un error al iniciar sesión, favor inténtelo nuevamente'

    LOGIN_GOOGLE_ERROR = 'Login de Google inválido, favor inténtelo nuevamente'
    LOGIN_GOOGLE_404 = 'Bienvenid@ {}! No está registrad@ en la plataforma, favor regístrese para continuar!'
    LOGIN_GOOGLE_SUCCESS = 'Login de Google éxitoso! Bienvenid@ {}! ' + SPINNER

    LOGIN_FACEBOOK_ERROR = 'Login de Facebook inválido, favor inténtelo nuevamente'
    LOGIN_FACEBOOK_404 = LOGIN_GOOGLE_404
    LOGIN_FACEBOOK_SUCCESS = 'Login de Facebook éxitoso! Bienvenid@ {}! ' + SPINNER

    LOGOUT_OK = 'Se cerró sesión éxitosamente ' + SPINNER
    LOGOUT_DB_ERROR = 'Ocurrio un error al cerrar sesión, favor inténtelo nuevamente'

    NOT_LOGUED = 'Favor inicie sesión para continuar.'

    PASS_CHANGED = 'Contraseña cambiada éxitosamente!'
    PASS_MISMATCH = 'Contraseña actual incorrecta, no se cambió la contraseña, inténtelo nuevamente!'
    PASS_DB_ERROR = 'Ocurrió un error al cambiar contraseña, no se cambió la contraseña, inténtelo nuevamente!'
    RELOAD_OK = 'Sesion iniciada como {}!' + SPINNER
    RELOAD_RELOGIN = 'Sesión expirada, favor inicie sesión nuevamente' + SPINNER
    RELOAD_DB_ERROR = LOGIN_DB_ERROR

    REGISTER_DB_ERROR = 'Ocurrió un error al registrarse!, favor inténtelo nuevamente'
    REGISTER_FORMAT_ERROR = 'Error de formato de datos, favor contáctese con el administrador del sistema!'
    REGISTER_INVALID_NOMBRE = 'El nombre ingresado no es válido, favor inténtelo nuevamente'
    REGISTER_INVALID_EMAIL = 'La dirección de correo electrónico ingresada no es válida, favor inténtelo nuevamente'
    REGISTER_EXISTING_CI = 'La cédula ingresada ya existe en el sistema, ' \
                           'si no recuerda su contraseña, favor vaya a la seccion "Olvidé mi contraseña"'

    REGISTER_EXISTING_EMAIL = 'La dirección de correo electrónico ingresada ya existe en el sistema, ' \
                              'si no recuerda su contraseña, favor vaya a la seccion "Olvidé mi contraseña"'

    REGISTER_EXISTING_UID = 'El usuario ingresado ya existe en el sistema, ' \
                            'si no recuerda su contraseña, favor vaya a la seccion "Olvidé mi contraseña"'

    REGISTER_EXISTING_FB_ID = 'El usuario ingresado ya existe en el sistema, ' \
                              'favor vaya a "Inicio de sesion" e "Ingresar con Facebook"'
    REGISTER_INVALID_FB_ID = 'Su sesión con Facebook ya expiró, favor vuelva a iniciar sesión'

    REGISTER_EXISTING_GOOGLE_ID = 'El usuario ingresado ya existe en el sistema, ' \
                                  'favor vaya a "Inicio de sesion" e "Ingresar con Google"'
    REGISTER_INVALID_GOOGLE_ID = 'Su sesión con Google ya expiró, favor vuelva a iniciar sesión'

    REGISTER_NO_PUEDE_LOGUEAR = 'La contraseña puede estar vacía solamente si utiliza el inicio de sesión con ' \
                                'Google o Facebook'

    REGISTER_SUCCESS = 'Se registró éxitosamente en el sistema! Bienvenid@ {}!' + SPINNER

    UPDATE_PROFILE_DB_ERROR = 'Ocurrió un error al actualizar el perfil!, favor inténtelo nuevamente'

    UPDATE_PROFILE_EXISTING_CI = 'La cédula ingresada ya existe en el sistema, ' \
                                 'si no recuerda su contraseña, favor vaya a la seccion "Olvidé mi contraseña"'

    UPDATE_PROFILE_EXISTING_EMAIL = 'La dirección de correo electrónico ingresada ya existe en el sistema, ' \
                                    'si no recuerda su contraseña, favor vaya a la seccion "Olvidé mi contraseña"'

    UPDATE_PROFILE_EXISTING_UID = 'El usuario ingresado ya existe en el sistema, ' \
                                  'si no recuerda su contraseña, favor vaya a la seccion "Olvidé mi contraseña"'
    UPDATE_SUCCESS = 'Perfil actualizado con éxito! Bienvenid@ {}!' + SPINNER


# === tabla de mensajes de retorno relacionados al archivo views/misc.py ===
class MsgDatasets(EnumWithGet):
    LISTAR_DB_ERROR = 'Ocurrio un error listando los conjuntos de datos, favor inténtelo nuevamente'

    GUARDAR_DB_ERROR = 'Ocurrio un error guardando el conjuntos de datos, favor inténtelo nuevamente'
    GUARDAR_SHORT_FIELD = 'Debe ingresar {}!, favor inténtelo nuevamente'
    GUARDAR_FORMAT_ERROR = MsgLogin.REGISTER_FORMAT_ERROR
    GUARDAR_FORMATO_ERRONEO = 'El formato de {} es incorrecto, favor inténtelo nuevamente'

    AGREGAR_OK = 'El conjunto de datos se agregó éxitosamente'
    MODIFICAR_OK = 'El conjunto de datos se modificó éxitosamente'
    BORRAR_OK = 'El conjunto de datos se borró éxitosamente'

    DATASET_NOT_FOUND = 'No se encontró un conjunto de datos con ese identificador, favor inténtelo nuevamente'
    BORRAR_DB_ERROR = 'Ocurrio un error borrando el conjunto de datos, favor inténtelo nuevamente'
    DATASETS_DATA_ERROR = 'Alguno de los datos ingresados/solicitados no existe, favor actualice la pagina e ' \
                          'inténtelo nuevamente'


# === tabla de mensajes de retorno no relacionados con un archivo especifico ===
class MsgMisc(EnumWithGet):
    NO_PERMISSION = 'No tiene permisos para realizar la accion solicitada, consulte con el administrador de sistema!'
    INSUFFICIENT_ARGUMENTS = 'Argumentos Insuficientes!'
    RELOGIN = MsgLogin.RELOAD_RELOGIN


class MsgReportes(EnumWithGet):
    ERROR_DE_AUTENTICACION = 'Sesión expirada, favor inicie sesión nuevamente'
    ERROR_DE_PERMISOS = 'No tiene los permisos para realizar la accion seleccionada!'


# === tabla de mensajes de retorno relacionados al manejo de permisos ===
class MsgPermisos(EnumWithGet):
    LISTAR_PERMISOS_DB_ERROR = 'Ocurrio un error listando los permisos, favor inténtelo nuevamente'
    LISTAR_GRUPOS_DB_ERROR = 'Ocurrio un error listando los roles de usuario, favor inténtelo nuevamente'
    LISTAR_USUARIOS_DB_ERROR = 'Ocurrio un error listando los usuarios, favor inténtelo nuevamente'

    SAVE_PERMISOS_DB_ERROR = 'Ocurrio un error guardando el rol de usuario, favor inténtelo nuevamente'
    GRUPO_404_ERROR = 'El rol de usuario seleccionado no existe, favor inténtelo nuevamente'
    SAVE_PERMISOS_INVALID_NOMBRE = 'El nombre ingresado no es válido, favor inténtelo nuevamente'
    SAVE_PERMISOS_FORMAT_ERROR = 'Error de formato de datos, favor contáctese con el administrador del sistema!'
    SAVE_PERMISOS_DATA_ERROR = 'Alguno de los datos ingresados no existe, favor actualice la pagina e ' \
                               'inténtelo nuevamente'
    BORRAR_DB_ERROR = 'Ocurrio un error borrando el rol, favor inténtelo nuevamente'
    AGREGAR_OK = 'El rol "{}" se agregó éxitosamente'
    MODIFICAR_OK = 'El rol "{}" se modificó éxitosamente'
    BORRAR_OK = 'El rol "{}" se borró éxitosamente'


class MsgDenuncia(EnumWithGet):
    LISTAR_TIPO_DB_ERROR = 'Ocurrio un error listando los tipos de denuncia, favor inténtelo nuevamente'
    LISTAR_MOTIVO_DB_ERROR = 'Ocurrio un error listando los motivos de denuncia, favor inténtelo nuevamente'
    LISTAR_ESTADO_DB_ERROR = 'Ocurrio un error listando los estados de denuncia, favor inténtelo nuevamente'
    LISTAR_DENUNCIA_DB_ERROR = 'Ocurrio un error listando las denuncias, favor inténtelo nuevamente'
    LISTAR_SEGUIMIENTO_DB_ERROR = 'Ocurrio un error al cargar el seguimiento, favor inténtelo nuevamente'

    GUARDAR_DB_ERROR = 'Ocurrio un error guardando la denuncia, favor inténtelo nuevamente'

    GUARDAR_INVALID_NOMBRE = 'El nombre ingresado no es válido, favor inténtelo nuevamente'
    GUARDAR_INVALID_DESCRIPCION = 'La descripción ingresada no es válida, favor inténtelo nuevamente'
    GUARDAR_DATA_ERROR = 'Alguno de los datos ingresados no existe, favor actualice la pagina e ' \
                         'inténtelo nuevamente'
    MOTIVO_404_ERROR = 'El motivo de denuncia seleccionado no existe, favor inténtelo nuevamente'
    DENUNCIA_404_ERROR = 'La denuncia seleccionada no existe, favor inténtelo nuevamente'
    SEGUIR_DB_ERROR = 'Ocurrio un error guardando el seguimiento de la denuncia, favor inténtelo nuevamente'
    CREAR_FORMAT_ERROR = 'Error de formato de datos, favor contáctese con el administrador del sistema!'
    COMENTARIO_SHORT_FIELD = 'El comentario no puede quedar vacio, favor inténtelo nuevamente'
    SEGUIR_DENUNCIA_OK = 'Comentario agregado con éxito!'

    AGREGAR_OK = 'La denuncia se registró con éxito!'

    ERROR_CAMBIO_ESTADO = 'Ocurrió un error al cambiar el estado de la denuncia, favor inténtelo nuevamente'
    FINALIZAR_DENUNCIA_OK = 'Denuncia finalizada con exito!'
    FINALIZAR_ESTADO_INCORRECTO = 'Las denuncia solo se puede finalizar si se encuentran en estado ' \
                                  '<b>"En Proceso"</b>, estado de la denuncia: <b>{}</b>'
    # REVISAR_ESTADO_INCORRECTO = 'Las denuncia solo se puede revisar si se encuentran en estado ' \
    #                               '<b>"En Proceso"</b>, estado de la denuncia: <b>{}</b>'
    REVISAR_DENUNCIA_OK = 'Denuncia cambiada a estado revisado!'


class MsgIntervencion(EnumWithGet):
    LISTAR_TIPO_DB_ERROR = 'Ocurrio un error listando los tipos de intervención, favor inténtelo nuevamente'
    LISTAR_MOTIVO_DB_ERROR = 'Ocurrio un error listando los motivos de intervención, favor inténtelo nuevamente'
    LISTAR_ESTADO_DB_ERROR = 'Ocurrio un error listando los estados de intervención, favor inténtelo nuevamente'
    LISTAR_INTERVENCION_DB_ERROR = 'Ocurrio un error listando las intervenciones, favor inténtelo nuevamente'
    # LISTAR_SEGUIMIENTO_DB_ERROR = 'Ocurrio un error al cargar el seguimiento, favor inténtelo nuevamente'

    INTERVENCION_404_ERROR = 'No se encontró la intervención o no pertenece al usuario actual,' \
                             ' favor actualice la pagina e inténtelo nuevamente'
    GUARDAR_INVALID_NOMBRE = MsgDenuncia.GUARDAR_INVALID_NOMBRE
    GUARDAR_INVALID_DESCRIPCION = MsgDenuncia.GUARDAR_INVALID_DESCRIPCION
    CREAR_FORMAT_ERROR = MsgDenuncia.CREAR_FORMAT_ERROR
    DENUNCIA_404_ERROR = 'La denuncia preliminar seleccionada no existe, favor inténtelo nuevamente'
    MOTIVO_404_ERROR = 'El motivo de intervención seleccionado no existe, favor inténtelo nuevamente'
    GUARDAR_DB_ERROR = 'Ocurrio un error guardando la intervención, favor inténtelo nuevamente'
    GUARDAR_DATA_ERROR = MsgDenuncia.GUARDAR_DATA_ERROR
    AGREGAR_OK = 'La intervención se registró con éxito!'
    MODIFICAR_OK = 'La intervención se modificó con éxito!'
    DENUNCIA_YA_INTERVENIDA = 'La denuncia seleccionada ya fue intervenida,' \
                              ' favor actualice la página e inténtelo nuevamente'

    ERROR_CAMBIO_ESTADO = 'Ocurrió un error al cambiar el estado de la intervención, favor inténtelo nuevamente'
    FINALIZAR_INTERVENCION_OK = 'Intervencion finalizada con exito! Enhorabuena!'


class MsgUpload(EnumWithGet):
    SINGLE_UPLOAD_SUCCESSFUL = 'El archivo se guardó con éxito!'
    MULTI_UPLOAD_SUCCESSFUL = 'Los archivos se guardaron con éxito!'
    MULTI_DELETE_SUCCESSFUL = 'Los archivos se borraron con éxito!'
    SINGLE_DELETE_SUCCESSFUL = 'El archivo se borró con éxito!'


class MsgOpenData(EnumWithGet):
    GET_CITI_LIMIT_DB_ERROR = 'Error leyendo el limite de asuncion, favor inténtelo nuevamente'
    GET_STREETS_DB_ERROR = 'Error leyendo las calles de asuncion, favor inténtelo nuevamente'
    GET_BARRIOS_DB_ERROR = 'Error leyendo los barrios de asuncion, favor inténtelo nuevamente'

    LISTAR_ALL_DB_ERROR = 'Ocurrio un error listando los datos actualizables del sistema, favor inténtelo nuevamente'

    GUARDAR_TARIFADAS_DB_ERROR = 'Ocurrio un error guardando las calles, favor inténtelo nuevamente'
    GUARDAR_TARIFADAS_DATA_ERROR = 'Alguno de los datos ingresados no existe, favor actualice la pagina e ' \
                                   'inténtelo nuevamente'
    GUARDAR_TARIFADAS_OK = 'Las calles tarifadas se actualizaron con éxito!'

    GUARDAR_ARCHIVO_DB_ERROR = 'Ocurrio un error guardando el nuevo archivo en dato de sistema seleccionado, ' \
                               'favor inténtelo nuevamente'
    GUARDAR_ARCHIVO_404_INVALID = 'El archivo seleccionado no existe, no pertenece al usuario o ya ha sido ' \
                                  'asociado a otro dato, favor inténtelo nuevamente'
    GUARDAR_ARCHIVO_OK = 'El dato de sistema <b>{}</b> se ha actualizado con éxito!'

    GET_INTERVENCIONES_FOR_MAP = 'Ocurrio un error listando las intervenciones, favor inténtelo nuevamente'


class MsgDashboard(EnumWithGet):
    GET_USERS_STATS = 'Error extrayendo datos estadisticos de usuarios'
