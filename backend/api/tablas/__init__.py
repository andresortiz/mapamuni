from enum import Enum

from utils.misc import get_str_or_enum_value


class EnumWithGet(Enum):
    @property
    def str_val(self):
        return get_str_or_enum_value(self.value)
