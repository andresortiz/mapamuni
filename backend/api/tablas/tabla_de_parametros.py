from api.tablas import EnumWithGet


class ParametroCod(EnumWithGet):
    ESTADO_DENUNCIA_PENDIENTE = 'estado_denuncia_pendiente'
    ESTADO_DENUNCIA_REVISADA = 'estado_denuncia_revisada'
    ESTADO_DENUNCIA_EN_PROCESO = 'estado_denuncia_en_proceso'
    ESTADO_DENUNCIA_FINALIZADA = 'estado_denuncia_finalizada'

    ESTADO_INTERVENCION_EN_PROCESO = 'estado_intervencion_en_proceso'
    ESTADO_INTERVENCION_FINALIZADA = 'estado_intervencion_finalizada'