from api.tablas import EnumWithGet
from utils.misc import get_str_or_enum_value


def crear_notiticacion(tpl, *args, path_extra=None):
    noti = get_str_or_enum_value(tpl)
    if path_extra:
        noti[path_extra]['detail'] = noti[path_extra]['detail'].format(*args)
    else:
        noti['detail'] = noti['detail'].format(*args)
    return noti


class Notificacion(EnumWithGet):
    NUEVA_DENUNCIA = dict(reload=True, msg=dict(severity="info", summary="",
                                                detail="El usuario {} {} agregó una nueva denuncia de tipo <b>{}</b>!"))

    NUEVA_INTERVENCION = dict(reload=True, msg=dict(severity="info", summary="",
                                                    detail="El funcionario {} {} agregó una nueva intervencion de tipo <b>{}</b>!"))
