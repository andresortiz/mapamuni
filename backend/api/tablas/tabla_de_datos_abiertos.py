from api.tablas import EnumWithGet


class DatosAbiertos(EnumWithGet):
    DISTRITOS_SHP = 'distrito_shp'
    DISTRITOS_DBF = 'distrito_dbf'

    BARRIOS_SHP = 'barrios_shp'
    BARRIOS_DBF = 'barrios_dbf'

    CALLES_SHP = 'calles_shp'
    CALLES_DBF = 'calles_dbf'
    CALLES_TARIFADAS = 'calles_tarifadas'
