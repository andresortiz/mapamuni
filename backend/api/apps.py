import sys
from threading import Thread

from django.apps import AppConfig


def runmqtt():
    from api.mqtt_handler import launch_mqtt_handler
    launch_mqtt_handler()


def runmqtt_client():
    from api.mqtt_handler import setup_mqtt_client
    setup_mqtt_client()


class ApiConfig(AppConfig):
    name = 'api'

    if 'runserver' in sys.argv:
        # Timer(1, runmqtt, args=None, kwargs=None).start()
        t = Thread(target=runmqtt_client)
        t.daemon = True
        t.start()
        # Timer(1, runmqtt_client, args=None, kwargs=None).start()
