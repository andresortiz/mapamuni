"""cepbsis URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import url, include

from api.views import login, permiso, upload_file, dashboard
from api.views.opendata import opendata, mapas

urlpatterns = [
    url(r'^login$', login.login),
    url(r'^reload_login$', login.reload_login),
    url(r'^logout$', login.logout),
    url(r'^registro$', login.registro),
    url(r'^cambio_de_pass$', login.cambiar_password),
    url(r'^perfil/guardar$', login.actualizar_perfil),
    #
    url(r'^permisos/listar$', permiso.listar_permisos),
    url(r'^usuarios/listar$', permiso.listar_usuarios),
    url(r'^roles/listar$', permiso.listar_grupos),
    url(r'^roles/crear', permiso.crear_grupo),
    url(r'^roles/guardar$', permiso.guardar_permisos),
    url(r'^roles/borrar$', permiso.borrar),

    url(r'^archivo/subir$', upload_file.subir),
    url(r'^archivo/cancelar$', upload_file.borrar_no_confirmados),

    url(r'^opendata/limite/asu$', opendata.dame_limite_asuncion),
    url(r'^opendata/barrios/asu$', opendata.dame_barrios_asuncion),
    url(r'^opendata/calles/asu$', opendata.dame_calles_asuncion),
    url(r'^opendata/listar$', opendata.listar_datos_actualizables),
    url(r'^opendata/get$', opendata.get_opendata),
    url(r'^opendata/archivo/set$', opendata.set_opendata_file),
    url(r'^opendata/calles/tarifadas/set$', opendata.set_calles_tarifadas),
    url(r'^opendata/calles/tarifadas/get$', opendata.dame_calles_asuncion_tarifadas),

    url(r'^opendata/intervenciones$', mapas.listar_intervenciones),
    url(r'^opendata/denuncias', mapas.listar_denuncias),

    url(r'^dashboard/stats_usuarios$', dashboard.user_stats),
    url(r'^dashboard/stats_denuncias$', dashboard.denuncia_stats),
    url(r'^dashboard/stats_intervenciones$', dashboard.intervencion_stats),

    url(r'^subscribe_user$', login.subscribir_push),



    # url(r'^web/', include('api.views.consulta_web.urls')),
    url(r'^abml/', include('api.views.cruds.urls')),
    url(r'^reporting/', include('api.reporting.urls')),

]
