from django.core.files.base import ContentFile
from django.shortcuts import render
from django_downloadview import VirtualDownloadView
from functools import partial

from utils.misc import may_fail, get_str_or_enum_value


class TextDownloadView(VirtualDownloadView):
    texto = ''

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def get_file(self):
        """Return :class:`django.core.files.base.ContentFile` object."""
        return ContentFile(self.texto)


def mysendfile(request, data, attachment=False, attachment_filename=None,
               mimetype=None, encoding=None):
    """Port of django-sendfile's API in django-downloadview.

    Instantiates a :class:`~django_downloadview.views.path.PathDownloadView` to
    stream the file by ``filename``.

    """
    view = TextDownloadView.as_view(texto=data,
                                    attachment=attachment,
                                    basename=attachment_filename,
                                    mimetype=mimetype,
                                    encoding=encoding)
    return view(request)


def servir_reporte(request, report_data, attachment_filename, formato='json', download=False,
                   override_ext=None):
    return mysendfile(request, report_data,
                      attachment_filename='{}.{}'.format(attachment_filename, override_ext or formato),
                      attachment=download)


class ReloginException(Exception):
    pass


class PermisosException(Exception):
    pass


def render_error_page(request, msg, *args, **kwargs):
    request.user = None
    return render(request, 'error.html', {'mensaje': get_str_or_enum_value(msg).format(*args, **kwargs)},
                  content_type='text/html')


#
# def may_fail_r(*args, **kwargs):
#     from utils.misc import may_fail
#     return partial(may_fail, use_messaging=False)(*args, **kwargs)


may_fail_r = partial(may_fail, use_messaging=False)
