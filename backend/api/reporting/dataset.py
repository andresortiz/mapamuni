import csv
import json

import io
from django.db import connection
from django.views.decorators.http import require_GET
from pyzip import PyZip

from api.reporting import servir_reporte, may_fail_r, \
    PermisosException, ReloginException  # , ReloginException, may_fail_r, PermisosException
from api.tablas.tabla_de_mensajes import MsgDatasets, MsgReportes
from api.tablas.tabla_de_permisos import Permiso
from modelos.models.acceso import Usuario
from modelos.models.misc import Dataset


def get_typecodes_names(type_codes):
    with connection.cursor() as cursor2:
        cursor2.execute('SELECT oid, typname FROM pg_type WHERE oid IN %s;', [type_codes])
        type_names = dict(cursor2)
        return type_names


@require_GET
@may_fail_r((KeyError, ValueError, Dataset.DoesNotExist), MsgDatasets.DATASETS_DATA_ERROR)
@may_fail_r((ReloginException, Usuario.DoesNotExist), MsgReportes.ERROR_DE_AUTENTICACION)
@may_fail_r(PermisosException, MsgReportes.ERROR_DE_PERMISOS)
def serve_dataset(request):
    download = 'download' in request.GET and request.GET['download'] == 'true'
    formato_csv = ('formato' in request.GET and request.GET['formato'] == 'csv')
    pretty = 'pretty' in request.GET and request.GET['pretty'] == 'true'
    plain = 'plain' in request.GET and request.GET['plain'] == 'true'

    if 'user' not in request.session:
        raise ReloginException()

    if Permiso.EXPORTAR_DATASETS.str_val not in Usuario.objects.get(pk=request.session['user']).permisos:
        raise PermisosException()

    dataset = Dataset.objects.get(id=request.GET['id'])

    try:
        descripciones = dataset.nombre_mapper
        with connection.cursor() as cursor:
            cursor.execute(dataset.query)
            data = [{cursor.description[idx].name: col for idx, col in enumerate(row)} for row in cursor]
            # print(data)

            ext = 'csv' if formato_csv else 'json'
            if formato_csv:
                csvf = io.StringIO()
                fieldnames = [col.name for col in cursor.description]
                writer = csv.DictWriter(csvf, fieldnames=fieldnames)
                writer.writeheader()
                writer.writerows(data)
                content = csvf.getvalue()
            else:
                content = json.dumps(data, indent=4, sort_keys=True) if pretty else json.dumps(data)

            if not plain:
                type_codes = tuple(list({col.type_code for col in cursor.description}))
                type_names = get_typecodes_names(type_codes)
                dicc = [dict(nombre=col.name,
                             descripcion=descripciones.get(col.name, ''),
                             tipo=type_names[col.type_code])
                        for col in cursor.description]

                if formato_csv:
                    csvf = io.StringIO()
                    writer = csv.DictWriter(csvf, fieldnames=['nombre', 'descripcion', 'tipo'])
                    writer.writeheader()
                    writer.writerows(dicc)
                    dicc_content = csvf.getvalue()
                else:
                    dicc_content = json.dumps(dicc, indent=4, sort_keys=True) if pretty else json.dumps(dicc)

                pyzip = PyZip()
                pyzip['{}.{}'.format(dataset.nombre, ext)] = content.encode('utf-8')
                pyzip['DICCIONARIO.{}'.format(ext)] = dicc_content.encode('utf-8')
                return servir_reporte(request, pyzip.to_bytes(), attachment_filename=dataset.nombre,
                                      formato='zip', download=True)
            else:
                # TODO to csv if ?

                return servir_reporte(request, content, attachment_filename=dataset.nombre,
                                      formato=ext, download=download)
                # print(dicc)
    except Exception as e:
        print(e)
        raise e
