import json
import signal
import threading
import time
import traceback
from json import JSONDecodeError
from queue import Queue

# isinstance(gen, types.GeneratorType)
import functools
import paho.mqtt.client as mqtt
import re
from django.conf import settings

from api.views.opendata.get_persona_from_buscardatos import get_persona
from modelos import EntityJSONEncoder
from modelos.models.misc import Persona
from utils.misc import set_alarm_handler, Alarm

uuid_req_regex = re.compile(r'^mapamuni/([a-zA-Z0-9_-]+)/([a-zA-Z0-9_-]+)/uuid/([a-zA-Z0-9_-]+)$')
req_topic_sub = 'mapamuni/+/+/uuid/+'

client = None

# uuid_req_dispacher_queue = Queue()

handlers = dict()


def mqtt_uuid_endpoint(url):
    def decorator(func):
        handlers[url] = func

        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            set_alarm_handler()
            signal.alarm(2)
            try:
                return func(*args, **kwargs)
            except Alarm:
                print('alarm triggered')
            finally:
                signal.alarm(0)

        return wrapper

    return decorator


# def add_endpoint(reqname, func):
#     handlers[reqname] = func



def uuid_req_dispacher(queue, client):
    # global uuid_req_dispacher_queue
    # work_pool = Pool(10)

    while True:
        try:
            devid, method, uuid_r, msg = queue.get()
            if method in handlers:
                try:
                    s = msg.payload.decode('UTF-8')
                    if s != '':
                        p = json.loads(s)
                    else:
                        p = ''
                except JSONDecodeError as e:
                    print('Error', e)
                    resp = None  # TODO exc handling
                else:
                    try:
                        # work_pool
                        # TODO work queue using threads
                        resp = handlers[method](p)
                    except Exception as e:
                        print('Error', e)
                        resp = None  # TODO exc handling

                    # try:
                    r = json.dumps(resp, cls=EntityJSONEncoder)
                    print(r)
                    client.publish('mapamuni/{}/{}/uuid/{}/r'.format(devid, method, uuid_r), payload=r, qos=0,
                                   retain=False)
        except Exception as e:
            # raise e
            print('Error', e)
            traceback.print_exc()
            # handle_exception()
            # print('received!! ', str(ph) + '')


def mqtt_dispacher_tg(queue, client):
    # global client
    # global connected
    # global uuid_req_dispacher_queue
    # client = mqtt.Client()
    print("Connecting to server")

    # The callback for when the client receives a CONNACK response from the server.
    def on_connect(client, userdata, flags, rc):
        print("Connected with result code " + str(rc))
        global connected
        connected = True
        # Subscribing in on_connect() means that if we lose the connection and
        # reconnect then subscriptions will be renewed.
        client.subscribe(req_topic_sub)

    def on_disconnect(client, userdata, self):
        global connected
        connected = False

    # The callback for when a PUBLISH message is received from the server.
    def on_message(client, userdata, msg):
        print(msg.topic + " " + str(msg.payload))
        m = uuid_req_regex.match(msg.topic)
        if m:
            devid, method, uuid_r = m.groups()
            print('enqueued ', (devid, method, uuid_r, msg))
            queue.put((devid, method, uuid_r, msg))
            # m = cmd_regex.match(msg.topic)

    client.on_connect = on_connect
    client.on_message = on_message
    client.on_disconnect = on_disconnect

    client.connect(settings.MQTT_HOST, 1883, 60)

    # Blocking call that processes network traffic, dispatches callbacks and
    # handles reconnecting.
    # Other loop*() functions are available that give a threaded interface and a
    # manual interface.

    client.loop_forever()


def run_mqtt_handler():
    # print('mqtt')
    client = mqtt.Client(clean_session=True)
    queue = Queue()
    t = threading.Thread(target=mqtt_dispacher_tg, args=(queue, client))
    t.daemon = True
    t.start()
    uuid_req_dispacher(queue, client)


def launch_mqtt_handler():
    # print('mqtt')
    client = mqtt.Client(clean_session=True)
    queue = Queue()
    t = threading.Thread(target=mqtt_dispacher_tg, args=(queue, client))
    t.daemon = True
    t.start()
    t = threading.Thread(target=uuid_req_dispacher, args=(queue, client))
    t.daemon = True
    t.start()


    # add_endpoint('get-persona', hola)
    #
    # add_endpoint('get-personas', getpersonas)


@mqtt_uuid_endpoint('get-persona')
def getpersona(ci):
    return get_persona(ci)


@mqtt_uuid_endpoint('sleep')
def sleep_end(number):
    time.sleep(float(number))
    return 'good morning!'


@mqtt_uuid_endpoint('get-personas')
def getpersonas(ci):
    # print('get_personas', ci)
    ci = str(ci)
    if len(ci) >= 1:
        q = Persona.objects.filter(ci__startswith=ci)[:5]
        # print(q.query)
        return q
    else:
        return []


def try_to_pub_noti_to_rt(topic, msg):
    global client
    try:
        client.publish('mapamuni/notificaciones/' + topic, json.dumps(msg))
    except Exception as e:
        print(e)


def try_to_pub_to_rt(topic, msg):
    global client
    try:
        client.publish(topic, json.dumps(msg))
    except Exception as e:
        print(e)


def setup_mqtt_client():
    global client
    client = mqtt.Client(clean_session=True)
    print("Connecting to server")

    # The callback for when the client receives a CONNACK response from the server.
    def on_connect(client, userdata, flags, rc):
        print("Connected with result code " + str(rc))
        global connected
        connected = True
        # Subscribing in on_connect() means that if we lose the connection and
        # reconnect then subscriptions will be renewed.
        # client.subscribe(req_topic_sub)

    def on_disconnect(client, userdata, self):
        global connected
        connected = False

    # The callback for when a PUBLISH message is received from the server.
    def on_message(client, userdata, msg):
        pass
        # print(msg.topic + " " + str(msg.payload))
        # m = uuid_req_regex.match(msg.topic)
        # if m:
        #     devid, method, uuid_r = m.groups()
        #     print('enqueued ', (devid, method, uuid_r, msg))
        #     queue.put((devid, method, uuid_r, msg))
        #     # m = cmd_regex.match(msg.topic)

    client.on_connect = on_connect
    client.on_message = on_message
    client.on_disconnect = on_disconnect

    client.connect(settings.MQTT_HOST, 1883, 60)

    # Blocking call that processes network traffic, dispatches callbacks and
    # handles reconnecting.
    # Other loop*() functions are available that give a threaded interface and a
    # manual interface.

    client.loop_forever()
