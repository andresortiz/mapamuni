import datetime

from django.db import DatabaseError
from django.views.decorators.http import require_GET

from api.tablas.tabla_de_mensajes import MsgOpenData
from modelos.models.denuncia import Denuncia
from modelos.models.intervencion import Intervencion
from utils.data_decorators import requires_permission_and_login
from utils.messaging import process_messaging
from utils.misc import may_fail


@require_GET
@process_messaging
@requires_permission_and_login(result=[])
@may_fail(DatabaseError, MsgOpenData.GET_INTERVENCIONES_FOR_MAP, result=[])
# === listar tipo denuncia ===
def listar_intervenciones(request):
    """
**param**

* request: ningun parámetro

**return**

* retorna [dataset] + mensajes, ver **utils.messaging** para formato general de retorno

 <a href="/index.html">volver a la raiz</a>
    """
    intervenciones = Intervencion.objects
    intervenciones = intervenciones.filter(
        interventor_id=request.usuario.id) if (
        'mias' in request.GET and request.GET['mias'] == 'true') else intervenciones
    intervenciones = intervenciones.filter(
        motivo_id=request.GET['id_motivo']) if 'id_motivo' in request.GET else intervenciones
    intervenciones = intervenciones.filter(
        motivo__tipo_id=request.GET['id_tipo']) if 'id_tipo' in request.GET else intervenciones
    intervenciones = intervenciones.filter(
        estado_id=request.GET['id_estado']) if 'id_estado' in request.GET else intervenciones
    if 'from' in request.GET:
        intervenciones = intervenciones.filter(
            fecha_creacion__gte=datetime.datetime.strptime(request.GET['from'], '%d-%m-%Y').date())
    if 'to' in request.GET:
        intervenciones = intervenciones.filter(
            fecha_creacion__lte=datetime.datetime.strptime(request.GET['to'], '%d-%m-%Y').date())

    return [dict(coord=[i.lon, i.lat], id=i.id, n=i.nombre, t=i.motivo.id) for i in
            intervenciones.filter(lat__isnull=False, lon__isnull=False).all()]


@require_GET
@process_messaging
@requires_permission_and_login(result=[])
@may_fail(DatabaseError, MsgOpenData.GET_INTERVENCIONES_FOR_MAP, result=[])
# === listar tipo denuncia ===
def listar_denuncias(request):
    """
**param**

* request: ningun parámetro

**return**

* retorna [dataset] + mensajes, ver **utils.messaging** para formato general de retorno

 <a href="/index.html">volver a la raiz</a>
    """
    denuncias = Denuncia.objects
    denuncias = denuncias.filter(denunciante_id=request.usuario.id) if (
        'mias' in request.GET and request.GET['mias'] == 'true') else denuncias
    denuncias = denuncias.filter(motivo_id=request.GET['id_motivo']) if 'id_motivo' in request.GET else denuncias
    denuncias = denuncias.filter(motivo__tipo_id=request.GET['id_tipo']) if 'id_tipo' in request.GET else denuncias
    denuncias = denuncias.filter(estado_id=request.GET['id_estado']) if 'id_estado' in request.GET else denuncias
    if 'from' in request.GET:
        denuncias = denuncias.filter(
            fecha_creacion__gte=datetime.datetime.strptime(request.GET['from'], '%d-%m-%Y').date())
    if 'to' in request.GET:
        denuncias = denuncias.filter(
            fecha_creacion__lte=datetime.datetime.strptime(request.GET['to'], '%d-%m-%Y').date())

    return [dict(coord=[i.lon, i.lat], id=i.id, n=i.nombre, m=i.motivo.id) for i in
            denuncias.filter(lat__isnull=False, lon__isnull=False).all()]
