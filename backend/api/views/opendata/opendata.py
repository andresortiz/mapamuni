import json
from json import JSONDecodeError

import geopandas
import io
import shapefile
from django.db import DatabaseError, transaction
from django.views.decorators.http import require_GET, require_POST

from api.tablas.tabla_de_datos_abiertos import DatosAbiertos
from api.tablas.tabla_de_mensajes import MsgOpenData
from api.tablas.tabla_de_permisos import Permiso
from modelos.models.misc import DatoAbiertoSistema, Archivo
from utils.data_decorators import requires_permission_and_login
from utils.messaging import process_messaging, process_messaging_json_input
from utils.misc import may_fail, get_str_or_enum_value
from utils.misc_decorators import check_for_json_args


def get_geodataframe_from_opendata_db(param_shp, param_dbf,
                                      crs='+proj=utm +zone=21 +south +datum=WGS84 +units=m +no_defs', to_epsg=4326,
                                      atr_transform_func=None, add_auto_ids=False):
    shp = io.BytesIO(DatoAbiertoSistema.get_val(param_shp))
    dbf = io.BytesIO(DatoAbiertoSistema.get_val(param_dbf))
    reader = shapefile.Reader(shp=shp, dbf=dbf)
    fields = reader.fields[1:]
    field_names = [field[0] for field in fields]
    buffer = []
    auto_id = 0
    for sr in reader.shapeRecords():
        atr = dict(zip(field_names, [s.decode('latin-1') if isinstance(s, bytes) else s for s in sr.record]))
        geom = sr.shape.__geo_interface__
        if add_auto_ids:
            atr['id'] = auto_id
            auto_id += 1
        if atr_transform_func and callable(atr_transform_func):
            atr = atr_transform_func(atr)
        buffer.append(dict(type="Feature", geometry=geom, properties=atr))
    geodf = geopandas.GeoDataFrame.from_features(buffer)
    if crs:
        geodf.crs = crs
    return geodf.to_crs(epsg=to_epsg) if to_epsg else geodf


@require_GET
@process_messaging
#@requires_permission_and_login
@may_fail(DatabaseError, MsgOpenData.GET_CITI_LIMIT_DB_ERROR, result=[])
# === listar tipo denuncia ===
def dame_limite_asuncion(request):
    """
**param**

* request: ningun parámetro

**return**

* retorna [dataset] + mensajes, ver **utils.messaging** para formato general de retorno

 <a href="/index.html">volver a la raiz</a>
    """
    muni = get_geodataframe_from_opendata_db(DatosAbiertos.DISTRITOS_SHP, DatosAbiertos.DISTRITOS_DBF).unary_union
    # print(muni.__geo_interface__)
    return dict(proj='EPSG:4326',
                feature=dict(type="Feature", geometry=muni.__geo_interface__, properties={}))


@require_GET
@process_messaging
@requires_permission_and_login(result=dict(proj='EPSG:4326', features=[]))
@may_fail(DatabaseError, MsgOpenData.GET_BARRIOS_DB_ERROR, result=[])
# === listar tipo denuncia ===
def dame_barrios_asuncion(request):
    """
**param**

* request: ningun parámetro

**return**

* retorna [dataset] + mensajes, ver **utils.messaging** para formato general de retorno

 <a href="/index.html">volver a la raiz</a>
    """
    barrios = get_geodataframe_from_opendata_db(DatosAbiertos.BARRIOS_SHP, DatosAbiertos.BARRIOS_DBF,
                                                atr_transform_func=lambda atr: dict(nombre=atr['nombre']))
    # save_obj_to_json_file('opendata.geojson', list(barrios.iterfeatures()))
    return dict(proj='EPSG:4326', features=list(barrios.iterfeatures()))


@require_GET
@process_messaging
@requires_permission_and_login(result=dict(proj='EPSG:4326', features=[]))
@may_fail(DatabaseError, MsgOpenData.GET_STREETS_DB_ERROR, result=[])
# === listar tipo denuncia ===
def dame_calles_asuncion(request):
    """
**param**

* request: ningun parámetro

**return**

* retorna [dataset] + mensajes, ver **utils.messaging** para formato general de retorno

 <a href="/index.html">volver a la raiz</a>
    """
    calles = get_geodataframe_from_opendata_db(DatosAbiertos.CALLES_SHP,
                                               DatosAbiertos.CALLES_DBF,
                                               atr_transform_func=lambda atr: dict(nombre=atr['nombre'], id=atr['id']),
                                               add_auto_ids=True)
    # save_obj_to_json_file('opendata.geojson', list(calles.iterfeatures()))
    return dict(proj='EPSG:4326', features=list(calles.iterfeatures()))


@require_GET
@process_messaging
@requires_permission_and_login(result=dict(proj='EPSG:4326', features=[]))
@may_fail(DatabaseError, MsgOpenData.GET_STREETS_DB_ERROR, result=[])
# === listar tipo denuncia ===
def dame_calles_asuncion_tarifadas(request):
    """
**param**

* request: ningun parámetro

**return**

* retorna [dataset] + mensajes, ver **utils.messaging** para formato general de retorno

 <a href="/index.html">volver a la raiz</a>
    """
    calles = get_geodataframe_from_opendata_db(DatosAbiertos.CALLES_SHP,
                                               DatosAbiertos.CALLES_DBF,
                                               atr_transform_func=lambda atr: dict(nombre=atr['nombre'], id=atr['id']),
                                               add_auto_ids=True)
    ids = DatoAbiertoSistema.get_val(DatosAbiertos.CALLES_TARIFADAS)
    # print(list(calles.iterfeatures())[0])
    features = [f for f in calles.iterfeatures() if f['properties']['id'] in ids]
    # save_obj_to_json_file('opendata.geojson', list(calles.iterfeatures()))
    return dict(proj='EPSG:4326', features=features)


@require_GET
@process_messaging
@requires_permission_and_login
@may_fail(DatabaseError, MsgOpenData.LISTAR_ALL_DB_ERROR, result=[])
# === listar denuncias propias ===
def get_opendata(request):
    """
**param**

* request: ningun parámetro

**return**

* retorna [dataset] + mensajes, ver **utils.messaging** para formato general de retorno

 <a href="/index.html">volver a la raiz</a>
    """
    odata = DatoAbiertoSistema.objects
    odata = odata.filter(id=request.GET['id']) if 'id' in request.GET else odata
    odata = odata.filter(codigo=request.GET['codigo']) if 'codigo' in request.GET else odata
    data = odata.get().value
    if isinstance(data, memoryview):
        bio = io.BytesIO(data)
        txtval = bio.getvalue().decode('UTF-8')
        try:
            return json.loads(txtval)
        except JSONDecodeError:
            return txtval
    return data


@require_GET
@process_messaging
@requires_permission_and_login(Permiso.UPLOAD_OPENDATA, result=[])
@may_fail(DatabaseError, MsgOpenData.LISTAR_ALL_DB_ERROR, result=[])
# === listar denuncias propias ===
def listar_datos_actualizables(request):
    """
**param**

* request: ningun parámetro

**return**

* retorna [dataset] + mensajes, ver **utils.messaging** para formato general de retorno

 <a href="/index.html">volver a la raiz</a>
    """
    return DatoAbiertoSistema.objects.filter(subible=True).all()


@require_POST
@process_messaging_json_input
@requires_permission_and_login(Permiso.SET_CALLES_TARIFADAS)
@check_for_json_args('ids')
@may_fail(DatabaseError, MsgOpenData.GUARDAR_TARIFADAS_DB_ERROR)
@may_fail((AttributeError,), MsgOpenData.GUARDAR_TARIFADAS_DATA_ERROR)
def set_calles_tarifadas(request, body):
    """
**param**

* request: acepta parametros (** uid[str], password[str(pass hasheado a md5)] ** )

**return**

* retorna success true/false, y en result un mapa {nombre:str, permisos:[str,...]}, ver **utils.messaging** para formato general de retorno,


 <a href="/index.html">volver a la raiz</a>
    """
    if not isinstance(body.ids, list):
        return request.fail_with_message(MsgOpenData.GUARDAR_TARIFADAS_DATA_ERROR)

    body.ids = set(body.ids)  # elimina repetiods

    # TODO cruzar con calles

    odato = DatoAbiertoSistema.objects.filter(codigo=get_str_or_enum_value(DatosAbiertos.CALLES_TARIFADAS)).get()

    odato.json_data = body.ids
    odato.save()

    return request.success_with_message(MsgOpenData.GUARDAR_TARIFADAS_OK)


@require_POST
@process_messaging_json_input
@requires_permission_and_login(Permiso.UPLOAD_OPENDATA)
@check_for_json_args('codigo', 'id')
@may_fail(DatabaseError, MsgOpenData.GUARDAR_ARCHIVO_DB_ERROR)
@may_fail(Archivo.DoesNotExist, MsgOpenData.GUARDAR_ARCHIVO_404_INVALID)
@transaction.atomic
def set_opendata_file(request, body):
    """
**param**

* request: acepta parametros (** uid[str], password[str(pass hasheado a md5)] ** )

**return**

* retorna success true/false, y en result un mapa {nombre:str, permisos:[str,...]}, ver **utils.messaging** para formato general de retorno,


 <a href="/index.html">volver a la raiz</a>
    """
    odato = DatoAbiertoSistema.objects.filter(codigo=body.codigo, subible=True).get()

    archivo = Archivo.objects.filter(id=body.id, owner_id=request.usuario.id, confirmado=False).get()

    odato.json_data = None
    odato.archivo = archivo
    odato.save()

    archivo.confirmado = True
    archivo.save()

    return request.success_with_message(MsgOpenData.GUARDAR_ARCHIVO_OK, odato.nombre)
