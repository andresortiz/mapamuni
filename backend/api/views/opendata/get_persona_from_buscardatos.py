import requests
from bs4 import BeautifulSoup

url = 'http://www.buscardatos.com/py/personas/padron_cedula_paraguay.php'

p_keys = ['ci', 'fullname', 'sexo', 'fecha_nac']


def get_persona(ci):
    php_text = requests.post(url, data=dict(cedula=ci)).text
    soup = BeautifulSoup(php_text, 'html.parser')
    datos_i = soup.table.find_all('i')
    print(str(datos_i[3].text))
    return {k: tag.text.replace('\\', '-') for k, tag in zip(p_keys, datos_i)}
