import datetime

from django.db import DatabaseError
from django.views.decorators.http import require_GET

from api.tablas.tabla_de_mensajes import MsgDashboard
from modelos.models.acceso import Usuario
from modelos.models.denuncia import EstadoDenuncia, Denuncia, TipoDenuncia
from modelos.models.intervencion import EstadoIntervencion, TipoIntervencion, Intervencion
from utils.data_decorators import requires_permission_and_login
from utils.messaging import process_messaging
from utils.misc import may_fail


@require_GET
@process_messaging
@requires_permission_and_login
@may_fail(DatabaseError, MsgDashboard.GET_USERS_STATS, result=[])
# === listar tipo denuncia ===
def user_stats(request):
    """
**param**

* request: ningun parámetro

**return**

* retorna [dataset] + mensajes, ver **utils.messaging** para formato general de retorno

 <a href="/index.html">volver a la raiz</a>
    """
    today = datetime.datetime.today()
    this_week = today.isocalendar()[1]
    return dict(registered=dict(
        today=Usuario.objects.filter(fecha_registro__day=today.day, fecha_registro__month=today.month,
                                     fecha_registro__year=today.year).count(),
        this_week=Usuario.objects.filter(fecha_registro__week=this_week).count(),
        this_month=Usuario.objects.filter(fecha_registro__month=today.month, fecha_registro__year=today.year).count(),
        this_year=Usuario.objects.filter(fecha_registro__year=today.year).count(),
        overall=Usuario.objects.count()
    ), logged=dict(
        today=Usuario.objects.filter(ultimo_acceso__day=today.day, ultimo_acceso__month=today.month,
                                     ultimo_acceso__year=today.year).count(),
        this_week=Usuario.objects.filter(ultimo_acceso__week=this_week).count(),
        this_month=Usuario.objects.filter(ultimo_acceso__month=today.month, ultimo_acceso__year=today.year).count(),
        this_year=Usuario.objects.filter(ultimo_acceso__year=today.year).count(),
        overall=Usuario.objects.filter(ultimo_acceso__isnull=False).count()
    ))


@require_GET
@process_messaging
# @requires_permission_and_login
@may_fail(DatabaseError, MsgDashboard.GET_USERS_STATS, result=[])
# === listar tipo denuncia ===
def denuncia_stats(request):
    """
**param**

* request: ningun parámetro

**return**

* retorna [dataset] + mensajes, ver **utils.messaging** para formato general de retorno

 <a href="/index.html">volver a la raiz</a>
    """
    today = datetime.datetime.today()
    this_week = today.isocalendar()[1]
    estados = EstadoDenuncia.objects.all()
    tipos = TipoDenuncia.objects.all()

    return dict(tipos=[t.nombre for t in tipos],
                labels=[[e.nombre for e in estados] for t in tipos],
                data=[dict(tipo=t, creadas=[dict(estado=e, stats=dict(
                    today=Denuncia.objects.filter(motivo__tipo=t, estado=e, fecha_creacion__day=today.day,
                                                  fecha_creacion__month=today.month,
                                                  fecha_creacion__year=today.year).count(),
                    this_week=Denuncia.objects.filter(motivo__tipo=t, estado=e, fecha_creacion__week=this_week).count(),
                    this_month=Denuncia.objects.filter(motivo__tipo=t, estado=e, fecha_creacion__month=today.month,
                                                       fecha_creacion__year=today.year).count(),
                    this_year=Denuncia.objects.filter(motivo__tipo=t, estado=e,
                                                      fecha_creacion__year=today.year).count(),
                    overall=Denuncia.objects.filter(motivo__tipo=t, estado=e).count())) for e in estados]) for t in
                      tipos])

    # return [dict(tipo=t, creadas=dict(
    #     today=[(e, Denuncia.objects.filter(estado=e, fecha_creacion__day=today.day, fecha_creacion__month=today.month,
    #                                        fecha_creacion__year=today.year).count()) for e in estados],
    #     this_week=[(e, Denuncia.objects.filter(estado=e, fecha_creacion__week=this_week).count()) for e in estados],
    #     this_month=[
    #         (e, Denuncia.objects.filter(estado=e, fecha_creacion__month=today.month,
    #                                     fecha_creacion__year=today.year).count()) for e in estados],
    #     this_year=[
    #         (e, Denuncia.objects.filter(estado=e, fecha_creacion__year=today.year).count()) for e in estados],
    #     overall=[(e, Denuncia.objects.filter(estado=e).count()) for e in estados])) for t in tipos]

    # )), finalizadas=dict(
    #     today=Denuncia.objects.filter(fecha_finalizacion__day=today.day, fecha_finalizacion__month=today.month,
    #                                   fecha_finalizacion__year=today.year).count(),
    #     this_week=Denuncia.objects.filter(fecha_creacion__week=this_week).count(),
    #     this_month=Denuncia.objects.filter(fecha_finalizacion__month=today.month,
    #                                        fecha_finalizacion__year=today.year).count(),
    #     this_year=Denuncia.objects.filter(fecha_finalizacion__year=today.year).count(),
    #     overall=Denuncia.objects.filter(fecha_finalizacion__isnull=False).count(),
    # ))


@require_GET
@process_messaging
# @requires_permission_and_login
@may_fail(DatabaseError, MsgDashboard.GET_USERS_STATS, result=[])
# === listar tipo denuncia ===
def intervencion_stats(request):
    """
**param**

* request: ningun parámetro

**return**

* retorna [dataset] + mensajes, ver **utils.messaging** para formato general de retorno

 <a href="/index.html">volver a la raiz</a>
    """
    today = datetime.datetime.today()
    this_week = today.isocalendar()[1]
    estados = EstadoIntervencion.objects.all()
    tipos = TipoIntervencion.objects.all()

    return dict(tipos=[t.nombre for t in tipos],
                labels=[[e.nombre for e in estados] for _ in tipos],
                data=[dict(tipo=t, creadas=[dict(estado=e, stats=dict(
                    today=Intervencion.objects.filter(motivo__tipo=t, estado=e, fecha_creacion__day=today.day,
                                                      fecha_creacion__month=today.month,
                                                      fecha_creacion__year=today.year).count(),
                    this_week=Intervencion.objects.filter(motivo__tipo=t, estado=e,
                                                          fecha_creacion__week=this_week).count(),
                    this_month=Intervencion.objects.filter(motivo__tipo=t, estado=e, fecha_creacion__month=today.month,
                                                           fecha_creacion__year=today.year).count(),
                    this_year=Intervencion.objects.filter(motivo__tipo=t, estado=e,
                                                          fecha_creacion__year=today.year).count(),
                    overall=Intervencion.objects.filter(motivo__tipo=t, estado=e).count())) for e in estados]) for t in
                      tipos])
