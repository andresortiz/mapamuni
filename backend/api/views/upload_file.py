import datetime

from django.views.decorators.http import require_POST
from io import BytesIO

from api.tablas.tabla_de_mensajes import MsgUpload
from modelos.models.misc import Archivo
from utils.data_decorators import requires_permission_and_login
from utils.messaging import process_messaging_json_input, process_messaging
from utils.misc_decorators import check_for_json_args


@require_POST
@process_messaging
@requires_permission_and_login
def subir(request):
    subidos = []
    if 'archivos' in request.FILES:
        for archivo in request.FILES.getlist('archivos'):
            print(archivo, type(archivo))
            reader = BytesIO()
            for chunk in archivo.chunks():
                reader.write(chunk)
            a = Archivo.objects.create(
                nombre=archivo.name,
                subido_en=datetime.datetime.now(),
                owner_id=request.usuario.id,
                datos=reader.getvalue()
            )
            reader.close()
            subidos.append(a.id)
            print(subidos)
        return request.success_with_message(
            MsgUpload.MULTI_UPLOAD_SUCCESSFUL if len(subidos) > 1 else MsgUpload.SINGLE_UPLOAD_SUCCESSFUL,
            result=subidos)


@require_POST
@process_messaging_json_input
@requires_permission_and_login
@check_for_json_args('ids')
def borrar_no_confirmados(request, body):
    if isinstance(body.ids, list):
        body.ids = set(body.ids)  # elimina repetiods

        q = Archivo.objects.filter(id__in=body.ids, owner=request.usuario, confirmado=False)
        if len(body.ids) == q.count():
            q.delete()
            print('delete successfull')

        return request.success_with_message(
            MsgUpload.MULTI_DELETE_SUCCESSFUL if len(body.ids) > 1 else MsgUpload.SINGLE_DELETE_SUCCESSFUL)

    return request.success_with_message(MsgUpload.MULTI_DELETE_SUCCESSFUL)
