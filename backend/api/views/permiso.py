import datetime

from django.db import DatabaseError, transaction
from django.views.decorators.http import require_GET, require_POST

from api.tablas.tabla_de_mensajes import MsgPermisos
from api.tablas.tabla_de_permisos import Permiso as PermisoT
from modelos.models.acceso import Permiso, GrupoUsuario, Usuario
from utils.data_decorators import requires_permission_and_login
from utils.messaging import process_messaging_json_input, process_messaging
from utils.misc import may_fail
from utils.misc_decorators import check_for_json_args


@require_GET
@process_messaging
@requires_permission_and_login(either=[PermisoT.EDITAR_PERMISOS, PermisoT.VER_PERMISOS], result=[])
@may_fail(DatabaseError, MsgPermisos.LISTAR_PERMISOS_DB_ERROR, result=[])
# === listar datasets ===
def listar_permisos(*args):
    """
**param**

* request: ningun parámetro

**return**

* retorna [dataset] + mensajes, ver **utils.messaging** para formato general de retorno

 <a href="/index.html">volver a la raiz</a>
    """
    roots = Permiso.objects.filter(padre__isnull=True).all()
    return [r.prime_json for r in roots]


@require_GET
@process_messaging
@requires_permission_and_login(either=[PermisoT.EDITAR_PERMISOS, PermisoT.VER_PERMISOS], result=[])
@may_fail(DatabaseError, MsgPermisos.LISTAR_GRUPOS_DB_ERROR, result=[])
# === listar datasets ===
def listar_grupos(*args):
    """
**param**

* request: ningun parámetro

**return**

* retorna [dataset] + mensajes, ver **utils.messaging** para formato general de retorno

 <a href="/index.html">volver a la raiz</a>
    """
    return [g.json_for_edit for g in GrupoUsuario.objects.all()]


@require_GET
@process_messaging
@requires_permission_and_login(either=[PermisoT.EDITAR_PERMISOS, PermisoT.VER_PERMISOS], result=[])
@may_fail(DatabaseError, MsgPermisos.LISTAR_USUARIOS_DB_ERROR, result=[])
# === listar datasets ===
def listar_usuarios(*args):
    """
**param**

* request: ningun parámetro

**return**

* retorna [dataset] + mensajes, ver **utils.messaging** para formato general de retorno

 <a href="/index.html">volver a la raiz</a>
    """
    return [u.json_for_edit for u in Usuario.objects.all()]


# === guardar grupos ====
@require_POST
@process_messaging_json_input
@requires_permission_and_login(PermisoT.CREAR_ROLES)
@check_for_json_args('nombre', 'descripcion')
@may_fail(DatabaseError, MsgPermisos.SAVE_PERMISOS_DB_ERROR)
def crear_grupo(request, body):
    """
**param**

* request: acepta parametros (** uid[str], password[str(pass hasheado a md5)] ** )

**return**

* retorna success true/false, y en result un mapa {nombre:str, permisos:[str,...]}, ver **utils.messaging** para formato general de retorno,


 <a href="/index.html">volver a la raiz</a>
    """
    grupo = GrupoUsuario()

    for attr in ['nombre', 'descripcion']:
        setattr(grupo, attr, getattr(body, attr).strip())  # copia de body a grupo

    if len(grupo.nombre) < 3:
        return request.fail_with_message(MsgPermisos.SAVE_PERMISOS_INVALID_NOMBRE)

    grupo.ultima_modificacion = datetime.datetime.now()
    grupo.save()

    return request.success_with_message(MsgPermisos.AGREGAR_OK, grupo.nombre)


# === guardar grupos ====
@require_POST
@process_messaging_json_input
@requires_permission_and_login(PermisoT.EDITAR_PERMISOS)
@check_for_json_args('id', 'usuarios', 'permisos')
@may_fail(DatabaseError, MsgPermisos.SAVE_PERMISOS_DB_ERROR)
@may_fail(GrupoUsuario.DoesNotExist, MsgPermisos.GRUPO_404_ERROR)
# @may_fail((AttributeError, decimal.InvalidOperation, RegisterFormatError), MsgLogin.REGISTER_FORMAT_ERROR)
@transaction.atomic
def guardar_permisos(request, body):
    """
**param**

* request: acepta parametros (** uid[str], password[str(pass hasheado a md5)] ** )

**return**

* retorna success true/false, y en result un mapa {nombre:str, permisos:[str,...]}, ver **utils.messaging** para formato general de retorno,


 <a href="/index.html">volver a la raiz</a>
    """
    # si modificar busca el grupo, si nuevo crea nuevo grupo
    grupo = GrupoUsuario.objects.get(pk=body.id)

    if not isinstance(body.permisos, list) or not isinstance(body.usuarios, list):
        return request.fail_with_message(MsgPermisos.SAVE_PERMISOS_FORMAT_ERROR)

    body.permisos = set(body.permisos)  # elimina repetiods
    body.usuarios = set(body.usuarios)  # elimina repetiods

    qP = Permiso.objects.filter(id__in=body.permisos)
    if len(body.permisos) != qP.count():
        return request.fail_with_message(MsgPermisos.SAVE_PERMISOS_DATA_ERROR)  # existen todos los ids?

    qU = Usuario.objects.filter(id__in=body.usuarios)
    if len(body.usuarios) != qU.count():
        return request.fail_with_message(MsgPermisos.SAVE_PERMISOS_DATA_ERROR)  # existen todos los ids?

    # TODO falta validar que no eliminen el permiso de editar permisos
    # permiso_critico = Permiso.objects.filter(codigo=TPermiso.EDITAR_PERMISOS).get()
    # grupos = permiso_critico.grupos.all()

    grupo.ultima_modificacion = datetime.datetime.now()
    grupo.save()

    grupo.permisos.set(list(qP.all()))
    grupo.usuarios.set(list(qU.all()))

    return request.success_with_message(MsgPermisos.MODIFICAR_OK, grupo.nombre)


# === borrar caja ===
@require_POST
@process_messaging_json_input
@requires_permission_and_login(PermisoT.BORRAR_ROLES)
@check_for_json_args('id')
@may_fail(DatabaseError, MsgPermisos.BORRAR_DB_ERROR)
@may_fail(GrupoUsuario.DoesNotExist, MsgPermisos.GRUPO_404_ERROR)
def borrar(request, body):
    """
**param**

* request: acepta parametro  (** id[int] ** )

**return**

* retorna success true/false + mensajes, ver **utils.messaging** para formato general de retorno

 <a href="/index.html">volver a la raiz</a>
    """
    # TODO falta validar que no eliminen el permiso de editar permisos

    GrupoUsuario.objects.get(pk=body.id).delete()
    return request.success_with_message(MsgPermisos.BORRAR_OK)
