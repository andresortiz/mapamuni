import datetime
import decimal

from django.db import DatabaseError
from django.db.models import Q
from django.views.decorators.http import require_POST

from api.tablas.tabla_de_mensajes import MsgLogin  # , MsgAperturaCierreCaja
from api.views import RegisterFormatError
from modelos.models.acceso import Usuario
from modelos.models.misc import PushSub
from utils.data_decorators import requires_permission_and_login
from utils.facebook_oauth import get_facebook_info_from_token
from utils.google_oauth import get_google_info_from_token
from utils.hash_checker import HashChecker
from utils.mailgun import send_message_bienvenida
from utils.messaging import process_messaging_json_input
from utils.misc import may_fail, validateEmail, get_json_from_obj
# === login ====
from utils.misc_decorators import check_for_json_args


@require_POST
@process_messaging_json_input
@may_fail(DatabaseError, MsgLogin.LOGIN_DB_ERROR)
@may_fail(Usuario.DoesNotExist, MsgLogin.WRONG_CREDENTIALS)
def login(request, body):
    """
**param**

* request: acepta parametros (** uid[str], password[str(pass hasheado a md5)] ** )

**return**

* retorna success true/false, y en result un mapa {nombre:str, permisos:[str,...]}, ver **utils.messaging** para formato general de retorno,


 <a href="/index.html">volver a la raiz</a>
    """
    msg = MsgLogin.LOGIN_OK
    user = None
    if 'uid' in body and 'password' in body:
        uid = body.uid.lower()
        passw = body.password

        user = Usuario.objects.filter(Q(user_id=uid) | Q(email=uid), password=passw).get()
        request.session['user'] = user.pk
        request.session['up_login'] = True
        msg = MsgLogin.LOGIN_OK
    elif 'token_fb' in body:
        print('fb token! ', body.token_fb)
        info = get_facebook_info_from_token(body.token_fb)
        # print(info)
        if info is None:
            return request.fail_with_message(MsgLogin.LOGIN_FACEBOOK_ERROR, result=None)
        else:
            try:
                user = Usuario.objects.filter(fb_id=info['uid']).get()
            except Usuario.DoesNotExist:
                info['token_fb'] = body.token_fb
                info['token_gg'] = None
                return request.fail_with_message(MsgLogin.LOGIN_FACEBOOK_404, info['nombre'], result=dict(fb=info))
            request.session['user'] = user.pk
            request.session['up_login'] = False
            request.session['token_fb'] = body.token_fb
            msg = MsgLogin.LOGIN_FACEBOOK_SUCCESS
    elif 'token_gg' in body:
        print('google token! ', body.token_gg)
        info = get_google_info_from_token(body.token_gg)
        if info is None:
            return request.fail_with_message(MsgLogin.LOGIN_GOOGLE_ERROR, result=None)
        else:
            try:
                user = Usuario.objects.filter(google_id=info['uid']).get()
            except Usuario.DoesNotExist:
                info['token_gg'] = body.token_gg
                info['token_fb'] = None
                return request.fail_with_message(MsgLogin.LOGIN_GOOGLE_404, info['nombre'], result=dict(google=info))

            request.session['user'] = user.pk
            request.session['up_login'] = False
            request.session['token_gg'] = body.token_gg
            msg = MsgLogin.LOGIN_GOOGLE_SUCCESS

    if user:
        try:
            user.ultimo_acceso = datetime.datetime.now()
            user.save()
        except DatabaseError as e:
            print(e)

        # perms = Permiso.objects.filter(grupos__usuarios=user).distinct().values_list('codigo', flat=True)
        # print('permisos_user', perms)
        # request.session.modified = True
        return request.success_with_message(msg, user.nombre, result=dict(nombre=user.nombre, perms=user.permisos))
    return request.fail_with_message(MsgLogin.LOGIN_DB_ERROR)


# === logout ===
@require_POST
@process_messaging_json_input
@may_fail(DatabaseError, MsgLogin.LOGOUT_DB_ERROR)
def logout(request, body):
    """
**param**

* request: no usa parametros

**return**

* retorna success true/false, ver **utils.messaging** para formato general de retorno,


 <a href="/index.html">volver a la raiz</a>
    """
    request.session.flush()
    return request.success_with_message(MsgLogin.LOGOUT_OK)


fields_upd_user = ['nombre', 'apellido', 'telefono', 'lat', 'lon', 'cta_catastral', 'ci',
                   'user_id', 'direccion', 'email']


def get_user_json(user):
    base = get_json_from_obj(user, fields_upd_user)
    base['perms'] = user.permisos
    return base


# === relogin ===
@require_POST
@process_messaging_json_input
@requires_permission_and_login
@may_fail(DatabaseError, MsgLogin.RELOAD_DB_ERROR)
@may_fail(Usuario.DoesNotExist, MsgLogin.RELOAD_RELOGIN)
def reload_login(request, body):
    """
**param**

* request: no usa parametros

**return**

* retorna success true/false y mensajes, ver **utils.messaging** para formato general de retorno,


 <a href="/index.html">volver a la raiz</a>
    """
    silent = ('silent' in body and body.silent)
    session = request.session
    if 'up_login' in session and request.session['up_login']:
        user = Usuario.objects.get(pk=request.session['user'])
        obj = get_user_json(user)
        return obj if silent else request.success_with_message(MsgLogin.RELOAD_OK, user.nombre, result=obj)

    elif 'token_gg' in session:
        print('google token! ', session['token_gg'])
        info = get_google_info_from_token(session['token_gg'])
        if info is None:
            return request.fail_with_message(MsgLogin.RELOAD_RELOGIN, result=None)
        else:
            user = Usuario.objects.filter(google_id=info['uid']).get()
            obj = get_user_json(user)
            return obj if silent else request.success_with_message(MsgLogin.LOGIN_GOOGLE_SUCCESS, user.nombre,
                                                                   result=obj)

    elif 'token_fb' in session:
        print('fb token! ', session['token_fb'])
        info = get_facebook_info_from_token(session['token_fb'])
        # print(info)
        if info is None:
            return request.fail_with_message(MsgLogin.RELOAD_RELOGIN, result=None)
        else:
            user = Usuario.objects.filter(fb_id=info['uid']).get()
            obj = get_user_json(user)
            return obj if silent else request.success_with_message(MsgLogin.LOGIN_FACEBOOK_SUCCESS, user.nombre,
                                                                   result=obj)

    raise Usuario.DoesNotExist()


# === register ====
@require_POST
@process_messaging_json_input
@check_for_json_args('nombre', 'apellido', 'telefono', 'lat', 'lon', 'cta_catastral', 'token_fb', 'token_gg', 'ci',
                     'user_id', 'direccion', 'email', 'password')
@may_fail(DatabaseError, MsgLogin.REGISTER_DB_ERROR)
@may_fail((AttributeError, decimal.InvalidOperation, RegisterFormatError), MsgLogin.REGISTER_FORMAT_ERROR)
def registro(request, body):
    """
**param**

* request: acepta parametros (** uid[str], password[str(pass hasheado a md5)] ** )

**return**

* retorna success true/false, y en result un mapa {nombre:str, permisos:[str,...]}, ver **utils.messaging** para formato general de retorno,


 <a href="/index.html">volver a la raiz</a>
    """
    nuevo = Usuario()
    for attr in ['nombre', 'apellido', 'telefono', 'cta_catastral', 'ci',
                 'user_id', 'direccion', 'email']:
        setattr(nuevo, attr, getattr(body, attr).strip())
    if len(nuevo.nombre) < 2:
        return request.fail_with_message(MsgLogin.REGISTER_INVALID_NOMBRE)
    if not validateEmail(nuevo.email):
        return request.fail_with_message(MsgLogin.REGISTER_INVALID_EMAIL)

    if body.password is not None and 'SHA-256' not in HashChecker(body.password).obtain_hash_type():
        raise RegisterFormatError()

    nuevo.password = body.password

    nuevo.user_id = nuevo.user_id.lower()
    nuevo.email = nuevo.email.lower()
    nuevo.lat = decimal.Decimal(body.lat) if body.lat is not None else None
    nuevo.lon = decimal.Decimal(body.lon) if body.lon is not None else None

    # TODO validar otros datos

    # TODO latlon validate

    # if Usuario.objects.filter(ci=nuevo.ci).exists():
    #     return request.fail_with_message(MsgLogin.REGISTER_EXISTING_CI)

    if Usuario.objects.filter(email=nuevo.email).exists():
        return request.fail_with_message(MsgLogin.REGISTER_EXISTING_EMAIL)

    if nuevo.user_id != '' and Usuario.objects.filter(user_id=nuevo.user_id).exists():
        return request.fail_with_message(MsgLogin.REGISTER_EXISTING_UID)

    up_login = True
    if body.token_fb:
        info = get_facebook_info_from_token(body.token_fb)
        if info is None:
            return request.fail_with_message(MsgLogin.REGISTER_INVALID_FB_ID)
        else:
            if Usuario.objects.filter(fb_id=info['uid']).exists():
                return request.fail_with_message(MsgLogin.REGISTER_EXISTING_FB_ID)
        nuevo.fb_id = info['uid']
        up_login = False

    if body.token_gg:
        info = get_google_info_from_token(body.token_gg)
        if info is None:
            return request.fail_with_message(MsgLogin.REGISTER_INVALID_GOOGLE_ID)
        else:
            if Usuario.objects.filter(google_id=info['uid']).exists():
                return request.fail_with_message(MsgLogin.REGISTER_EXISTING_GOOGLE_ID)
        nuevo.google_id = info['uid']
        up_login = False

    if nuevo.google_id is None and nuevo.fb_id is None and nuevo.password is None:
        return request.fail_with_message(MsgLogin.REGISTER_NO_PUEDE_LOGUEAR)

    nuevo.fecha_registro = datetime.datetime.now()
    nuevo.save()

    request.session['user'] = nuevo.pk
    request.session['up_login'] = up_login
    if body.token_gg:
        request.session['token_gg'] = body.token_gg
    if body.token_fb:
        request.session['token_fb'] = body.token_fb

    send_message_bienvenida(nuevo.nombre, nuevo.email, "Bienvenido a MapaMuni!", "<h1> Bienvenido a mapamuni, ya puede ingresar a su nueva cuenta en http://mapamuni.com    Bienvenido!</h1>")

    return request.success_with_message(MsgLogin.REGISTER_SUCCESS, nuevo.nombre, result=dict(nombre=nuevo.nombre))


# === cambio de password ===
@require_POST
@process_messaging_json_input
@requires_permission_and_login
@check_for_json_args('currentp', 'newp')
@may_fail(DatabaseError, MsgLogin.PASS_DB_ERROR)
@may_fail((AttributeError, RegisterFormatError), MsgLogin.PASS_DB_ERROR)
def cambiar_password(request, body):
    """
**param**

* request: no usa parametros

**return**

* retorna success true/false y mensajes, ver **utils.messaging** para formato general de retorno,


 <a href="/index.html">volver a la raiz</a>
    """

    if 'SHA-256' not in HashChecker(body.currentp).obtain_hash_type():
        raise RegisterFormatError()
    if 'SHA-256' not in HashChecker(body.newp).obtain_hash_type():
        raise RegisterFormatError()

    if request.usuario.password != body.currentp:
        return request.fail_with_message(MsgLogin.PASS_MISMATCH)

    user = request.usuario
    user.password = body.newp
    user.save()
    return request.success_with_message(MsgLogin.PASS_CHANGED)


# === register ====
@require_POST
@process_messaging_json_input
@requires_permission_and_login
@check_for_json_args('nombre', 'apellido', 'telefono', 'lat', 'lon', 'cta_catastral', 'ci',
                     'user_id', 'direccion', 'email')
@may_fail(DatabaseError, MsgLogin.UPDATE_PROFILE_DB_ERROR)
@may_fail((AttributeError, decimal.InvalidOperation, RegisterFormatError), MsgLogin.REGISTER_FORMAT_ERROR)
def actualizar_perfil(request, body):
    """
**param**

* request: acepta parametros (** uid[str], password[str(pass hasheado a md5)] ** )

**return**

* retorna success true/false, y en result un mapa {nombre:str, permisos:[str,...]}, ver **utils.messaging** para formato general de retorno,


 <a href="/index.html">volver a la raiz</a>
    """
    actual = request.usuario
    for attr in ['nombre', 'apellido', 'cta_catastral', 'ci', 'user_id', 'direccion', 'email', ]:
        setattr(actual, attr, getattr(body, attr).strip())
    if not validateEmail(actual.email):
        return request.fail_with_message(MsgLogin.REGISTER_INVALID_EMAIL)

    if len(actual.nombre) < 2:
        return request.fail_with_message(MsgLogin.REGISTER_INVALID_NOMBRE)

    actual.user_id = actual.user_id.lower()
    actual.email = actual.email.lower()
    actual.lat = decimal.Decimal(body.lat) if body.lat is not None else None
    actual.lon = decimal.Decimal(body.lon) if body.lon is not None else None
    actual.telefono = body.telefono

    # TODO validar otros datos

    # TODO latlon validate

    # if Usuario.objects.filter(ci=actual.ci).exclude(id=actual.id).exists():
    #     return request.fail_with_message(MsgLogin.REGISTER_EXISTING_CI)

    if Usuario.objects.filter(email=actual.email).exclude(id=actual.id).exists():
        return request.fail_with_message(MsgLogin.REGISTER_EXISTING_EMAIL)

    if Usuario.objects.filter(user_id=actual.user_id).exclude(id=actual.id).exists():
        return request.fail_with_message(MsgLogin.REGISTER_EXISTING_UID)

    # TODO conectar con facebook/google
    # if body.token_fb:
    #     info = get_facebook_info_from_token(body.token)
    #     if info is None:
    #         return request.fail_with_message(MsgLogin.REGISTER_INVALID_FB_ID)
    #     else:
    #         if Usuario.objects.filter(fb_id=info['uid']).exclude(id=actual.id).exists():
    #             return request.fail_with_message(MsgLogin.REGISTER_EXISTING_FB_ID)
    #     actual.fb_id = info['uid']
    #
    # if body.token_gg:
    #     info = get_google_info_from_token(body.token)
    #     if info is None:
    #         return request.fail_with_message(MsgLogin.REGISTER_INVALID_GOOGLE_ID)
    #     else:
    #         if Usuario.objects.filter(google_id=info['uid']).exclude(id=actual.id).exists():
    #             return request.fail_with_message(MsgLogin.REGISTER_EXISTING_GOOGLE_ID)
    #     actual.google_id = info['uid']

    actual.fecha_registro = datetime.datetime.now()
    actual.save()

    return request.success_with_message(MsgLogin.UPDATE_SUCCESS, actual.nombre, result=dict(nombre=actual.nombre))


# === register ====
@require_POST
@process_messaging_json_input
@requires_permission_and_login
@may_fail(DatabaseError, MsgLogin.UPDATE_PROFILE_DB_ERROR)
def subscribir_push(request, body):
    PushSub.objects.create(
        push_endpoint=body,
        usuario=request.usuario
    )
    print(body)
