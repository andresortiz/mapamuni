import datetime
import decimal

from django.core.paginator import EmptyPage
from django.db import DatabaseError, transaction
from django.views.decorators.http import require_GET, require_POST

from api.mqtt_handler import try_to_pub_noti_to_rt
from api.tablas.tabla_de_mensajes import MsgDenuncia
from api.tablas.tabla_de_notificaciones import Notificacion, crear_notiticacion
from api.tablas.tabla_de_parametros import ParametroCod
from api.tablas.tabla_de_permisos import Permiso
from modelos.models.denuncia import Denuncia, TipoDenuncia, MotivoDenuncia, SeguimientoDenuncia, EstadoDenuncia
from modelos.models.misc import Archivo
from utils.data_decorators import requires_permission_and_login
from utils.mailgun import send_mensage_seguimiento
from utils.messaging import process_messaging, process_messaging_json_input
from utils.misc import may_fail, get_param_convert_to_int_or_default, JSONPaginator, null_paginator_page
from utils.misc_decorators import check_for_args, check_for_json_args


@require_GET
@process_messaging
@requires_permission_and_login
@may_fail(DatabaseError, MsgDenuncia.LISTAR_TIPO_DB_ERROR, result=[])
# === listar tipo denuncia ===
def listar_tipo_denuncia(request):
    """
**param**

* request: ningun parámetro

**return**

* retorna [dataset] + mensajes, ver **utils.messaging** para formato general de retorno

 <a href="/index.html">volver a la raiz</a>
    """
    return TipoDenuncia.objects.all()


@require_GET
@process_messaging
@check_for_args('idtipo')
@requires_permission_and_login
@may_fail(DatabaseError, MsgDenuncia.LISTAR_MOTIVO_DB_ERROR, result=[])
# === listar tipo denuncia ===
def listar_motivo_denuncia(request):
    """
**param**

* request: ningun parámetro

**return**

* retorna [dataset] + mensajes, ver **utils.messaging** para formato general de retorno

 <a href="/index.html">volver a la raiz</a>
    """
    return MotivoDenuncia.objects.filter(tipo_id=request.GET['idtipo']).all()


@require_GET
@process_messaging
@requires_permission_and_login
@may_fail(DatabaseError, MsgDenuncia.LISTAR_ESTADO_DB_ERROR, result=[])
# === listar tipo denuncia ===
def listar_estado_denuncia(request):
    """
**param**

* request: ningun parámetro

**return**

* retorna [dataset] + mensajes, ver **utils.messaging** para formato general de retorno

 <a href="/index.html">volver a la raiz</a>
    """
    return EstadoDenuncia.objects.all()


@require_GET
@process_messaging
@requires_permission_and_login(either=[Permiso.LISTAR_DENUNCIAS,
                                       Permiso.FINALIZAR_DENUNCIA,
                                       Permiso.VER_SEGUIMIENTO_DENUNCIA,
                                       Permiso.SEGUIR_DENUNCIA,
                                       Permiso.CREAR_INTERVENCION], result=null_paginator_page)
@may_fail(DatabaseError, MsgDenuncia.LISTAR_DENUNCIA_DB_ERROR, result=null_paginator_page)
# === listar denuncias ===
def listar_denuncia(request):
    """
**param**

* request: ningun parámetro

**return**

* retorna [dataset] + mensajes, ver **utils.messaging** para formato general de retorno

 <a href="/index.html">volver a la raiz</a>
    """
    denuncias = Denuncia.objects
    denuncias = denuncias.filter(
        denunciante_id=request.GET['id_denunciante']) if 'id_denunciante' in request.GET else denuncias
    denuncias = denuncias.filter(
        motivo_id=request.GET['id_motivo']) if 'id_motivo' in request.GET else denuncias
    denuncias = denuncias.filter(
        nombre__contains=request.GET['nombre']) if 'nombre' in request.GET else denuncias
    denuncias = denuncias.filter(
        motivo__tipo_id=request.GET['id_tipo']) if 'id_tipo' in request.GET else denuncias
    denuncias = denuncias.filter(
        estado_id=request.GET['id_estado']) if 'id_estado' in request.GET else denuncias
    denuncias = denuncias.filter(
        from_callcenter=request.GET['callcenter']) if 'callcenter' in request.GET else denuncias
    # denuncias = denuncias.filter(
    #     estado_id=request.GET['id_estado']) if 'id_estado' in request.GET else denuncias

    # res = [d.json_for_list for d in denuncias.order_by('-fecha_creacion').all()]
    paginator = None
    try:
        cantidad_por_pagina = get_param_convert_to_int_or_default(request, paramname='resultadosxpagina', default=10)
        pagina = get_param_convert_to_int_or_default(request, paramname='pagina', default=1)

        paginator = JSONPaginator(denuncias.order_by('-fecha_creacion').all(), cantidad_por_pagina)

        return paginator.page(pagina)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        return paginator.page(paginator.num_pages)
        # print(res)
        # return res


@require_GET
@process_messaging
@requires_permission_and_login
@may_fail(DatabaseError, MsgDenuncia.LISTAR_DENUNCIA_DB_ERROR, result=null_paginator_page)
# === listar denuncias propias ===
def listar_denuncia_me(request):
    """
**param**

* request: ningun parámetro

**return**

* retorna [dataset] + mensajes, ver **utils.messaging** para formato general de retorno

 <a href="/index.html">volver a la raiz</a>
    """
    denuncias = Denuncia.objects.filter(denunciante_id=request.usuario.id, from_callcenter=False)
    # return denuncias.order_by('-fecha_creacion').all()
    paginator = None
    try:
        cantidad_por_pagina = get_param_convert_to_int_or_default(request, paramname='resultadosxpagina', default=10)
        pagina = get_param_convert_to_int_or_default(request, paramname='pagina', default=1)

        paginator = JSONPaginator(denuncias.order_by('-fecha_creacion').all(), cantidad_por_pagina)

        return paginator.page(pagina)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        return paginator.page(paginator.num_pages)
        # print(res)


# === guardar grupos ====
@require_POST
@process_messaging_json_input
@requires_permission_and_login(Permiso.CREAR_DENUNCIA_TELEFONICA)
@check_for_json_args('nombre', 'descripcion', 'motivo', 'lat', 'lon', 'archivos', 'denunciante_ci')
@may_fail(DatabaseError, MsgDenuncia.GUARDAR_DB_ERROR)
@may_fail(MotivoDenuncia.DoesNotExist, MsgDenuncia.MOTIVO_404_ERROR)
@may_fail((AttributeError, decimal.InvalidOperation), MsgDenuncia.CREAR_FORMAT_ERROR)
@transaction.atomic
def crear_denuncia(request, body):
    """
**param**

* request: acepta parametros (** uid[str], password[str(pass hasheado a md5)] ** )

**return**

* retorna success true/false, y en result un mapa {nombre:str, permisos:[str,...]}, ver **utils.messaging** para formato general de retorno,


 <a href="/index.html">volver a la raiz</a>
    """
    # si modificar busca el grupo, si nuevo crea nuevo grupo
    denuncia = Denuncia()

    for attr in ['nombre', 'descripcion', 'denunciante_ci']:
        setattr(denuncia, attr, getattr(body, attr).strip())  # copia de body a grupo

    if len(denuncia.nombre) < 4:
        return request.fail_with_message(MsgDenuncia.GUARDAR_INVALID_NOMBRE)
    if len(denuncia.descripcion) < 5:
        return request.fail_with_message(MsgDenuncia.GUARDAR_INVALID_DESCRIPCION)

    denuncia.lat = decimal.Decimal(body.lat) if body.lat is not None else None
    denuncia.lon = decimal.Decimal(body.lon) if body.lon is not None else None

    # TODO latlon validate

    denuncia.denunciante_id = request.usuario.id
    denuncia.motivo = MotivoDenuncia.objects.filter(id=body.motivo['id']).get()

    denuncia.estado = EstadoDenuncia.get_from_param(ParametroCod.ESTADO_DENUNCIA_PENDIENTE)
    denuncia.from_callcenter = True

    if not isinstance(body.archivos, list):
        return request.fail_with_message(MsgDenuncia.GUARDAR_DATA_ERROR)

    body.archivos = set(body.archivos)  # elimina repetiods

    qA = Archivo.objects.filter(id__in=body.archivos, owner_id=request.usuario.id, confirmado=False)
    if len(body.archivos) != qA.count():
        return request.fail_with_message(MsgDenuncia.GUARDAR_DATA_ERROR)  # existen todos los ids?

    denuncia.fecha_creacion = datetime.datetime.now()
    denuncia.save()

    fs = list(qA.all())
    denuncia.archivos.set(fs)

    for f in fs:
        f.confirmado = True
        f.save()

    send_mensage_seguimiento(request.usuario.nombre, request.usuario.email, "Denuncia recibida!",
                             "Hemos recibido tu denuncia y nos vamos a poner a trabajar para solucionarla, gracias por utilizar mapamuni!")

    return request.success_with_message(MsgDenuncia.AGREGAR_OK, denuncia.nombre)


# === guardar grupos ====
@require_POST
@process_messaging_json_input
@requires_permission_and_login
@check_for_json_args('nombre', 'descripcion', 'motivo', 'lat', 'lon', 'archivos')
@may_fail(DatabaseError, MsgDenuncia.GUARDAR_DB_ERROR)
@may_fail(MotivoDenuncia.DoesNotExist, MsgDenuncia.MOTIVO_404_ERROR)
@may_fail((AttributeError, decimal.InvalidOperation), MsgDenuncia.CREAR_FORMAT_ERROR)
@transaction.atomic
def crear_denuncia_me(request, body):
    """
**param**

* request: acepta parametros (** uid[str], password[str(pass hasheado a md5)] ** )

**return**

* retorna success true/false, y en result un mapa {nombre:str, permisos:[str,...]}, ver **utils.messaging** para formato general de retorno,


 <a href="/index.html">volver a la raiz</a>
    """
    # si modificar busca el grupo, si nuevo crea nuevo grupo
    denuncia = Denuncia()

    for attr in ['nombre', 'descripcion']:
        setattr(denuncia, attr, getattr(body, attr).strip())  # copia de body a grupo

    if len(denuncia.nombre) < 4:
        return request.fail_with_message(MsgDenuncia.GUARDAR_INVALID_NOMBRE)
    if len(denuncia.descripcion) < 5:
        return request.fail_with_message(MsgDenuncia.GUARDAR_INVALID_DESCRIPCION)

    denuncia.lat = decimal.Decimal(body.lat) if body.lat is not None else None
    denuncia.lon = decimal.Decimal(body.lon) if body.lon is not None else None

    # TODO latlon validate

    denuncia.denunciante_id = request.usuario.id
    denuncia.motivo = MotivoDenuncia.objects.filter(id=body.motivo['id']).get()

    denuncia.estado = EstadoDenuncia.get_from_param(ParametroCod.ESTADO_DENUNCIA_PENDIENTE)

    if not isinstance(body.archivos, list):
        return request.fail_with_message(MsgDenuncia.GUARDAR_DATA_ERROR)

    body.archivos = set(body.archivos)  # elimina repetiods

    qA = Archivo.objects.filter(id__in=body.archivos, owner_id=request.usuario.id, confirmado=False)
    if len(body.archivos) != qA.count():
        return request.fail_with_message(MsgDenuncia.GUARDAR_DATA_ERROR)  # existen todos los ids?

    denuncia.fecha_creacion = datetime.datetime.now()
    denuncia.save()

    fs = list(qA.all())
    denuncia.archivos.set(fs)

    for f in fs:
        f.confirmado = True
        f.save()

    try_to_pub_noti_to_rt('nuevas-denuncias', crear_notiticacion(Notificacion.NUEVA_DENUNCIA, request.usuario.nombre,
                                                                 request.usuario.apellido, denuncia.motivo.tipo.nombre,
                                                                 path_extra='msg'))

    return request.success_with_message(MsgDenuncia.AGREGAR_OK, denuncia.nombre)


@require_GET
@process_messaging
@check_for_args('iddenuncia')
@requires_permission_and_login(either=[Permiso.VER_SEGUIMIENTO_DENUNCIA, Permiso.SEGUIR_DENUNCIA], result=[])
@may_fail(DatabaseError, MsgDenuncia.LISTAR_SEGUIMIENTO_DB_ERROR, result=[])
# === listar tipo denuncia ===
def listar_seguimiento_denuncia(request):
    """
**param**

* request: ningun parámetro

**return**

* retorna [dataset] + mensajes, ver **utils.messaging** para formato general de retorno

 <a href="/index.html">volver a la raiz</a>
    """
    ss = list(SeguimientoDenuncia.objects.filter(denuncia_id=request.GET['iddenuncia']).all())
    for s in ss:
        s.usuario_actual = request.usuario
    return ss


@require_GET
@process_messaging
@check_for_args('iddenuncia')
@requires_permission_and_login
@may_fail(DatabaseError, MsgDenuncia.LISTAR_SEGUIMIENTO_DB_ERROR, result=[])
# === listar tipo denuncia ===
def listar_seguimiento_denuncia_me(request):
    """
**param**

* request: ningun parámetro

**return**

* retorna [dataset] + mensajes, ver **utils.messaging** para formato general de retorno

 <a href="/index.html">volver a la raiz</a>
    """
    ss = list(SeguimientoDenuncia.objects.filter(denuncia_id=request.GET['iddenuncia'],
                                                 denuncia__denunciante=request.usuario,
                                                 denuncia__from_callcenter=False).all())
    for s in ss:
        s.usuario_actual = request.usuario
    return ss


# === guardar grupos ====
@require_POST
@process_messaging_json_input
@requires_permission_and_login(Permiso.SEGUIR_DENUNCIA)
@check_for_json_args('iddenuncia', 'comentario')
@may_fail(DatabaseError, MsgDenuncia.SEGUIR_DB_ERROR)
@may_fail(Denuncia.DoesNotExist, MsgDenuncia.DENUNCIA_404_ERROR)
@transaction.atomic
def seguir_denuncia(request, body):
    """
**param**

* request: acepta parametros (** uid[str], password[str(pass hasheado a md5)] ** )

**return**

* retorna success true/false, y en result un mapa {nombre:str, permisos:[str,...]}, ver **utils.messaging** para formato general de retorno,


 <a href="/index.html">volver a la raiz</a>
    """
    denuncia = Denuncia.objects.get(pk=body.iddenuncia)
    seguimiento = SeguimientoDenuncia(comentario=str(body.comentario).strip(),
                                      denuncia=denuncia,
                                      usuario=request.usuario,
                                      fecha=datetime.datetime.now())

    if len(seguimiento.comentario) < 3:
        return request.fail_with_message(MsgDenuncia.COMENTARIO_SHORT_FIELD)

    pendiente = EstadoDenuncia.get_from_param(ParametroCod.ESTADO_DENUNCIA_PENDIENTE)
    revisada = EstadoDenuncia.get_from_param(ParametroCod.ESTADO_DENUNCIA_REVISADA)
    if denuncia.estado_id in [revisada.id, pendiente.id]:
        denuncia.estado = EstadoDenuncia.get_from_param(ParametroCod.ESTADO_DENUNCIA_EN_PROCESO)
        denuncia.save()

    seguimiento.save()

    send_mensage_seguimiento(request.usuario.nombre, request.usuario.email, "Tu Denuncia recibio una actualizacion!",
                             "Tu denuncia ha recibido el siguiente comentario: {}".format(seguimiento.comentario))

    return request.success_with_message(MsgDenuncia.SEGUIR_DENUNCIA_OK)


# === guardar grupos ====
@require_POST
@process_messaging_json_input
@requires_permission_and_login
@check_for_json_args('iddenuncia', 'comentario')
@may_fail(DatabaseError, MsgDenuncia.SEGUIR_DB_ERROR)
@may_fail(Denuncia.DoesNotExist, MsgDenuncia.DENUNCIA_404_ERROR)
def seguir_denuncia_me(request, body):
    """
**param**

* request: acepta parametros (** uid[str], password[str(pass hasheado a md5)] ** )

**return**

* retorna success true/false, y en result un mapa {nombre:str, permisos:[str,...]}, ver **utils.messaging** para formato general de retorno,


 <a href="/index.html">volver a la raiz</a>
    """
    denuncia = Denuncia.objects.get(pk=body.iddenuncia, denunciante=request.usuario)
    seguimiento = SeguimientoDenuncia(comentario=str(body.comentario).strip(),
                                      denuncia=denuncia,
                                      usuario=request.usuario,
                                      fecha=datetime.datetime.now())

    if len(seguimiento.comentario) < 3:
        return request.fail_with_message(MsgDenuncia.COMENTARIO_SHORT_FIELD)

    seguimiento.save()
    return request.success_with_message(MsgDenuncia.SEGUIR_DENUNCIA_OK)


# === guardar grupos ====
@require_POST
@process_messaging_json_input
@requires_permission_and_login(
    either=[Permiso.VER_SEGUIMIENTO_DENUNCIA, Permiso.SEGUIR_DENUNCIA, Permiso.FINALIZAR_DENUNCIA])
@check_for_json_args('iddenuncia')
@may_fail(Exception, MsgDenuncia.ERROR_CAMBIO_ESTADO)
def revisar(request, body):
    """
**param**

* request: acepta parametros (** uid[str], password[str(pass hasheado a md5)] ** )

**return**

* retorna success true/false, y en result un mapa {nombre:str, permisos:[str,...]}, ver **utils.messaging** para formato general de retorno,


 <a href="/index.html">volver a la raiz</a>
    """
    denuncia = Denuncia.objects.get(pk=body.iddenuncia)
    pendiente = EstadoDenuncia.get_from_param(ParametroCod.ESTADO_DENUNCIA_PENDIENTE)
    if denuncia.estado_id == pendiente.id:
        denuncia.estado = EstadoDenuncia.get_from_param(ParametroCod.ESTADO_DENUNCIA_REVISADA)
        denuncia.save()
        return request.success_with_message(MsgDenuncia.REVISAR_DENUNCIA_OK)


# === guardar grupos ====
@require_POST
@process_messaging_json_input
@requires_permission_and_login(Permiso.FINALIZAR_DENUNCIA)
@check_for_json_args('iddenuncia')
@may_fail(Exception, MsgDenuncia.ERROR_CAMBIO_ESTADO)
def finalizar(request, body):
    """
**param**

* request: acepta parametros (** uid[str], password[str(pass hasheado a md5)] ** )

**return**

* retorna success true/false, y en result un mapa {nombre:str, permisos:[str,...]}, ver **utils.messaging** para formato general de retorno,


 <a href="/index.html">volver a la raiz</a>
    """
    denuncia = Denuncia.objects.get(pk=body.iddenuncia)
    en_proceso = EstadoDenuncia.get_from_param(ParametroCod.ESTADO_DENUNCIA_EN_PROCESO)
    if denuncia.estado_id == en_proceso.id:
        denuncia.estado = EstadoDenuncia.get_from_param(ParametroCod.ESTADO_DENUNCIA_FINALIZADA)
        denuncia.fecha_finalizacion = datetime.datetime.now()
        denuncia.save()
        return request.success_with_message(MsgDenuncia.FINALIZAR_DENUNCIA_OK)
    else:
        return request.fail_with_message(MsgDenuncia.FINALIZAR_ESTADO_INCORRECTO, denuncia.estado.nombre)


# === guardar grupos ====
@require_POST
@process_messaging_json_input
@requires_permission_and_login
@check_for_json_args('iddenuncia')
@may_fail(Exception, MsgDenuncia.ERROR_CAMBIO_ESTADO)
def finalizar_me(request, body):
    """
**param**

* request: acepta parametros (** uid[str], password[str(pass hasheado a md5)] ** )

**return**

* retorna success true/false, y en result un mapa {nombre:str, permisos:[str,...]}, ver **utils.messaging** para formato general de retorno,


 <a href="/index.html">volver a la raiz</a>
    """
    denuncia = Denuncia.objects.get(pk=body.iddenuncia, denunciante=request.usuario)
    # en_proceso = EstadoDenuncia.get_from_param(ParametroCod.ESTADO_DENUNCIA_EN_PROCESO)
    # if denuncia.estado_id == en_proceso.id:
    denuncia.estado = EstadoDenuncia.get_from_param(ParametroCod.ESTADO_DENUNCIA_FINALIZADA)
    denuncia.fecha_finalizacion = datetime.datetime.now()
    denuncia.save()
    return request.success_with_message(MsgDenuncia.FINALIZAR_DENUNCIA_OK)
