from django.conf.urls import url

from api.views.cruds import dataset, denuncia, intervencion

urlpatterns = [
    url(r'^dataset/guardar$', dataset.guardar),
    url(r'^dataset/borrar$', dataset.borrar),
    url(r'^dataset/listar$', dataset.listar),

    # url(r'^denuncia/guardar$', dataset.guardar),
    # url(r'^denuncia/borrar$', dataset.borrar),
    url(r'^denuncia/listar/tipos$', denuncia.listar_tipo_denuncia),
    url(r'^denuncia/listar/motivos$', denuncia.listar_motivo_denuncia),
    url(r'^denuncia/listar/estados$', denuncia.listar_estado_denuncia),
    url(r'^denuncia/listar$', denuncia.listar_denuncia),
    url(r'^denuncia/listar/me$', denuncia.listar_denuncia_me),
    url(r'^denuncia/revisar$', denuncia.revisar),
    url(r'^denuncia/finalizar$', denuncia.finalizar),
    url(r'^denuncia/crear$', denuncia.crear_denuncia),
    url(r'^denuncia/crear/me$', denuncia.crear_denuncia_me),
    url(r'^denuncia/seguimiento/listar$', denuncia.listar_seguimiento_denuncia),
    url(r'^denuncia/seguimiento/listar/me$', denuncia.listar_seguimiento_denuncia_me),
    url(r'^denuncia/seguimiento/crear$', denuncia.seguir_denuncia),
    url(r'^denuncia/seguimiento/crear/me$', denuncia.seguir_denuncia_me),

    url(r'^intervencion/listar/tipos$', intervencion.listar_tipo),
    url(r'^intervencion/listar/motivos$', intervencion.listar_motivo),
    url(r'^intervencion/listar/estados', intervencion.listar_estado_intervencion),
    url(r'^intervencion/listar$', intervencion.listar_intervencion),
    url(r'^intervencion/listar/me$', intervencion.listar_intervencion_me),
    url(r'^intervencion/crear/me$', intervencion.guardar_intervencion),
    url(r'^intervencion/finalizar', intervencion.finalizar),
    # url(r'^intervencion/seguimiento/listar', denuncia.listar_seguimiento_denuncia),
    # url(r'^intervencion/seguimiento/listar/me', denuncia.listar_seguimiento_denuncia_me),

]
