import json

from django.db import DatabaseError
from django.views.decorators.http import require_GET, require_POST

from api.tablas.tabla_de_mensajes import MsgDatasets
from api.tablas.tabla_de_permisos import Permiso
from modelos.models.misc import Dataset
from utils.data_decorators import requires_permission_and_login
from utils.messaging import process_messaging_json_input
from utils.misc import may_fail, check_dict
from utils.misc_decorators import check_for_json_args


@require_GET
@process_messaging_json_input
@requires_permission_and_login(either=[Permiso.EXPORTAR_DATASETS, Permiso.VER_DATASETS], result=[])
@may_fail(DatabaseError, MsgDatasets.LISTAR_DB_ERROR, result=[])
# === listar datasets ===
def listar(request, body):
    """
**param**

* request: ningun parámetro

**return**

* retorna [dataset] + mensajes, ver **utils.messaging** para formato general de retorno

 <a href="/index.html">volver a la raiz</a>
    """
    return Dataset.objects.all()


fmt_diccionario = {'nombre': str, 'descripcion': str}


@require_POST
@process_messaging_json_input
@check_for_json_args('nombre', 'descripcion', 'query', 'descripciones')
@requires_permission_and_login(Permiso.GUARDAR_DATASETS)
@may_fail((ValueError, DatabaseError), MsgDatasets.GUARDAR_DB_ERROR)
@may_fail((AttributeError,), MsgDatasets.GUARDAR_FORMAT_ERROR)
def guardar(request, body):
    """
**param**

* request: recibe parametro ** nro[int] ** para ajustar el siguiente recibo

**return**

* retorna null en result, success true/false, ver **utils.messaging** para formato general de retorno,


 <a href="/index.html">volver a la raiz</a>
    """
    nuevo = ('id' not in body or body.id is None)
    dataset = Dataset() if nuevo else Dataset.objects.get(pk=body.id)

    for attr in ['nombre', 'descripcion', 'query']:
        setattr(dataset, attr, getattr(body, attr).strip())
    if len(dataset.nombre) < 2:
        return request.fail_with_message(MsgDatasets.GUARDAR_SHORT_FIELD, 'un nombre valido')
    if len(dataset.query) < 20:
        return request.fail_with_message(MsgDatasets.GUARDAR_SHORT_FIELD, 'una sentencia sql válida')

    if not all(check_dict(va, fmt_diccionario) for va in body.descripciones):
        return request.fail_with_message(MsgDatasets.GUARDAR_FORMATO_ERRONEO, 'el diccionario de datos')

    dataset.descripciones = json.dumps(body.descripciones)

    # try:
    #     with connection.cursor() as c:
    #         c.execute(dataset.query)
    # except Exception as e:
    #     print(e)

    dataset.save()
    return request.success_with_message(
        MsgDatasets.AGREGAR_OK if ('id' not in body or body.id is None) else MsgDatasets.MODIFICAR_OK)


# === borrar dataset ===
@require_POST
@process_messaging_json_input
@requires_permission_and_login(Permiso.GUARDAR_DATASETS)
@check_for_json_args('id')
@may_fail(DatabaseError, MsgDatasets.BORRAR_DB_ERROR)
@may_fail((ValueError, Dataset.DoesNotExist), MsgDatasets.DATASET_NOT_FOUND)
def borrar(request, body):
    """
**param**

* request: acepta parametro  (** id[int] ** )

**return**

* retorna success true/false + mensajes, ver **utils.messaging** para formato general de retorno

 <a href="/index.html">volver a la raiz</a>
    """
    Dataset.objects.get(pk=body.id).delete()
    return request.success_with_message(MsgDatasets.BORRAR_OK)
