import datetime
import decimal

from django.core.paginator import EmptyPage
from django.db import DatabaseError, transaction
from django.views.decorators.http import require_GET, require_POST

from api.mqtt_handler import try_to_pub_noti_to_rt
from api.tablas.tabla_de_mensajes import MsgIntervencion
from api.tablas.tabla_de_notificaciones import crear_notiticacion, Notificacion
from api.tablas.tabla_de_parametros import ParametroCod
from api.tablas.tabla_de_permisos import Permiso
from modelos.models.denuncia import Denuncia, EstadoDenuncia, SeguimientoDenuncia
from modelos.models.intervencion import TipoIntervencion, MotivoIntervencion, Intervencion, EstadoIntervencion
from modelos.models.misc import Archivo
from utils.data_decorators import requires_permission_and_login
from utils.messaging import process_messaging, process_messaging_json_input
from utils.misc import may_fail, get_param_convert_to_int_or_default, JSONPaginator, null_paginator_page
from utils.misc_decorators import check_for_args, check_for_json_args


@require_GET
@process_messaging
@requires_permission_and_login
@may_fail(DatabaseError, MsgIntervencion.LISTAR_TIPO_DB_ERROR, result=[])
# === listar tipo denuncia ===
def listar_tipo(request):
    """
**param**

* request: ningun parámetro

**return**

* retorna [dataset] + mensajes, ver **utils.messaging** para formato general de retorno

 <a href="/index.html">volver a la raiz</a>
    """
    return TipoIntervencion.objects.all()


@require_GET
@process_messaging
@check_for_args('idtipo')
@requires_permission_and_login
@may_fail(DatabaseError, MsgIntervencion.LISTAR_MOTIVO_DB_ERROR, result=[])
# === listar tipo denuncia ===
def listar_motivo(request):
    """
**param**

* request: ningun parámetro

**return**

* retorna [dataset] + mensajes, ver **utils.messaging** para formato general de retorno

 <a href="/index.html">volver a la raiz</a>
    """
    return MotivoIntervencion.objects.filter(tipo_id=request.GET['idtipo']).all()


@require_GET
@process_messaging
@requires_permission_and_login
@may_fail(DatabaseError, MsgIntervencion.LISTAR_ESTADO_DB_ERROR, result=[])
# === listar tipo denuncia ===
def listar_estado_intervencion(request):
    """
**param**

* request: ningun parámetro

**return**

* retorna [dataset] + mensajes, ver **utils.messaging** para formato general de retorno

 <a href="/index.html">volver a la raiz</a>
    """
    return EstadoIntervencion.objects.all()


@require_GET
@process_messaging
@requires_permission_and_login(either=[Permiso.LISTAR_INTERVENCION, Permiso.FINALIZAR_INTERVENCION])
@may_fail(DatabaseError, MsgIntervencion.LISTAR_INTERVENCION_DB_ERROR, result=null_paginator_page)
# === listar denuncias ===
def listar_intervencion(request):
    """
**param**

* request: ningun parámetro

**return**

* retorna [dataset] + mensajes, ver **utils.messaging** para formato general de retorno

 <a href="/index.html">volver a la raiz</a>
    """
    intervenciones = Intervencion.objects
    intervenciones = intervenciones.filter(
        interventor_id=request.GET['id_interventor']) if 'id_interventor' in request.GET else intervenciones
    intervenciones = intervenciones.filter(
        motivo_id=request.GET['id_motivo']) if 'id_motivo' in request.GET else intervenciones
    intervenciones = intervenciones.filter(
        nombre__contains=request.GET['nombre']) if 'nombre' in request.GET else intervenciones
    intervenciones = intervenciones.filter(
        motivo__tipo_id=request.GET['id_tipo']) if 'id_tipo' in request.GET else intervenciones
    intervenciones = intervenciones.filter(
        estado_id=request.GET['id_estado']) if 'id_estado' in request.GET else intervenciones
    # denuncias = denuncias.filter(
    #     estado_id=request.GET['id_estado']) if 'id_estado' in request.GET else denuncias

    # res = [d.json_for_list for d in intervenciones.order_by('-fecha_creacion').all()]

    paginator = None
    try:
        cantidad_por_pagina = get_param_convert_to_int_or_default(request, paramname='resultadosxpagina', default=10)
        pagina = get_param_convert_to_int_or_default(request, paramname='pagina', default=1)

        paginator = JSONPaginator(intervenciones.order_by('-fecha_creacion').all(), cantidad_por_pagina)

        return paginator.page(pagina)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        return paginator.page(paginator.num_pages)


@require_GET
@process_messaging
@requires_permission_and_login
@may_fail(DatabaseError, MsgIntervencion.LISTAR_INTERVENCION_DB_ERROR, result=null_paginator_page)
# === listar denuncias propias ===
def listar_intervencion_me(request):
    """
**param**

* request: ningun parámetro

**return**

* retorna [dataset] + mensajes, ver **utils.messaging** para formato general de retorno

 <a href="/index.html">volver a la raiz</a>
    """
    # res = [d.json_for_list for d in
    #        Intervencion.objects.filter(interventor_id=request.usuario.id).order_by('-fecha_creacion').all()]

    intervenciones = Intervencion.objects.filter(interventor_id=request.usuario.id)
    # return intervenciones.order_by('-fecha_creacion').all()
    paginator = None
    try:
        cantidad_por_pagina = get_param_convert_to_int_or_default(request, paramname='resultadosxpagina', default=10)
        pagina = get_param_convert_to_int_or_default(request, paramname='pagina', default=1)

        paginator = JSONPaginator(intervenciones.order_by('-fecha_creacion').all(), cantidad_por_pagina)

        return paginator.page(pagina)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        return paginator.page(paginator.num_pages)
        # print(res)


# === guardar grupos ====
@require_POST
@process_messaging_json_input
@requires_permission_and_login(Permiso.CREAR_INTERVENCION)
#TODO hotfix motivo
@check_for_json_args('nombre', 'descripcion',  'lat', 'lon', 'archivos', 'denuncia_preliminar', 'involucrados')
@may_fail(DatabaseError, MsgIntervencion.GUARDAR_DB_ERROR)
@may_fail(Denuncia.DoesNotExist, MsgIntervencion.DENUNCIA_404_ERROR)
@may_fail(MotivoIntervencion.DoesNotExist, MsgIntervencion.MOTIVO_404_ERROR)
@may_fail(Intervencion.DoesNotExist, MsgIntervencion.INTERVENCION_404_ERROR)
@may_fail((AttributeError, decimal.InvalidOperation), MsgIntervencion.CREAR_FORMAT_ERROR)
@transaction.atomic
def guardar_intervencion(request, body):
    """
**param**

* request: acepta parametros (** uid[str], password[str(pass hasheado a md5)] ** )

**return**

* retorna success true/false, y en result un mapa {nombre:str, permisos:[str,...]}, ver **utils.messaging** para formato general de retorno,


 <a href="/index.html">volver a la raiz</a>
    """
    # si modificar busca la intervencion, si nuevo crea nuevo grupo
    nueva = 'id' not in body or body.id is None
    intervencion = Intervencion() if nueva else Intervencion.objects.get(pk=body.id, interventor=request.usuario)

    for attr in ['nombre', 'descripcion']:
        setattr(intervencion, attr, getattr(body, attr).strip())  # copia de body a grupo

    if len(intervencion.nombre) < 4:
        return request.fail_with_message(MsgIntervencion.GUARDAR_INVALID_NOMBRE)
    if len(intervencion.descripcion) < 5:
        return request.fail_with_message(MsgIntervencion.GUARDAR_INVALID_DESCRIPCION)

    intervencion.lat = decimal.Decimal(body.lat) if body.lat is not None else None
    intervencion.lon = decimal.Decimal(body.lon) if body.lon is not None else None

    # TODO latlon validate

    if nueva:
        intervencion.interventor_id = request.usuario.id
        intervencion.fecha_creacion = datetime.datetime.now()
        if body.denuncia_preliminar is not None:
            denuncia = Denuncia.objects.get(pk=body.denuncia_preliminar['id'])
            if denuncia.intervenciones.exists():
                return request.fail_with_message(MsgIntervencion.DENUNCIA_YA_INTERVENIDA)

            pendiente = EstadoDenuncia.get_from_param(ParametroCod.ESTADO_DENUNCIA_PENDIENTE)
            revisada = EstadoDenuncia.get_from_param(ParametroCod.ESTADO_DENUNCIA_REVISADA)
            if denuncia.estado_id in [revisada.id, pendiente.id]:
                denuncia.estado = EstadoDenuncia.get_from_param(ParametroCod.ESTADO_DENUNCIA_EN_PROCESO)
                denuncia.save()
            seguimiento = SeguimientoDenuncia(
                comentario='El oficial {} {} procedió a intervenir en base a esta denuncia'.format(
                    request.usuario.nombre, request.usuario.apellido),
                denuncia=denuncia,
                usuario=request.usuario,
                fecha=datetime.datetime.now())
            seguimiento.save()
            intervencion.denuncia_preliminar = denuncia
    else:
        intervencion.fecha_modificacion = datetime.datetime.now()

    if 'motivo' in body:
        intervencion.motivo = MotivoIntervencion.objects.filter(id=body.motivo['id']).get()
    else:
        intervencion.motivo_id = 4
    intervencion.estado = EstadoIntervencion.get_from_param(ParametroCod.ESTADO_INTERVENCION_EN_PROCESO)

    if not isinstance(body.involucrados, list):
        return request.fail_with_message(MsgIntervencion.GUARDAR_DATA_ERROR)

    # TODO check contents of involucrados
    intervencion.involucrados = body.involucrados

    if not isinstance(body.archivos, list):
        return request.fail_with_message(MsgIntervencion.GUARDAR_DATA_ERROR)

    body.archivos = set(body.archivos)  # elimina repetiods

    if nueva:
        qA = Archivo.objects.filter(id__in=body.archivos, owner_id=request.usuario.id, confirmado=False)
        if len(body.archivos) != qA.count():
            return request.fail_with_message(MsgIntervencion.GUARDAR_DATA_ERROR)  # existen todos los ids?
        fs = list(qA.all())
        nuevos_fs = fs
    else:
        actuales = set(intervencion.archivos.values_list('id', flat=True))
        nuevos = body.archivos - actuales
        actuales_que_quedan = body.archivos - nuevos
        qA = Archivo.objects.filter(id__in=nuevos, owner_id=request.usuario.id, confirmado=False)
        if len(nuevos) != qA.count():
            return request.fail_with_message(MsgIntervencion.GUARDAR_DATA_ERROR)  # existen todos los ids nuevos?
        nuevos_fs = list(qA.all())
        actuales_que_quedan_fs = list(Archivo.objects.filter(id__in=actuales_que_quedan).all())
        fs = actuales_que_quedan_fs + nuevos_fs

    intervencion.save()
    intervencion.archivos.set(fs)

    for f in nuevos_fs:
        f.confirmado = True
        f.save()

    try_to_pub_noti_to_rt('nuevas-intervenciones',
                          crear_notiticacion(Notificacion.NUEVA_INTERVENCION, request.usuario.nombre,
                                             request.usuario.apellido, intervencion.motivo.tipo.nombre,
                                             path_extra='msg'))

    return request.success_with_message(MsgIntervencion.AGREGAR_OK if nueva else MsgIntervencion.MODIFICAR_OK,
                                        intervencion.nombre)


# === guardar grupos ====
@require_POST
@process_messaging_json_input
@requires_permission_and_login(Permiso.FINALIZAR_INTERVENCION)
@check_for_json_args('idintervencion')
@may_fail(Exception, MsgIntervencion.ERROR_CAMBIO_ESTADO)
def finalizar(request, body):
    """
**param**

* request: acepta parametros (** uid[str], password[str(pass hasheado a md5)] ** )

**return**

* retorna success true/false, y en result un mapa {nombre:str, permisos:[str,...]}, ver **utils.messaging** para formato general de retorno,


 <a href="/index.html">volver a la raiz</a>
    """
    intervencion = Intervencion.objects.get(pk=body.idintervencion)
    en_proceso = EstadoIntervencion.get_from_param(ParametroCod.ESTADO_INTERVENCION_EN_PROCESO)
    if intervencion.estado_id == en_proceso.id:
        intervencion.estado = EstadoIntervencion.get_from_param(ParametroCod.ESTADO_INTERVENCION_FINALIZADA)
        intervencion.fecha_finalizacion = datetime.datetime.now()
        intervencion.save()
        return request.success_with_message(MsgIntervencion.FINALIZAR_INTERVENCION_OK)
