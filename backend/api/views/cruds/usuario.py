# @require_GET
# @process_messaging
# # @requires_permission_and_login
# @may_fail(DatabaseError, MsgDatasets.LISTAR_DB_ERROR, result=[])
# # === listar datasets ===
# def listar(request):
#     """
# **param**
#
# * request: ningun parámetro
#
# **return**
#
# * retorna [dataset] + mensajes, ver **utils.messaging** para formato general de retorno
#
#  <a href="/index.html">volver a la raiz</a>
#     """
#     return Usuario.objects.all()
#
#
# fmt_diccionario = {'nombre': str, 'descripcion': str}

#
# @require_POST
# @process_messaging_json_input
# @check_for_json_args('nombre', 'descripcion', 'query', 'descripciones')
# # @requires_permission_and_login(Permiso.OPERAR_CAJA)
# # @strip_fields
# @may_fail((ValueError, DatabaseError), MsgDatasets.GUARDAR_DB_ERROR)
# def guardar(request, body):
#     """
# **param**
#
# * request: recibe parametro ** nro[int] ** para ajustar el siguiente recibo
#
# **return**
#
# * retorna null en result, success true/false, ver **utils.messaging** para formato general de retorno,
#
#
#  <a href="/index.html">volver a la raiz</a>
#     """
#     print(body)
#     # return request.success_with_message(
#     #    MsgDatasets.AGREGAR_OK if 'id' not in body else MsgDatasets.MODIFICAR_OK)
#     dataset = Dataset() if ('id' not in body or body['id'] is None) else Dataset.objects.get(pk=body['id'])
#
#     dataset.descripcion = body['descripcion']
#     dataset.nombre = body['nombre']
#     dataset.query = body['query']
#
#     if not all(check_dict(va, fmt_diccionario) for va in body['descripciones']):
#         return request.fail_with_message(MsgDatasets.GUARDAR_FORMATO_ERRONEO, 'el diccionario de datos')
#
#     dataset.descripciones = json.dumps(body['descripciones'])
#
#     # try:
#     #     with connection.cursor() as c:
#     #         c.execute(dataset.query)
#     # except Exception as e:
#     #     print(e)
#
#     dataset.save()
#     return request.success_with_message(
#         MsgDatasets.AGREGAR_OK if 'id' not in body else MsgDatasets.MODIFICAR_OK)
#
#
# # === borrar caja ===
# @require_POST
# @process_messaging_json_input
# # @requires_permission_and_login(Permiso.CRUD_CAJA)
# @check_for_json_args('id')
# @may_fail(DatabaseError, MsgDatasets.BORRAR_DB_ERROR)
# @may_fail((ValueError, Dataset.DoesNotExist), MsgDatasets.DATASET_NOT_FOUND)
# def borrar(request):
#     """
# **param**
#
# * request: acepta parametro  (** id[int] ** )
#
# **return**
#
# * retorna success true/false + mensajes, ver **utils.messaging** para formato general de retorno
#
#  <a href="/index.html">volver a la raiz</a>
#     """
#     Dataset.objects.get(pk=request.POST['id']).delete()
#     return request.success_with_message(MsgDatasets.BORRAR_OK)
