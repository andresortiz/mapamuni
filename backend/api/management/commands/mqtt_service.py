import json

from django.core.management.base import BaseCommand

from api.mqtt_handler import launch_mqtt_handler, run_mqtt_handler
from modelos.models.misc import Persona


def loads_json_from_file(path):
    with open(path, 'r') as fi:
        sjson = fi.read(10000000)
    return json.loads(sjson)


class Command(BaseCommand):
    help = 'lanza el microservicio helper mqtt de mapamuni'

    def handle(self, *args, **options):
        run_mqtt_handler()

