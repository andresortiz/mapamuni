import datetime
import json

from django.core.management.base import BaseCommand
from pywebpush import webpush, WebPushException

from utils.misc import loads_json_from_file


class Command(BaseCommand):
    help = 'Importa padron csv a bd'

    def add_arguments(self, parser):
        parser.add_argument('cred', type=str)
        parser.add_argument('sub', type=str)

    def handle(self, *args, **options):
        cred = loads_json_from_file(options['cred'])
        sub = loads_json_from_file(options['sub'])

        notif = {
            "notification": {
                "title": "Angular News",
                "body": "Newsletter Available!",
                "icon": "assets/images/marker.png",
                "vibrate": [100, 50, 100],
                "data": {
                    "dateOfArrival": str(datetime.datetime.now()),
                    "primaryKey": 1
                },
                "actions": [{
                    "action": "explore",
                    "title": "Go to the site leroy"
                }]
            }
        }

        try:
            webpush(
                subscription_info=sub,
                data=json.dumps(notif),
                vapid_private_key=cred['privateKey'],
                vapid_claims={
                    "sub": "mailto:YourNameHere@example.org",

                }
            )
        except WebPushException as ex:
            print("I'm sorry, Dave, but I can't do that: {}", repr(ex))
            # Mozilla returns additional information in the body of the response.
            if ex.response and ex.response.json():
                extra = ex.response.json()
                print("Remote service replied with a {}:{}, {}",
                      extra.code,
                      extra.errno,
                      extra.message
                      )
