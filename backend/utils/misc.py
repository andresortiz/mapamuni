import datetime
import json
import signal
import time
from enum import Enum

import functools
from annoying.functions import get_config
from django.core.paginator import Paginator, Page
from django.db import connection


def get_str_or_enum_value(val) -> str:
    while isinstance(val, Enum):
        val = val.value
    return val


def parse_date_str(date_s, fmt='%Y-%m-%d'):
    return datetime.datetime(*time.strptime(date_s, fmt)[:6]).date()


def get_param_convert_to_int_or_default(request, paramname, default) -> int:
    try:
        return int(request.GET[paramname] if paramname in request.GET else default)
    except ValueError:
        return default


class JSONPage(Page):
    def __json__(self):
        return {
            'page': self.number,
            'page_size': self.paginator.per_page,
            'total': self.paginator.num_pages,
            'count': self.paginator.count,
            'prev_page': self.previous_page_number() if self.has_previous() else 1,
            'next_page': self.next_page_number() if self.has_next() else self.paginator.num_pages,
            'objects': list(self),
        }


class JSONPaginator(Paginator):
    def _get_page(self, *args, **kwargs):
        return JSONPage(*args, **kwargs)

null_paginator_page = {'page': 0, 'page_size': 0, 'total': 0, 'count': 0, 'prev_page': 0, 'next_page': 0, 'objects': []}



# Para poder recuperar el periodo actual de datocolegio de forma estatica
class ClassPropertyDescriptor(object):
    def __init__(self, fget, fset=None):
        self.fget = fget
        self.fset = fset

    def __get__(self, obj, klass=None):
        if klass is None:
            klass = type(obj)
        return self.fget.__get__(obj, klass)()

    def __set__(self, obj, value):
        if not self.fset:
            return
            # raise AttributeError("can't set attribute")
        type_ = type(obj)
        return self.fset.__get__(obj, type_)(value)

    def setter(self, func):
        if not isinstance(func, (classmethod, staticmethod)):
            func = classmethod(func)
        self.fset = func
        return self


def classproperty(func):
    if not isinstance(func, (classmethod, staticmethod)):
        func = classmethod(func)

    return ClassPropertyDescriptor(func)


def if_debug_log_queries(request):
    if get_config('DEBUG', False):
        path = '{}/{} - {} {}.log'.format(get_config('QUERYFILES_DIR', None), datetime.datetime.now().isoformat(),
                                          request.method, request.path.replace('/', '\\'))
        with open(path, 'w') as qo:
            for q in [q for q in connection.queries if q['sql'] != 'SET SQL_AUTO_IS_NULL = 0']:
                qo.write('{:>3}\t   {}\n'.format(q['time'], q['sql']))


def may_fail(cls_exc, msg, *argsu, use_messaging=True, **kwargsu):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(request, *args, **kwargs):
            try:
                return func(request, *args, **kwargs)
            except cls_exc as exc:
                if use_messaging:
                    if get_config('DEBUG', False):
                        request.enqueue_message(info='DEBUG=true, Excepcion: {}'.format(repr(exc)))
                    return request.fail_with_message(msg, *argsu, **kwargsu)
                else:
                    from api.reporting import render_error_page
                    return render_error_page(request, msg, *exc.args)

        return wrapper

    return decorator


def check_dict(to_check, template):
    if len(set(to_check) & set(template)) != len(set(template)):
        return False
    for key, tipe in template.items():
        if not isinstance(to_check[key], tipe):
            return False
    return True


def merge_dicts(*dict_args):
    """
    Given any number of dicts, shallow copy and merge into a new dict,
    precedence goes to key value pairs in latter dicts.
    """
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result


class objdict(dict):
    def __getattr__(self, name):
        if name in self:
            return self[name]
        else:
            raise AttributeError("No such attribute: " + name)

    def __setattr__(self, name, value):
        self[name] = value

    def __delattr__(self, name):
        if name in self:
            del self[name]
        else:
            raise AttributeError("No such attribute: " + name)


def validateEmail(email):
    from django.core.validators import validate_email
    from django.core.exceptions import ValidationError
    try:
        validate_email(email)
        return True
    except ValidationError:
        return False


def get_json_from_obj(user, fields):
    r = {}
    for field in fields:
        r[field] = getattr(user, field)
    return r


def loads_json_from_file(path):
    with open(path, 'r') as fi:
        sjson = fi.read(10000000)
    return json.loads(sjson)


def save_obj_to_json_file(path, obj):
    with open(path, 'w') as f:
        f.write(json.dumps(obj, separators=(',', ': ')))


class Alarm(Exception):
    pass


def alarm_handler(signum, frame):
    raise Alarm


def set_alarm_handler():
    if signal.getsignal(signal.SIGALRM) != alarm_handler:
        print('reseting handler')
        signal.signal(signal.SIGALRM, alarm_handler)
        signal.alarm(0)
