from django.conf import settings
from google.auth.transport import requests
from google.oauth2 import id_token


def get_google_info_from_token(token):
    try:
        # Specify the CLIENT_ID of the app that accesses the backend:
        idinfo = id_token.verify_oauth2_token(token, requests.Request(), settings.GOOGLE_CLIENT_ID)
        if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
            raise ValueError('Wrong issuer.')

        # If auth request is from a G Suite domain:
        # if idinfo['hd'] != GSUITE_DOMAIN_NAME:
        #     raise ValueError('Wrong hosted domain.')

        # ID token is valid. Get the user's Google Account ID from the decoded token.
        return dict(uid=idinfo['sub'],
                    email=idinfo['email'],
                    nombre=idinfo['given_name'],
                    apellido=idinfo['family_name']
                    )
    except ValueError as e:
        print(e)
        return None
        # Invalid token
