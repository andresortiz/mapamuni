from django.conf import settings
from itsdangerous import URLSafeTimedSerializer

serializer = URLSafeTimedSerializer(settings.SECRET_KEY)


def get_token(ci):
    return serializer.dumps(ci)


def decode_token(token, max_age=5):
    return serializer.loads(token, max_age=max_age)
