import functools

from api.tablas.tabla_de_mensajes import MsgMisc
from modelos.models.acceso import Usuario
from utils.misc import get_str_or_enum_value, may_fail


def requires_permission_and_login(*argsu, **kwargsu):
    kwargsu['msg'] = kwargsu.get('msg', MsgMisc.NO_PERMISSION)
    kwargsu['result'] = kwargsu.get('result', None)
    argsu = [get_str_or_enum_value(arg) for arg in argsu]
    either = set([] if 'either' not in kwargsu else [get_str_or_enum_value(arg) for arg in kwargsu['either']])

    def decorator(func):
        @may_fail(Usuario.DoesNotExist, MsgMisc.RELOGIN, result=kwargsu['result'])
        @may_fail(KeyError, MsgMisc.RELOGIN, result=kwargsu['result'])
        @functools.wraps(func)
        def wrapper(request, *args, **kwargs):
            request.usuario = Usuario.objects.get(pk=request.session['user'])
            permisos = request.usuario.permisos
            if ((argsu and len(argsu) > 0 and not callable(argsu[0])) and not (set(argsu) <= set(permisos))) or (
                        (len(either) > 0 and len(argsu) > 0 and not callable(argsu[0])) and (
                        len(either & set(permisos)) == 0)):
                return request.fail_with_message(kwargsu['msg'], result=kwargsu['result'])

            return func(request, *args, **kwargs)

        return wrapper

    return decorator(argsu[0]) if argsu and callable(argsu[0]) else decorator
