import requests
from django.conf import settings


def get_facebook_info_from_token(token):
    appLink = 'https://graph.facebook.com/oauth/access_token?client_id={}&client_secret={}&grant_type=client_credentials'.format(
        settings.FB_APP_ID, settings.FB_APP_SECRET)
    # From appLink, retrieve the second accessToken: app access_token
    appToken = requests.get(appLink).json()['access_token']
    link = 'https://graph.facebook.com/debug_token?input_token={}&access_token={}'.format(token, appToken)
    try:
        userdata = requests.get(link).json()['data']
        if userdata['app_id'] == settings.FB_APP_ID and userdata['is_valid'] == True:
            urlinfo = 'https://graph.facebook.com/v3.0/me?access_token={}&fields=name,email,first_name,last_name'.format(
                token)
            idinfo = requests.get(urlinfo).json()
            if idinfo['id'] != userdata['user_id']:
                return None
            return dict(uid=idinfo['id'],
                        email=idinfo['email'],
                        nombre=idinfo['first_name'],
                        apellido=idinfo['last_name']
                        )
        else:
            return None
    # ['user_id']
    except (ValueError, KeyError, TypeError) as error:
        print(error)
        return None
    # print(userId)



