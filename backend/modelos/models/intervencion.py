import jsonfield
from django.db import models

from modelos.models.acceso import Usuario
from modelos.models.denuncia import Denuncia
from modelos.models.misc import Archivo, Parametro
from utils.misc import merge_dicts
from utils.misc_decorators import add_auto_json


@add_auto_json
# Los tipos de intervencion (Infraccion, Accidente)
class TipoIntervencion(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(unique=True, max_length=255)
    descripcion = models.TextField()
    icono = models.CharField(max_length=255)

    class Meta:
        db_table = 'tipo_intervencion'


@add_auto_json
# Los motivos posibles de cada tipo de intervencion
class MotivoIntervencion(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(unique=True, max_length=255)
    descripcion = models.TextField()
    tipo = models.ForeignKey(TipoIntervencion, models.PROTECT, related_name='motivos')
    icono = models.CharField(max_length=255)

    class Meta:
        db_table = 'motivo_intervencion'

    @property
    def json_for_list(self):
        base = self.__json__()
        return merge_dicts(base, dict(
            tipo=self.tipo.__json__(),
        ))


@add_auto_json
# TODO ver estados de intervencion
# Los estados de cada denuncia (Pendiente, Revisada, En Proceso, Finalizada)
class EstadoIntervencion(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(unique=True, max_length=255)
    descripcion = models.TextField()

    class Meta:
        db_table = 'estado_intervencion'

    @classmethod
    def get_from_param(cls, key):
        return EstadoIntervencion.objects.get(id=Parametro.get_val(key))


@add_auto_json
class Intervencion(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=255)
    descripcion = models.TextField()

    involucrados = jsonfield.JSONField()

    # TODO deberia ser uno a uno? (Puede haber mas de una intervencion vinculada a una misma denuncia)
    denuncia_preliminar = models.ForeignKey(Denuncia, models.PROTECT, related_name='intervenciones', null=True)

    interventor = models.ForeignKey(Usuario, models.PROTECT, related_name='intervenciones')

    estado = models.ForeignKey(EstadoIntervencion, models.PROTECT, related_name='intervenciones')
    motivo = models.ForeignKey(MotivoIntervencion, models.PROTECT, related_name='intervenciones')

    lat = models.DecimalField(max_digits=20, decimal_places=15, null=True)
    lon = models.DecimalField(max_digits=20, decimal_places=15, null=True)

    archivos = models.ManyToManyField(Archivo)

    fecha_creacion = models.DateTimeField()
    fecha_modificacion = models.DateTimeField(null=True)
    fecha_finalizacion = models.DateTimeField(null=True)

    # TODO intervencion tiene otra fecha dependiendo del estado?
    # TODO intervencion se puede linkear con una infraccion o boleta o algo?
    # TODO intervencion tiene actores o algo asi?
    # TODO intervencion, que estados tiene
    # TODO campos faltantes en intervencion?

    class Meta:
        db_table = 'intervencion'

    def __json__(self):
        return dict(
            motivo=self.motivo.json_for_list,
            estado=self.estado.__json__(),
            interventor=dict(nombre=self.interventor.nombre, apellido=self.interventor.apellido),
            denuncia_preliminar=self.denuncia_preliminar if self.denuncia_preliminar else None
        )

# #TODO agregar informacion del movil que realiza la intervencion
# class Movil(models.Model):
#     id = models.AutoField(primary_key=True)
#     nombre = models.CharField(unique=True, max_length=255)
#     descripcion = models.TextField()
#     inicio = models.DateTimeField(null=True)
#     fin = models.DateTimeField(null=True)
#     link = models.TextField()

# alumno = models.ForeignKey(Alumno, models.PROTECT, db_column='idAlumno', related_name='deudas', null=True)
#     razon_anulacion = models.ForeignKey(RazonAnulacionDeuda, models.PROTECT, db_column='idRazonAnulacion',
#                                         null=True)
#     fecha_vencimiento = models.DateField(db_column='fechaVencimiento', null=True)
#     materia = models.ForeignKey(Materia, models.PROTECT, db_column='idMateria', null=True)
#     cantidad = models.IntegerField(default=1, null=False)
#     subtotal = models.IntegerField(default=0, null=False)
#     descuento = models.IntegerField(default=0, null=False)
#     migrado = Bit1BooleanField(default=False, null=False)
#     examen_derecho = models.ForeignKey(Examen, models.PROTECT, db_column='idExamenDerecho', null=True)
