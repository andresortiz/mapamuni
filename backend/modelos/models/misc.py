import datetime
import json

import jsonfield
import re
from django.db import models

from modelos.models.acceso import Usuario
from utils.misc import get_str_or_enum_value
from utils.misc_decorators import add_auto_json


@add_auto_json
class Dataset(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(unique=True, max_length=255)
    descripcion = models.TextField()
    query = models.TextField()
    descripciones = models.TextField()

    class Meta:
        db_table = 'datasets'

    @property
    def nombre_mapper(self):
        return {row['nombre']: row['descripcion'] for row in json.loads(self.descripciones)}

    def __json__(self):
        return dict(descripciones=json.loads(self.descripciones))


@add_auto_json
class Archivo(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=255)
    descripcion = models.TextField()

    url = models.CharField(max_length=255, default='')
    owner = models.ForeignKey(Usuario, on_delete=models.SET_NULL, null=True)
    datos = models.BinaryField()

    subido_en = models.DateTimeField()
    confirmado = models.BooleanField(default=False)

    class Meta:
        db_table = 'archivo'


idx_regex = re.compile(r'^\S+\[(\d+)\]$')


class Parametro(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=255)
    date_val = models.DateField(null=True)
    string_val = models.TextField(null=True)
    int_val = models.IntegerField(null=True)

    @property
    def value(self):
        filtered = [p for p in [self.date_val, self.string_val, self.int_val] if p is not None]
        return None if filtered == [] else filtered[0]

    @value.setter
    def set_value(self, new_val):
        self.date_val = new_val if isinstance(new_val, datetime.date) else None
        self.int_val = new_val if isinstance(new_val, int) else None
        self.string_val = new_val if isinstance(new_val, str) else None if new_val is None else str(new_val)

    class Meta:
        db_table = 'parametro'

    @classmethod
    def get_val(cls, key, array=False, as_tuples=False):
        key = get_str_or_enum_value(key)
        if not array:
            return Parametro.objects.get(nombre=key).value
        else:
            ps = Parametro.objects.filter(nombre__startswith='%s[' % key, nombre__endswith=']').all()
            tuples = sorted([(int(idx_regex.match(p.nombre).groups()[0]), p.value) for p in ps])
            return tuples if as_tuples else [b for a, b in tuples]

    @classmethod
    def get_val_or_none(cls, key):
        try:
            return Parametro.get_val(key)
        except Parametro.DoesNotExist:
            return None


@add_auto_json
class Persona(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=255)
    apellido = models.CharField(max_length=255, db_index=True)
    ci = models.CharField(max_length=10, unique=True, db_index=True)

    class Meta:
        db_table = 'persona'


@add_auto_json
class DatoAbiertoSistema(models.Model):
    id = models.AutoField(primary_key=True)
    codigo = models.CharField(unique=True, max_length=255)
    nombre = models.CharField(unique=True, max_length=255)
    descripcion = models.TextField()
    archivo = models.ForeignKey(Archivo, on_delete=models.PROTECT, null=True)
    subible = models.BooleanField(default=True)

    json_data = jsonfield.JSONField(null=True)

    class Meta:
        db_table = 'dato_abierto'

    # @property
    # def nombre_mapper(self):
    #     return {row['nombre']: row['descripcion'] for row in json.loads(self.descripciones)}

    def __json__(self):
        return dict(nombre_archivo=self.archivo.nombre) if self.archivo else '<JSON DATA>'

    @classmethod
    def get_val(cls, key, array=False, as_tuples=False):
        key = get_str_or_enum_value(key)
        if not array:
            return DatoAbiertoSistema.objects.get(codigo=key).value
        else:
            ps = DatoAbiertoSistema.objects.filter(codigo__startswith='%s[' % key, codigo__endswith=']').all()
            tuples = sorted([(int(idx_regex.match(p.codigo).groups()[0]), p.value) for p in ps])
            return tuples if as_tuples else [b for a, b in tuples]

    @property
    def value(self):
        if self.archivo:
            return self.archivo.datos
        return self.json_data


@add_auto_json
class PushSub(models.Model):
    id = models.AutoField(primary_key=True)
    push_endpoint = jsonfield.JSONField(unique=True)
    usuario = models.ForeignKey(Usuario, models.PROTECT, related_name='pushers')

    class Meta:
        db_table = 'push_subs'
