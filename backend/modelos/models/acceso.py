from django.db import models

# TODO
from utils.misc_decorators import add_auto_json


@add_auto_json
class Permiso(models.Model):
    id = models.AutoField(primary_key=True)
    codigo = models.CharField(unique=True, max_length=255, null=True)
    nombre = models.CharField(max_length=255)
    padre = models.ForeignKey('Permiso', models.SET_NULL, null=True, related_name='hijos')
    descripcion = models.TextField()

    class Meta:
        db_table = 'permiso'

    @property
    def prime_json(self):
        es_hoja = self.es_hoja
        hijos = [] if es_hoja else [c.prime_json for c in self.hijos.all()]
        return dict(
            label=self.nombre,
            data=self.id,
            # icon='fa fa-file-image-o' if es_hoja else '',
            expandedIcon='fa fa-folder-open',
            collapsedIcon='fa fa-folder',
            children=[] if es_hoja else hijos,
            leaf=es_hoja,
            expanded=True
        )

    @property
    def es_hoja(self):
        return not self.hijos.exists()


@add_auto_json
class GrupoUsuario(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(unique=True, max_length=255)
    descripcion = models.TextField()

    permisos = models.ManyToManyField(Permiso, related_name='grupos')

    # usuarios = models.ManyToManyField('Usuario')

    ultima_modificacion = models.DateTimeField()

    class Meta:
        db_table = 'grupo_usuario'

    @property
    def json_for_edit(self):
        base = self.__json__()
        base['permisos'] = self.permisos.values_list('id', flat=True)
        base['usuarios'] = self.usuarios.values_list('id', flat=True)
        return base


@add_auto_json(
    exclude=['password', 'cta_catastral', 'direccion', 'telefono', 'lat', 'lon', 'fb_id', 'google_id', 'ultimo_acceso',
             'fecha_registro'])
class Usuario(models.Model):
    id = models.AutoField(primary_key=True)
    user_id = models.CharField(max_length=255)
    nombre = models.CharField(max_length=255)
    apellido = models.CharField(max_length=255)
    ci = models.CharField(max_length=255)
    email = models.CharField(max_length=255)
    password = models.CharField(max_length=255, null=True)

    cta_catastral = models.CharField(max_length=255)
    direccion = models.TextField()
    telefono = models.CharField(max_length=255)

    lat = models.DecimalField(max_digits=20, decimal_places=15, null=True)
    lon = models.DecimalField(max_digits=20, decimal_places=15, null=True)

    fb_id = models.CharField(max_length=255, null=True)
    google_id = models.CharField(max_length=255, null=True)

    ultimo_acceso = models.DateTimeField(null=True)
    fecha_registro = models.DateTimeField()

    permisos = models.ManyToManyField(Permiso)
    grupos = models.ManyToManyField(GrupoUsuario, related_name='usuarios')

    resetToken = models.CharField(max_length=255, null=True)

    class Meta:
        db_table = 'usuario'

    @property
    def json_for_edit(self):
        base = self.__json__()
        base['permisos'] = self.permisos.values_list('id', flat=True)
        base['grupos'] = self.grupos.values_list('id', flat=True)
        return base

    @property
    def permisos(self):
        return Permiso.objects.filter(grupos__usuarios=self).distinct().values_list('codigo', flat=True)
