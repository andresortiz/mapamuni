from django.db import models

from modelos.models.acceso import Usuario
from modelos.models.misc import Archivo, Parametro
from utils.misc import merge_dicts
from utils.misc_decorators import add_auto_json, get_auto_json


@add_auto_json
# Los tipos de denucia (Infraccion, Reclamo, Accidente)
class TipoDenuncia(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(unique=True, max_length=255)
    descripcion = models.TextField()
    icono = models.CharField(max_length=255)
    intervenible = models.BooleanField(default=True)

    class Meta:
        db_table = 'tipo_denuncia'


@add_auto_json
# Los motivos posibles de cada tipo de denuncia
class MotivoDenuncia(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(unique=True, max_length=255)
    descripcion = models.TextField()
    tipo = models.ForeignKey(TipoDenuncia, models.PROTECT, related_name='motivos')
    icono = models.CharField(max_length=255)

    class Meta:
        db_table = 'motivo_denuncia'

    @property
    def json_for_list(self):
        base = self.__json__()
        return merge_dicts(base, dict(
            tipo=self.tipo.__json__(),
        ))


@add_auto_json
# Los estados de cada denuncia (Pendiente, Revisada, En Proceso, Finalizada)
class EstadoDenuncia(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(unique=True, max_length=255)
    descripcion = models.TextField()

    class Meta:
        db_table = 'estado_denuncia'

    @classmethod
    def get_from_param(cls, key):
        return EstadoDenuncia.objects.get(id=Parametro.get_val(key))


@add_auto_json
class Denuncia(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=255)
    descripcion = models.TextField()

    denunciante = models.ForeignKey(Usuario, models.PROTECT, related_name='denuncias')

    from_callcenter = models.BooleanField(default=False)
    denunciante_ci = models.CharField(max_length=255, default='')

    estado = models.ForeignKey(EstadoDenuncia, models.PROTECT, related_name='denuncias')
    motivo = models.ForeignKey(MotivoDenuncia, models.PROTECT, related_name='denuncias')

    lat = models.DecimalField(max_digits=20, decimal_places=15, null=True)
    lon = models.DecimalField(max_digits=20, decimal_places=15, null=True)
    archivos = models.ManyToManyField(Archivo)

    fecha_creacion = models.DateTimeField()
    fecha_finalizacion = models.DateTimeField(null=True)

    class Meta:
        db_table = 'denuncia'

    @property
    def json_for_list(self):
        base = get_auto_json(self)
        return merge_dicts(base, dict(
            motivo=self.motivo.json_for_list,
            estado=self.estado.__json__(),
            denunciante=dict(nombre=self.denunciante.nombre, apellido=self.denunciante.apellido),

        ))

    def __json__(self):
        return dict(
            motivo=self.motivo.json_for_list,
            estado=self.estado.__json__(),
            denunciante=dict(nombre=self.denunciante.nombre, apellido=self.denunciante.apellido),
            intervenida=self.intervenciones.exists()
        )


@add_auto_json
class SeguimientoDenuncia(models.Model):
    id = models.AutoField(primary_key=True)
    denuncia = models.ForeignKey(Denuncia, models.PROTECT, related_name='seguimientos')

    usuario = models.ForeignKey(Usuario, models.PROTECT, related_name='seguimientos')
    comentario = models.TextField()
    archivos = models.ManyToManyField(Archivo)

    fecha = models.DateTimeField()

    class Meta:
        db_table = 'seguimiento_denuncia'

    def __json__(self):
        base = dict(
            usuario=dict(nombre=self.usuario.nombre, apellido=self.usuario.apellido)
        )
        if self.usuario_actual:
            base['mio'] = (self.usuario.id == self.usuario_actual.id)
        return base
