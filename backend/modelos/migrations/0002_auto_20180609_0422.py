# Generated by Django 2.0.6 on 2018-06-09 04:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('modelos', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usuario',
            name='lat',
            field=models.DecimalField(decimal_places=15, max_digits=20, null=True),
        ),
        migrations.AlterField(
            model_name='usuario',
            name='lon',
            field=models.DecimalField(decimal_places=15, max_digits=20, null=True),
        ),
    ]
