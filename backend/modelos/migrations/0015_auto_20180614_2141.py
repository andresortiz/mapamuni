# Generated by Django 2.0.6 on 2018-06-14 21:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('modelos', '0014_auto_20180614_2128'),
    ]

    operations = [
        migrations.AlterField(
            model_name='persona',
            name='ci',
            field=models.CharField(max_length=255, unique=True),
        ),
    ]
