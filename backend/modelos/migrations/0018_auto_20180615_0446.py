# Generated by Django 2.0.6 on 2018-06-15 04:46

from django.db import migrations, models
import django.db.models.deletion
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('modelos', '0017_auto_20180615_0257'),
    ]

    operations = [
        migrations.CreateModel(
            name='EstadoIntervencion',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=255, unique=True)),
                ('descripcion', models.TextField()),
            ],
            options={
                'db_table': 'estado_intervencion',
            },
        ),
        migrations.CreateModel(
            name='Intervencion',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=255)),
                ('descripcion', models.TextField()),
                ('involucrados', jsonfield.fields.JSONField()),
                ('lat', models.DecimalField(decimal_places=15, max_digits=20, null=True)),
                ('lon', models.DecimalField(decimal_places=15, max_digits=20, null=True)),
                ('fecha_creacion', models.DateTimeField()),
                ('archivos', models.ManyToManyField(to='modelos.Archivo')),
                ('denuncia_preliminar', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='itervenciones', to='modelos.Denuncia')),
                ('estado', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='intervenciones', to='modelos.EstadoIntervencion')),
                ('interventor', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='intervenciones', to='modelos.Usuario')),
            ],
            options={
                'db_table': 'intervencion',
            },
        ),
        migrations.CreateModel(
            name='MotivoIntervencion',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=255, unique=True)),
                ('descripcion', models.TextField()),
                ('icono', models.CharField(max_length=255)),
            ],
            options={
                'db_table': 'motivo_intervencion',
            },
        ),
        migrations.CreateModel(
            name='TipoIntervencion',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=255, unique=True)),
                ('descripcion', models.TextField()),
                ('icono', models.CharField(max_length=255)),
            ],
            options={
                'db_table': 'tipo_intervencion',
            },
        ),
        migrations.AddField(
            model_name='motivointervencion',
            name='tipo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='motivos', to='modelos.TipoIntervencion'),
        ),
        migrations.AddField(
            model_name='intervencion',
            name='motivo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='intervenciones', to='modelos.MotivoIntervencion'),
        ),
    ]
