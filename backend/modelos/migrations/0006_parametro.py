# Generated by Django 2.0.6 on 2018-06-10 04:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('modelos', '0005_auto_20180609_2228'),
    ]

    operations = [
        migrations.CreateModel(
            name='Parametro',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=255)),
                ('date_val', models.DateField(null=True)),
                ('string_val', models.TextField(null=True)),
                ('int_val', models.IntegerField(null=True)),
            ],
            options={
                'db_table': 'parametro',
                'managed': False,
            },
        ),
    ]
