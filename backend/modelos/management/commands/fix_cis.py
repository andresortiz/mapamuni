import json

from django.core.management.base import BaseCommand

from modelos.models.misc import Persona


def loads_json_from_file(path):
    with open(path, 'r') as fi:
        sjson = fi.read(10000000)
    return json.loads(sjson)


class Command(BaseCommand):
    help = 'arregla cis'

    def handle(self, *args, **options):
        cis = loads_json_from_file('ci_repetida.json')
        for obj in cis:
            personas = Persona.objects.filter(ci=obj['ci']).order_by('apellido').all()
            c = 0
            for persona in personas:
                persona.ci += 'abcdefg'[c]
                persona.save()
                print('{} - {}, {}'.format(persona.ci, persona.apellido, persona.nombre))
                c += 1
