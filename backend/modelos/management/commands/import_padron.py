import csv
import datetime

from django.core.management.base import BaseCommand

from modelos.models.misc import Persona


class Command(BaseCommand):
    help = 'Importa padron csv a bd'

    def add_arguments(self, parser):
        parser.add_argument('csvpath', type=str)

    def handle(self, *args, **options):
        with open(options['csvpath']) as csvfile:
            spamreader = csv.reader(csvfile)
            not_first = False
            c = 0
            last = datetime.datetime.now()
            batch = []
            for row in spamreader:
                # print(row)
                if not_first:
                    c += 1
                    if c % 10000 == 0:

                        Persona.objects.bulk_create(
                            batch
                        )
                        batch = []
                        print('Persona {} : {}'.format(c, str(datetime.datetime.now() - last)))
                        last = datetime.datetime.now()
                    batch.append(Persona(ci=row[0],
                                         nombre=row[1],
                                         apellido=row[2]))
                else:
                    not_first = True
