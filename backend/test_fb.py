import requests

# Send the first accessToken returned by Facebook SDK to backend
userToken = "EAAEDRGHpEHMBAL6oRk1ckEfXrZAEMfLpdU3uOf4hSNG1ajxZATldYbjPenwa0UQdpus4ZBIEgcTp36rOMBwvlm4rnZBZAW2rGrVPfewNna7ZB3BrhTTG3ETJn6gDKzgcFA2pl3X7QdPHhx5Q8VeONLL9DN9eGorPCPH5ZCrgIvvcWZB0apdPZCT2sMIzpFkHygjEQqjbDOB7TxQZDZD"
# copy clientId, clientSecret from MY APP Page
clientId = '285067212034163'
clientSecret = '8ba3577c8d3522f929e536d337f1ee4f'


#
# appLink = 'https://graph.facebook.com/oauth/access_token?client_id=' + clientId + '&client_secret=' + clientSecret + '&grant_type=client_credentials'
# # From appLink, retrieve the second accessToken: app access_token
# appToken = requests.get(appLink).json()['access_token']
# link = 'https://graph.facebook.com/debug_token?input_token=' + userToken + '&access_token=' + appToken
# try:
#     userId = requests.get(link).json()['data']
#
# # ['user_id']
# except (ValueError, KeyError, TypeError) as error:
#     raise error
# print(userId)
#
# urld = 'https://graph.facebook.com/v3.0/me' + '?access_token=' + userToken + '&fields=name,email,first_name,last_name'
#
# print(requests.get(urld).json())



def get_facebook_info_from_token(token):
    appLink = 'https://graph.facebook.com/oauth/access_token?client_id={}&client_secret={}&grant_type=client_credentials'.format(
        clientId, clientSecret)
    # From appLink, retrieve the second accessToken: app access_token
    appToken = requests.get(appLink).json()['access_token']
    link = 'https://graph.facebook.com/debug_token?input_token={}&access_token={}'.format(token, appToken)
    try:
        userdata = requests.get(link).json()['data']
        if userdata['app_id'] == clientId and userdata['is_valid'] == True:
            urlinfo = 'https://graph.facebook.com/v3.0/me?access_token={}&fields=name,email,first_name,last_name'.format(
                token)
            idinfo = requests.get(urlinfo).json()
            if idinfo['id'] != userdata['user_id']:
                return None
            return dict(uid=idinfo['id'],
                        email=idinfo['email'],
                        nombre=idinfo['first_name'],
                        apellido=idinfo['last_name']
                        )
        else:
            return None
    # ['user_id']
    except (ValueError, KeyError, TypeError) as error:
        print(error)
        return None
        # print(userId)


print(get_facebook_info_from_token(userToken))
