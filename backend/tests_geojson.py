import json

import geopandas as gpd

# proj = {s.strip().split('=')[0]: s.strip().split('=')[1] for s in
#        '+proj=utm +zone=21 +south +datum=WGS84 +units=m +no_defs'.split('+') if len(s) > 2}
from geopandas import GeoSeries
from shapely.geometry import Point

proj2 = dict(
    proj='utm', zone=21, south=True, datum='WGS84', units='m', no_defs=True
)
states = gpd.read_file('calles.geojson', crs='+proj=utm +zone=21 +south +datum=WGS84 +units=m +no_defs')
states.crs = {'init': '+proj=utm +zone=21 +south +datum=WGS84 +units=m +no_defs'}

p1 = GeoSeries([Point([437498.5415184938, 7201701.484534275])])
# p2 = GeoSeries([Point([438081.8269345404, 7202632.267488665])])

print(states)
# print(type(states.__geo_interface__))
# print(p1[0])
print(json.dumps(p1.buffer(500).__geo_interface__['features'][0]))
#print(type(list(states.iterfeatures())[0]))
buff = p1.buffer(5000)
#print(states.geometry.within(buff))
#print(len(states[states.geometry.within(buff)]))
#states.plot()
