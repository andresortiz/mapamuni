import shapefile

# read the shapefile
reader = shapefile.Reader("calles")
fields = reader.fields[1:]
field_names = [field[0] for field in fields]
buffer = []
for sr in reader.shapeRecords():

    atr = dict(zip(field_names, [s.decode('latin-1') if isinstance(s, bytes) else s for s in sr.record]))
    geom = sr.shape.__geo_interface__
    atr = dict(nombre=atr['nombre'])  # filtrar solo nombre
    buffer.append(dict(type="Feature", geometry=geom, properties=atr))
    print(atr)

    # write the GeoJSON file
from json import dumps

geojson = open("pyshp-demo.json", "w")
geojson.write(dumps({"type": "FeatureCollection", \
                     "features": buffer}) + "\n")
geojson.close()
