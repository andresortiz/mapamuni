import json

import geopandas as gpd
import paho.mqtt.client as mqtt
import shapefile
from geopandas import GeoSeries
from shapely.geometry import Point


def loads_json_from_file(path):
    with open(path, 'r') as fi:
        sjson = fi.read(10000000)
    return json.loads(sjson)


client = mqtt.Client()
print("Connecting to server")

# read the shapefile
reader = shapefile.Reader("calles")
fields = reader.fields[1:]
field_names = [field[0] for field in fields]
buffer = []
for sr in reader.shapeRecords():
    atr = dict(zip(field_names, [s.decode('latin-1') if isinstance(s, bytes) else s for s in sr.record]))
    geom = sr.shape.__geo_interface__
    buffer.append(dict(type="Feature", geometry=geom, properties=atr))
    #print(atr)

calles = gpd.GeoDataFrame.from_features(buffer)
print(calles)
exit()
# calles = gpd.read_file('calles.geojson', crs='+proj=utm +zone=21 +south +datum=WGS84 +units=m +no_defs')
# calles.crs = {'init': '+proj=utm +zone=21 +south +datum=WGS84 +units=m +no_defs'}

# gpd.read_file(

# for i in range(1000, 1000+len(calles)):
#     calles[]
distritos = gpd.read_file('distritos.geojson', crs='+proj=utm +zone=21 +south +datum=WGS84 +units=m +no_defs')
distritos.crs = {'init': '+proj=utm +zone=21 +south +datum=WGS84 +units=m +no_defs'}


# print(distritos.ix[0].geometry)


def sendstuff(client):
    p1 = GeoSeries([Point([437498.5415184938, 7201701.484534275])])

    client.publish('my/streets',
                   payload=json.dumps(p1.buffer(500).__geo_interface__['features'][0]), qos=0,
                   retain=False)
    # print(json.dumps(p1.buffer(500).__geo_interface__['features'][0]))

    # geog = loads_json_from_file('calles.geojson')
    #
    # print(len(geog['features']))
    #
    # for feature in geog['features']:
    #     client.publish('my/streets',
    #                    payload=json.dumps(feature), qos=0,
    #                    retain=False)
    # time.sleep(0.1)


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

    # threading.Thread(target=sendstuff, args=(client,)).start()
    client.subscribe("my/location")


def translate(value, leftMin, leftMax, rightMin, rightMax):
    # Figure out how 'wide' each range is
    leftSpan = leftMax - leftMin
    rightSpan = rightMax - rightMin

    # Convert the left range into a 0-1 range (float)
    valueScaled = float(value - leftMin) / float(leftSpan)

    # Convert the 0-1 range into a value in the right range.
    return rightMin + (valueScaled * rightSpan)


def on_message(client, userdata, msg):
    print(msg.topic + " " + str(msg.payload))

    O = json.loads(msg.payload.decode('UTF-8'))
    p1 = GeoSeries([Point(O['c'])])
    bbox = p1.buffer(translate(O['zoom'], 12, 18, 10000, 1000))

    bound = bbox.__geo_interface__['features'][0]
    # del bound['id']

    client.publish('my/streets',
                   payload=json.dumps(bound), qos=0,
                   retain=False)

    filtered = calles[calles.geometry.within(bbox.ix[0])]
    for feature in filtered.__geo_interface__['features']:
        client.publish('my/streets',
                       payload=json.dumps(feature), qos=0,
                       retain=False)


client.on_connect = on_connect

client.on_message = on_message

client.connect('192.168.183.233', 1883, 60)

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.

client.loop_forever()
