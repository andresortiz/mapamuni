from django.conf import settings
from google.oauth2 import id_token
from google.auth.transport import requests

# (Receive token by HTTPS POST)
# ...
token= "eyJhbGciOiJSUzI1NiIsImtpZCI6IjE5MjMzOTczODFkOTU3NGJiODczMjAyYTkwYzMyYjdjZWVhZWQwMjcifQ.eyJhenAiOiI3MTAyNDM4ODI3OTctZWx0Y24wcm5wYTMwY2JoNWE5bmo2Zm9sY2hjM3FwcWMuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiI3MTAyNDM4ODI3OTctZWx0Y24wcm5wYTMwY2JoNWE5bmo2Zm9sY2hjM3FwcWMuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMDM1NTUyMzE1NjI0MDYwOTk1NjciLCJoZCI6ImptdGVjaC5jb20ucHkiLCJlbWFpbCI6ImpvcmdlQGptdGVjaC5jb20ucHkiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXRfaGFzaCI6Ii1ZZWV5N0FxZGxlY0dkX2NTQTc0TlEiLCJleHAiOjE1Mjg0Mjk0NzUsImlzcyI6ImFjY291bnRzLmdvb2dsZS5jb20iLCJqdGkiOiJkNTMzZDk5Y2VhNDliYjk4OTVlMmMyZWVlZjZmNGVhOGY4ZjJkNWZjIiwiaWF0IjoxNTI4NDI1ODc1LCJuYW1lIjoiSm9yZ2UgTWVzcXVpdGEiLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDYuZ29vZ2xldXNlcmNvbnRlbnQuY29tLy1STzNCWVRobVh0ay9BQUFBQUFBQUFBSS9BQUFBQUFBQUFnQS9FQVlPd2pmaEJJRS9zOTYtYy9waG90by5qcGciLCJnaXZlbl9uYW1lIjoiSm9yZ2UiLCJmYW1pbHlfbmFtZSI6Ik1lc3F1aXRhIiwibG9jYWxlIjoiZXMtNDE5In0.DP0XLa3P7-ffU9UdQlB0krlu4SSrCaWqvCAvquJg_NR7pqzHdK135aZ4hatYZBh1i0fCtWyCeC3T0gljGkY9KiwCl_VAhCe9XM6haV27sHfM2OTO7fOsz_NO14ljnIY9WRzgkDqI7TLw7T_moR0Errw_reM9x3TH3fw2GyL2UIBJgZxwFsyp0LwAZITIW-Rl8aReBrD-kii2B8FYLfsdGVWvQj4sLZg2iN4sV06jqA_DOkZnZjhlqT73gHQ0_2i14xgbPqctIi94p0qzDUjUW5vuW8egZv7MJjLE4iYppW1hYQ1C1e29BvkM74Qk5SqxNDJlR9wU7DSwSXKE5VX7Yg"
CLIENT_ID = "710243882797-eltcn0rnpa30cbh5a9nj6folchc3qpqc.apps.googleusercontent.com"
try:
    # Specify the CLIENT_ID of the app that accesses the backend:
    idinfo = id_token.verify_oauth2_token(token, requests.Request(), CLIENT_ID)

    # Or, if multiple clients access the backend server:
    # idinfo = id_token.verify_oauth2_token(token, requests.Request())
    # if idinfo['aud'] not in [CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]:
    #     raise ValueError('Could not verify audience.')

    if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
        raise ValueError('Wrong issuer.')

    # If auth request is from a G Suite domain:
    # if idinfo['hd'] != GSUITE_DOMAIN_NAME:
    #     raise ValueError('Wrong hosted domain.')

    # ID token is valid. Get the user's Google Account ID from the decoded token.
    userid = idinfo['sub']
    print(idinfo)
except ValueError as e:
    raise e
    # Invalid token
    pass

#
# def get_google_info_from_token(token):
#     try:
#         # Specify the CLIENT_ID of the app that accesses the backend:
#         idinfo = id_token.verify_oauth2_token(token, requests.Request(), settings.GOOGLE_CLIENT_ID)
#
#         if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
#             raise ValueError('Wrong issuer.')
#
#         # If auth request is from a G Suite domain:
#         # if idinfo['hd'] != GSUITE_DOMAIN_NAME:
#         #     raise ValueError('Wrong hosted domain.')
#
#         # ID token is valid. Get the user's Google Account ID from the decoded token.
#         userid = idinfo['sub']
#         print(idinfo)
#     except ValueError:
#         # Invalid token
#         pass