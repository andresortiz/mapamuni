import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AppComponent} from './app.component';
import {AuthGuard} from './shared';

const routes: Routes = [
    {path: 'perfil', loadChildren: './perfil/perfil.module#PerfilModule'},
    {path: '', loadChildren: './layout/layout.module#LayoutModule'},
    {path: 'login', loadChildren: './login/login.module#LoginModule'},
    {path: 'signup', loadChildren: './signup/signup.module#SignupModule'},
    {path: 'error', loadChildren: './error-message/server-error/server-error.module#ServerErrorModule'},
    {path: 'access-denied', loadChildren: './error-message/access-denied/access-denied.module#AccessDeniedModule'},
    {path: 'not-found', loadChildren: './error-message/not-found/not-found.module#NotFoundModule'},
    {path: '**', redirectTo: 'not-found'}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
