import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SignupRoutingModule } from './signup-routing.module';
import { SignupComponent } from './signup.component';
import {FormsModule} from '@angular/forms';
import {AccesoService} from '../shared/services/acceso.service';
import {GrowlModule} from 'primeng/growl';
import {MessageService} from 'primeng/components/common/messageservice';
import {LocationPickerComponent} from '../shared/components/location-picker/location-picker.component';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SignupRoutingModule,
      FormsModule,
      GrowlModule,
      SharedModule
  ],
    providers: [AccesoService, MessageService],
  declarations: [SignupComponent]
})
export class SignupModule { }
