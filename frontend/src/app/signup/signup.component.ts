import {Component, OnInit} from '@angular/core';
import {routerTransition} from '../router.animations';
import {Router} from '@angular/router';
import {EmiterAccesoService} from '../shared/services/emiter-acceso.service';
import {AccesoService} from '../shared/services/acceso.service';
import {RegistroRequest} from '../shared/domain/registro-request';
import {sha256} from 'js-sha256';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    animations: [routerTransition()]
})
export class SignupComponent implements OnInit {

    nombre = '';
    apellido = '';
    documento = '';
    direccion = '';
    ctacatastral = '';
    telefono = '';
    email = '';
    passwordInicial = '';
    passwordRepetido = '';
    password: string;
    tokenFb: string;
    tokenGg: string;
    usuario = '';

    registroRequest: RegistroRequest;

    // notifaciones
    mensajeAlert: string;
    success: boolean;

    miCurrentCoord: ol.Coordinate = null;

    pickedCoords = [null, null] as ol.Coordinate;

    constructor(private router: Router,
                private emiterAccesoService: EmiterAccesoService,
                private acccesoService: AccesoService) {

        this.password = null;
        this.tokenFb = null;
        this.tokenGg = null;
        this.mensajeAlert = '';
    }

    ngOnInit() {
        this.emiterAccesoService.userInfoEmitido.subscribe(
            rta => {
                if (rta != null) {
                    this.nombre = rta.nombre;
                    this.apellido = rta.apellido;
                    this.email = rta.email;
                    this.tokenFb = rta.token_fb;
                    this.tokenGg = rta.token_gg;
                    //this.miCurrentCoord = [rta.lon, rta.lat];
                }
            },
            error => {
                console.log(error);
            }
        );
    }

    locationPicked(coords) {
        // console.log(event);
        this.pickedCoords = coords;
    }


    registrarse() {
        debugger;
        const lat = this.pickedCoords[1] ? this.pickedCoords[1] + '' : null;
        const lon = this.pickedCoords[0] ? this.pickedCoords[0] + '' : null;
        // Formar el parametro a enviar al back-end para registrarle al usuario.
        this.registroRequest = new RegistroRequest(this.nombre, this.apellido, this.telefono, lat, lon, this.ctacatastral,
            this.tokenFb, this.tokenGg, this.documento, this.usuario, this.direccion, this.email, this.password);
        console.log('==> registroRequest a enviar como parametro del service: ', this.registroRequest);

        this.acccesoService.register(this.registroRequest).subscribe(
            rta => {
                // En caso de éxito,
                if (rta.success) {
                    localStorage.setItem('perfil-ciudadano', '' + (true));
                    // redirigir al inicio de la aplicación.
                    this.router.navigate(['']);
                }
            });
    }

    /**
     * Cuando el usuario introduce una contraseña nuevamente.
     * @param {string} value
     */
    onKeyPasswordAgain(value: string) {
        if (value === this.passwordInicial) {
            // Si es vacio manda null sino manda el password hasheado.
            this.password = value === '' ? null : sha256(value);
        }
    }
}
