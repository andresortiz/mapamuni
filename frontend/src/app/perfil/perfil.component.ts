import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
    selector: 'app-perfil',
    templateUrl: './perfil.component.html',
    styleUrls: ['./perfil.component.scss']
})
export class PerfilComponent implements OnInit {

    constructor(public router: Router) {
    }

    ngOnInit() {
    }

    perfilFuncionario(como_funcionario: boolean) {
        // alert(como_funcionario);
        localStorage.setItem('perfil-ciudadano', '' + (!como_funcionario));
        this.router.navigate(['']);
    }
}
