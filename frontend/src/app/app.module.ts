import {CommonModule} from '@angular/common';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AuthGuard} from './shared';
import {MangolModule} from 'mangol';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {EmiterDatasetService} from './shared/services/emiter-dataset.service';

// AoT requires an exported function for factories
export const createTranslateLoader = (http: HttpClient) => {
    /* for development
    return new TranslateHttpLoader(
        http,
        '/start-angular/SB-Admin-BS4-Angular-6/master/dist/assets/i18n/',
        '.json'
    ); */
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};

import {
    SocialLoginModule,
    AuthServiceConfig,
    GoogleLoginProvider,
    FacebookLoginProvider,
} from 'angular-6-social-login';
import {EmiterAccesoService} from './shared/services/emiter-acceso.service';
import {GrowlModule} from 'primeng/growl';
import {MessageService} from 'primeng/components/common/messageservice';
import {IMqttServiceOptions, MqttModule} from 'ngx-mqtt';
import {EmiterDenunciaService} from './shared/services/emiter-denuncia.service';
import {EmiterIntervencionService} from './shared/services/emiter-intervencion.service';
import {SharedModule} from './shared/shared.module';
import {DatoAbiertoService} from './shared/services/dato-abierto.service';
import {LocalStorageService} from './shared/services/local-storage.service';
import {FileUploadService} from './shared/services/file-upload.service';

import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {EmiterUrlOrigenService} from './shared/services/emiter-url-origen.service';
import {FeaturesService} from './shared/services/features.service';
import {UUID} from 'angular2-uuid';
import {RtService} from "./shared/services/rt.service";
import {PERMISOS_T} from "./shared/permisos_t";


// Configs
export function getAuthServiceConfigs() {
    const config = new AuthServiceConfig(
        [
            {
                id: FacebookLoginProvider.PROVIDER_ID,
                provider: new FacebookLoginProvider('285067212034163')
            },
            {
                id: GoogleLoginProvider.PROVIDER_ID,
                provider: new GoogleLoginProvider('710243882797-eltcn0rnpa30cbh5a9nj6folchc3qpqc.apps.googleusercontent.com')
            },
        ]);
    return config;
}

// const my_uuid = UUID.UUID();
// console.log('in app', my_uuid);
export const MQTT_SERVICE_OPTIONS: IMqttServiceOptions = {
    hostname: 'rt.mapamuni.com',
    port: 443,
    path: '/mqtt',
    protocol: 'wss',
    // clientId: my_uuid
};


@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        AppRoutingModule,
        MangolModule,
        FormsModule,
        ReactiveFormsModule,
        NoopAnimationsModule,
        SocialLoginModule,
        GrowlModule,
        MqttModule.forRoot(MQTT_SERVICE_OPTIONS),
        SharedModule,
        ServiceWorkerModule.register('/ngsw-worker.js', {enabled: environment.production}),

    ],
    declarations: [AppComponent],
    providers: [
        AuthGuard,
        EmiterIntervencionService,
        EmiterDatasetService,
        EmiterAccesoService,
        EmiterDenunciaService,
        EmiterUrlOrigenService,
        {
            provide: AuthServiceConfig,
            useFactory: getAuthServiceConfigs
        },
        MessageService,
        LocalStorageService,
        DatoAbiertoService,
        FeaturesService,
        FileUploadService,
        PERMISOS_T
    ],
    bootstrap: [AppComponent],
})
export class AppModule {
}
