import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';
import {Router} from '@angular/router';
import {LocalStorageService} from '../services/local-storage.service';
import {AccesoService} from "../services/acceso.service";

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(private router: Router, private localS: LocalStorageService, private accesoService: AccesoService) {
        this.accesoService.getUsuarioLogueado({silent: true}).subscribe(
            rta => {
                if (rta.success) {
                    this.localS.set('perms', rta.result.perms);
                    this.localS.set('logged', true);
                }
            }
        );
    }

    canActivate() {
        // if (localStorage.getItem('isLoggedin')) {
        //     return true;
        // }
        if (this.localS.get<boolean>('logged')) {
            return true;
        }

        this.router.navigate(['/login']);
        return false;
    }
}
