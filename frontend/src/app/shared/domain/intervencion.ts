import {Denuncia} from './denuncia';

export class TipoIntervencion {
    constructor(public id: number,
                public nombre: string,
                public descripcion: string,
                public icono: any
    ) {}
}

export class MotivoIntervencion {
    constructor(public id: number,
                public nombre: string,
                public descripcion: string,
                public tipo: TipoIntervencion,
                public icono: any
    ) {}
}

export class EstadoIntervencion {
    constructor(public id: number,
                public nombre: string,
                public descripcion: string
    ) {}
}

export class Intervencion {
    constructor(public id: number,
                public nombre: string,
                public descripcion: string,

                public denuncia_preliminar: Denuncia,

                public interventor: any, // Usuario

                public estado: EstadoIntervencion,
                public motivo: MotivoIntervencion,

                public lat: number,
                public lon: number,

                public archivos: any,

                public fecha_creacion: any,

                public involucrados: string[]
    ) {}
}
