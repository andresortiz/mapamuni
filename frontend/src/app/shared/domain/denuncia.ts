export class TipoDenuncia {
    constructor(public id: number,
                public nombre: string,
                public descripcion: string,
                public icono: string,
                public intervenible: boolean
    ) {
    }
}

export class MotivoDenuncia {
    constructor(public id: number,
                public nombre: string,
                public descripcion: string,
                public tipo: TipoDenuncia,
                public icono: string
    ) {
    }
}

export class EstadoDenuncia {
    constructor(public id: number,
                public nombre: string,
                public descripcion: string
    ) {
    }
}

export class Denuncia {
    constructor(public id: number,
                public nombre: string,
                public descripcion: string,
                public denunciante: any, // Usuario,
                public estado: EstadoDenuncia,
                public motivo: MotivoDenuncia,
                public lat: number,
                public lon: number,
                public archivos: any[], // Archivo[]
                public fecha_creacion: any,
                public from_callcenter: boolean,
                public denunciante_ci: string,
                public intervenida: boolean
    ) {
    }
}

export class SeguimientoDenuncia {
    constructor(public id: number,
                public denuncia: Denuncia,
                public usuario: any, // Usuario
                public comentario: string,
                public fecha: any,
                public archivos: any[] // Archivo[]
    ) {
    }
}
