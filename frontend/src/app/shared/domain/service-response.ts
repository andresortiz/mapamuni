
export class ServiceResponse {
    constructor(public success: boolean,
                public messages: Message[],
                public result: any) {
    }
}

export class Message {
    constructor(public type: string,
                public msg: string) {
    }
}
