export class DatoAbiertoSistema {
    constructor(public id: number,
                public nombre: string,
                public descripcion: string,
                public nombre_archivo: string,
                public codigo: string) {
    }
}
