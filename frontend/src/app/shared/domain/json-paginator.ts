export class JsonPaginator {
    constructor(public next_page: number,
                public page_size: number,
                public prev_page: number,
                public total: number
    ) {}
}
