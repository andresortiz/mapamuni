
export class RegistroRequest {
    constructor(public nombre: string,
                public apellido: string,
                public telefono: string,
                public lat: string,
                public lon: string,
                public cta_catastral: string,
                public token_fb: string,
                public token_gg: string,
                public ci: string,
                public user_id: string,
                public direccion: string,
                public email: string,
                public password: string) {
    }
}
