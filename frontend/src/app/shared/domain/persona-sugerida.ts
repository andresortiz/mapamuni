export class PersonaSugerida {
    constructor(public ci: string,
                public nombre: string,
                public apellido: string
    ) {}
}