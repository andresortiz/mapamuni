import {ColumnaDiccionario} from './columna-diccionario';

export class Dataset {
    constructor(public id: number,
                public nombre: string,
                public descripcion: string,
                public query: string,
                public descripciones: ColumnaDiccionario[]) {
    }
}
