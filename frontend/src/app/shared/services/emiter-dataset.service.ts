import {Dataset} from '../domain/dataset';
import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class EmiterDatasetService {

    // emitir Dataset
    emitDatasetSource = new BehaviorSubject<Dataset>(null);
    datasetEmitido = this.emitDatasetSource.asObservable();

    constructor() {
    }

    /**
     * Emitir dataset
     */
    emitirDataset(change: Dataset) {
        console.log('emitir dataset:  ', change);
        this.emitDatasetSource.next(change);
    }

}