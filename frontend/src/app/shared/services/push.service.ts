import {Injectable} from '@angular/core';
import {SwPush} from '@angular/service-worker';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {MessageService} from 'primeng/components/common/messageservice';
import {BaseService} from './base.service';

@Injectable({
    providedIn: 'root'
})
export class PushService extends BaseService {

    readonly VAPID_PUBLIC_KEY = 'BG5anYdAtr-F2YmtvuoaxWf4h0Cs9bdVwgbvI6B4AtpTn7_y5MYrA-qOb2Lj8aw35KKttGTA9gq9w4SJGDz0wks';


    constructor(private swPush: SwPush, private http: HttpClient, public m_s: MessageService, public router: Router) {
        super(http, m_s);
    }


    subscribeToNotifications() {
        // console.log('sub?');
        // this.swPush.requestSubscription({
        //     serverPublicKey: this.VAPID_PUBLIC_KEY
        // }).then(sub => this.post('/subscribe_user', sub))
        //     .catch(err => console.error('Could not subscribe to notifications', err));
    }
}
