import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Intervencion} from '../domain/intervencion';


@Injectable({
    providedIn: 'root',
})
export class EmiterIntervencionService {

    emitIntervencionSource = new BehaviorSubject<Intervencion>(null);
    intervencionEmitida = this.emitIntervencionSource.asObservable();

    constructor() {
    }

    emitirIntervencion(change: Intervencion) {
        console.log('emitir intervencion: ', change);
        this.emitIntervencionSource.next(change);
    }

}

