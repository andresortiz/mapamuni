import {Injectable, Injector} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {ServiceResponse} from '../domain/service-response';
import {HttpParams} from '@angular/common/http/src/params';
import {serialize} from '../serialize';
import {MessageService} from 'primeng/components/common/messageservice';
import {Subject} from "rxjs/Subject";

@Injectable({
    providedIn: 'root'
})
export class BaseService {
    public readonly baseAPIUrl = 'https://api.mapamuni.com/api';
    public readonly uploadUrl = this.baseAPIUrl + '/archivo/subir';
    public readonly reportsUrl = this.baseAPIUrl + '/reporting/dataset';

    //protected http_s: HttpClient;

    protected opts: {
        headers?: HttpHeaders | {
            [header: string]: string | string[];
        };
        observe?: 'body';
        params?: HttpParams | {
            [param: string]: string | string[];
        };
        reportProgress?: boolean;
        responseType?: 'json';
        withCredentials?: boolean;
    } = {
        headers: new HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }),
        withCredentials: true,
        responseType: 'json',
        observe: 'body',
    };

    constructor(protected http_s: HttpClient, public m_s?: MessageService) {
        // const i = Injector.create({providers: [{provide: HttpClient, useClass: HttpClient, deps: []}]});
        // this.http_s = http_ss; // i.get(HttpClient);
    }

    doMessaging(observable: Observable<ServiceResponse>): Observable<ServiceResponse> {
        if (this.m_s) {
            const sub = new Subject<ServiceResponse>();
            observable.subscribe((rta: ServiceResponse) => {
                rta.messages.forEach(msg => {
                    this.m_s.add({severity: msg.type, summary: '', detail: msg.msg});
                });
                sub.next(rta);
                sub.complete();
            }, error => {
                console.log(error);
                this.m_s.add({
                    severity: 'error', summary: '',
                    detail: 'Error de comunicación con el servidor. Favor intente nuevamente y si ' +
                    'persiste comuniquese con el administrador.'
                });
                sub.error(error);
                sub.complete();
            });
            return sub.asObservable();
        }
        return observable;
    }

    checkOnline(): boolean {
        if (!navigator.onLine) {
            if (this.m_s) {
                this.m_s.add({
                    severity: 'warn', summary: '',
                    detail: 'Usted se encuentra fuera de linea, la accion que solicitó solo puede completarse' +
                    ' una vez que se conecte a Internet...'
                });
            }
            return false;
        }
        return true;
    }

    post(url: string, body: any): Observable<ServiceResponse> {
        if (this.checkOnline()) {
            return this.doMessaging(this.http_s.post<ServiceResponse>(this.baseAPIUrl + url, body ? JSON.stringify(body) : '', this.opts));
        } else {
            return new Observable<ServiceResponse>();
        }
    }

    get(url: string, params?: any): Observable<ServiceResponse> {
        if (this.checkOnline()) {
            if (params) {

                return this.doMessaging(this.http_s.get<ServiceResponse>(this.baseAPIUrl + url, {
                    headers: new HttpHeaders({
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    }),
                    withCredentials: true,
                    responseType: 'json',
                    observe: 'body',
                    params: serialize(params)
                }));
            } else {
                return this.doMessaging(this.http_s.get<ServiceResponse>(this.baseAPIUrl + url, this.opts));
            }
        } else {
            return new Observable<ServiceResponse>();
        }
    }

}
