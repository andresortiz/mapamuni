import {Dataset} from '../domain/dataset';
import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class EmiterAccesoService {

    // emitir Dataset
    emitUserInfoSource = new BehaviorSubject<any>(null);
    userInfoEmitido = this.emitUserInfoSource.asObservable();

    constructor() {
    }

    /**
     * Emitir dataset
     */
    emitirUserInfo(change: any) {
        console.log('emitir user info:  ', change);
        this.emitUserInfoSource.next(change);
    }

}