import {Injectable} from '@angular/core';
import {BaseService} from './base.service';
import {MessageService} from 'primeng/components/common/messageservice';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import * as ol from 'openlayers';
import {Subject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class FeaturesService extends BaseService {


    constructor(private http: HttpClient, public m_s: MessageService) {
        super(http, m_s);
    }

    getMuniFeature(desiredProj = 'EPSG:4326'): Observable<ol.Feature> {
        const sub = new Subject<ol.Feature>();
        this.get('/opendata/limite/asu').subscribe(rta => {
            if (rta.success) {
                sub.next((new ol.format.GeoJSON()).readFeature(rta.result.feature, {
                    dataProjection: rta.result.proj,
                    featureProjection: desiredProj
                }));
            }
            sub.complete();
        });
        return sub.asObservable();
    }

    getCalleFeatures(desiredProj = 'EPSG:4326'): Observable<ol.Feature> {
        const sub = new Subject<ol.Feature>();
        this.get('/opendata/calles/asu').subscribe(rta => {
            if (rta.success) {
                rta.result.features.forEach(feature => {
                    sub.next((new ol.format.GeoJSON()).readFeature(feature, {
                        dataProjection: rta.result.proj,
                        featureProjection: desiredProj
                    }));
                });
            }
            sub.complete();
        });
        return sub.asObservable();
    }

    getCalleTarifadasFeatures(desiredProj = 'EPSG:4326'): Observable<ol.Feature> {
        const sub = new Subject<ol.Feature>();
        this.get('/opendata/calles/tarifadas/get').subscribe(rta => {
            if (rta.success) {
                rta.result.features.forEach(feature => {
                    sub.next((new ol.format.GeoJSON()).readFeature(feature, {
                        dataProjection: rta.result.proj,
                        featureProjection: desiredProj
                    }));
                });
            }
            sub.complete();
        });
        return sub.asObservable();
    }

    getWifiFeatures(desiredProj = 'EPSG:4326'): Observable<ol.Feature> {
        const sub = new Subject<ol.Feature>();
        this.get('/opendata/get', {codigo: 'json_igep'}).subscribe(rta => {
            if (rta.success) {
                rta.result.forEach(item => {
                    sub.next(new ol.Feature({
                        name: item.detalle,
                        properties: {'item': item},
                        geometry: new ol.geom.Point([parseFloat(item.longitud), parseFloat(item.latitud)])
                    }));
                });
            }
            sub.complete();
        });
        return sub.asObservable();
    }

    getIntervencionesMapa(filters): Observable<ol.Feature> {
        const sub = new Subject<ol.Feature>();
        this.get('/opendata/intervenciones', filters).subscribe(rta => {
            if (rta.success) {
                rta.result.forEach(item => {
                    sub.next(new ol.Feature({
                        name: item.n,
                        properties: {'item': item},
                        geometry: new ol.geom.Point(item.coord)
                    }));
                });
            }
            sub.complete();
        });
        return sub.asObservable();
    }

    getDenunciasMapa(filters): Observable<ol.Feature> {
        const sub = new Subject<ol.Feature>();
        this.get('/opendata/denuncias', filters).subscribe(rta => {
            if (rta.success) {
                rta.result.forEach(item => {
                    sub.next(new ol.Feature({
                        name: item.n,
                        properties: {'item': item},
                        geometry: new ol.geom.Point(item.coord)
                    }));
                });
            }
            sub.complete();
        });
        return sub.asObservable();
    }

}
