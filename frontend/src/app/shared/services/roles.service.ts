import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import {Injectable} from '@angular/core';
import {BaseService} from './base.service';
import {MessageService} from 'primeng/components/common/messageservice';
import {ServiceResponse} from '../domain/service-response';
import {Rol} from '../domain/rol';

@Injectable()
export class RolesService extends BaseService {

    constructor(private http: HttpClient, public m_s: MessageService) {
        super(http, m_s);
    }

    getGrupos(): Observable<ServiceResponse> {
        return this.get('/roles/listar');
    }

    getUsuarios(): Observable<ServiceResponse> {
        return this.get('/usuarios/listar');
    }

    getPermisos(): Observable<ServiceResponse> {
        return this.get('/permisos/listar');
    }

    saveRol(body: Rol): Observable<ServiceResponse> {
        return this.post('/roles/guardar', body);
    }

    crearRol(body: Rol): Observable<ServiceResponse> {
        return this.post('/roles/crear', body);
    }

}