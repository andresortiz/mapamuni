import {Injectable} from '@angular/core';
import {BaseService} from './base.service';
import {MessageService} from 'primeng/components/common/messageservice';
import {HttpClient} from '@angular/common/http';
import {ServiceResponse} from '../domain/service-response';
import {Observable, Subject} from '../../../../node_modules/rxjs/Rx';

@Injectable({
    providedIn: 'root'
})
export class FileUploadService extends BaseService {


    constructor(private http: HttpClient, public m_s: MessageService) {
        super(http, m_s);
    }

    procesarUpload(xhr: XMLHttpRequest): number[] {
        const sub = new Subject<ServiceResponse>();
        const rta = <ServiceResponse>JSON.parse(xhr.responseText);
        this.doMessaging(sub.asObservable());
        sub.next(rta);
        sub.complete();
        return rta.success ? rta.result : [];
    }

    cancelarSubidos(body: { ids: number[] }): Observable<ServiceResponse> {
        return this.post('/archivo/cancelar', body);
    }
}
