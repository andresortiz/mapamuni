import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Denuncia} from '../domain/denuncia';


@Injectable({
    providedIn: 'root',
})
export class EmiterDenunciaService {

    // Observable sources to know if the selection of shipping service is valid or not
    emitDenunciaSource = new BehaviorSubject<Denuncia>(null);
    // Observable streams to know if the selection of shipping service is valid or not
    denunciaEmitida = this.emitDenunciaSource.asObservable();

    constructor() {
    }

    // Service message commands for Shipping Service to know if the selection of shipping service is valid or not
    emitirDenuncia(change: Denuncia) {
        console.log('emitir denuncia: ', change);
        this.emitDenunciaSource.next(change);
    }
}

