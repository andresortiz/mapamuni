import {Injectable, OnDestroy} from '@angular/core';
import {BaseService} from './base.service';
import {MessageService} from 'primeng/components/common/messageservice';
import {HttpClient} from '@angular/common/http';
import {ServiceResponse} from '../domain/service-response';
import {Observable, Subject} from '../../../../node_modules/rxjs/Rx';
import {IMqttMessage, MqttService} from "ngx-mqtt";

@Injectable()
export class RtService extends BaseService implements OnDestroy {

    subs = {};

    constructor(private http: HttpClient, public m_s: MessageService, private mqttService: MqttService) {
        super(http, m_s);
    }

    notifyMe(topic: string, subject: Subject<any>) {
        if (this.subs[topic]) {
            this.unNotifyMe(topic);
        }
        this.subs[topic] = this.mqttService.observe('mapamuni/notificaciones/' + topic).subscribe((message: IMqttMessage) => {
            try {
                const msg = JSON.parse(message.payload.toString());
                this.m_s.add(msg.msg);
                if (subject) {
                    subject.next(msg);
                }
            } catch (e) {
                console.log(e);
            }
        });
    }

    unNotifyMe(topic: string) {
        if (this.subs[topic]) {
            this.subs[topic].unsubscribe();
            this.subs[topic] = null;
        }
    }

    ngOnDestroy() {
        Object.keys(this.subs).forEach(topic => {
            this.subs[topic].forEach(subscription => {
                subscription.unsubscribe();
            });
        });
    }
}
