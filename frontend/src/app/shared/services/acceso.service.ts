import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import {Injectable} from '@angular/core';
import {BaseService} from './base.service';
import {ServiceResponse} from '../domain/service-response';
import {MessageService} from 'primeng/components/common/messageservice';
import {LocalStorageService} from "./local-storage.service";
import {Subject} from "rxjs/Subject";
import {Router} from "@angular/router";

@Injectable({
    providedIn: 'root'
})
export class AccesoService extends BaseService {


    constructor(private http: HttpClient, public m_s: MessageService, private localS: LocalStorageService, public router: Router) {
        super(http, m_s);
    }

    login(body: any): Observable<ServiceResponse> {
        return this.post('/login', body);
    }

    register(body: any): Observable<ServiceResponse> {
        return this.post('/registro', body);
    }

    logout(body: any): Observable<ServiceResponse> {
        return this.post('/logout', body);
    }

    cambiarPassword(body: any): Observable<ServiceResponse> {
        return this.post('/cambio_de_pass', body);
    }

    cambiarDatosUsuario(body: any): Observable<ServiceResponse> {
        return this.post('/perfil/guardar', body);
    }

    getPerfilUsuarioLogueado(body: any): Observable<ServiceResponse> {
        const obs = this.post('/reload_login', body);
        const sub = new Subject<ServiceResponse>();
        obs.subscribe(rta => {
            if (rta.success) {
                this.localS.set('perms', rta.result.perms);
                this.localS.set('logged', true);
            } else {
                this.router.navigate(['/login']);
            }
            sub.next(rta);
            sub.complete();
        }, sub.error);
        return sub.asObservable();
    }

    getUsuarioLogueado(body: any): Observable<ServiceResponse> {
        const obs = this.post('/reload_login', body);
        const sub = new Subject<ServiceResponse>();
        obs.subscribe(rta => {
            if (rta.success) {
                this.localS.set('perms', rta.result.perms);
                this.localS.set('logged', true);
                this.router.navigate(['']);
            } else {
                this.router.navigate(['/login']);
            }
            sub.next(rta);
            sub.complete();
        }, sub.error);
        return sub.asObservable();
    }

}