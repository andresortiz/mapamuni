import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import {Injectable} from '@angular/core';
import {BaseService} from './base.service';
import {ServiceResponse} from '../domain/service-response';
import {MessageService} from 'primeng/components/common/messageservice';
import {LocalStorageService} from "./local-storage.service";
import {Subject} from "rxjs/Subject";
import {Router} from "@angular/router";

@Injectable({
    providedIn: 'root'
})
export class DashboardService extends BaseService {


    constructor(private http: HttpClient, public m_s: MessageService, public router: Router) {
        super(http, m_s);
    }

    getUserStats(): Observable<ServiceResponse> {
        return this.get('/dashboard/stats_usuarios');
    }

    getDenunciaStats(): Observable<ServiceResponse> {
        return this.get('/dashboard/stats_denuncias');
    }

    getIntervencionStats(): Observable<ServiceResponse> {
        return this.get('/dashboard/stats_intervenciones');
    }

}