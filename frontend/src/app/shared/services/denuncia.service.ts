import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import {Injectable} from '@angular/core';
import {MessageService} from 'primeng/components/common/messageservice';
import {BaseService} from './base.service';
import {ServiceResponse} from '../domain/service-response';
import {Dataset} from '../domain/dataset';
import {Denuncia} from '../domain/denuncia';

@Injectable()
export class DenunciaService extends BaseService {

    private baseUrl = '/abml/denuncia';

    constructor(private http: HttpClient, public m_s: MessageService) {
        super(http, m_s);
    }

    getDenunciasUsuarioLogueado(page: number, xpag: number): Observable<ServiceResponse> {
        return this.get(this.baseUrl + '/listar/me', {pagina: page, resultadosxpagina: xpag});
    }

    getDenuncias(page: number, xpag: number, filters: any): Observable<ServiceResponse> {
        const params_base = {
            pagina: page,
            resultadosxpagina: xpag,
        };
        return this.get(this.baseUrl + '/listar', Object.assign(params_base, filters));
    }

    getTiposDenuncia(): Observable<ServiceResponse> {
        return this.get(this.baseUrl + '/listar/tipos');
    }

    getMotivosByTipo(idtipo: number): Observable<ServiceResponse> {
        return this.get(this.baseUrl + '/listar/motivos', {'idtipo': idtipo});
    }

    getEstadosDenuncia(): Observable<ServiceResponse> {
        return this.get(this.baseUrl + '/listar/estados');
    }

    saveMiDenuncia(body: Denuncia): Observable<ServiceResponse> {
        return this.post(this.baseUrl + '/crear/me', body);
    }

    saveDenuncia(body: Denuncia): Observable<ServiceResponse> {
        return this.post(this.baseUrl + '/crear', body);
    }

    getMiSeguimientoByDenuncia(iddenuncia: number): Observable<ServiceResponse> {
        return this.get(this.baseUrl + '/seguimiento/listar/me', {'iddenuncia': iddenuncia});
    }

    getSeguimientoByDenuncia(iddenuncia: number): Observable<ServiceResponse> {
        return this.get(this.baseUrl + '/seguimiento/listar', {'iddenuncia': iddenuncia});
    }

    seguirDenuncia(comentario: string, iddenuncia: number): Observable<ServiceResponse> {
        return this.post(this.baseUrl + '/seguimiento/crear', {iddenuncia: iddenuncia, comentario: comentario});
    }

    seguirMiDenuncia(comentario: string, iddenuncia: number): Observable<ServiceResponse> {
        return this.post(this.baseUrl + '/seguimiento/crear/me', {iddenuncia: iddenuncia, comentario: comentario});
    }

    finalizarDenuncia(iddenuncia: number): Observable<ServiceResponse> {
        return this.post(this.baseUrl + '/finalizar', {iddenuncia: iddenuncia});
    }

    revisarDenuncia(iddenuncia: number): Observable<ServiceResponse> {
        return this.post(this.baseUrl + '/revisar', {iddenuncia: iddenuncia});
    }


}