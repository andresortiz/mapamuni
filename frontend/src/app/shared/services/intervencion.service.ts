import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import {Injectable} from '@angular/core';
import {MessageService} from 'primeng/components/common/messageservice';
import {BaseService} from './base.service';
import {ServiceResponse} from '../domain/service-response';
import {Denuncia} from '../domain/denuncia';
import {Intervencion} from '../domain/intervencion';
import {Subject} from "rxjs/index";
import * as ol from "openlayers";

@Injectable()
export class IntervencionService extends BaseService {

    private baseUrl = '/abml/intervencion';

    constructor(private http: HttpClient, public m_s: MessageService) {
        super(http, m_s);
    }

    getIntervenciones(page: number, xpag: number, filters: any): Observable<ServiceResponse> {
        const params_base = {
            pagina: page,
            resultadosxpagina: xpag,
        };
        return this.get(this.baseUrl + '/listar', Object.assign(params_base, filters));
    }

    getMisIntervenciones(page: number, xpag: number): Observable<ServiceResponse> {
        return this.get(this.baseUrl + '/listar/me', {pagina: page, resultadosxpagina: xpag});
    }

    getTiposIntervencion(): Observable<ServiceResponse> {
        return this.get(this.baseUrl + '/listar/tipos');
    }

    getMotivosByTipo(idtipo: number): Observable<ServiceResponse> {
        return this.get(this.baseUrl + '/listar/motivos', {'idtipo': idtipo});
    }

    getEstadosIntervencion(): Observable<ServiceResponse> {
        return this.get(this.baseUrl + '/listar/estados');
    }

    saveMiIntervencion(body: Intervencion): Observable<ServiceResponse> {
        return this.post(this.baseUrl + '/crear/me', body);
    }

    finalizarIntervencion(idintervencion: number): Observable<ServiceResponse> {
        return this.post(this.baseUrl + '/finalizar', {idintervencion: idintervencion});
    }



}