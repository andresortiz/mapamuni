import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';


@Injectable({
    providedIn: 'root',
})
export class EmiterUrlOrigenService {

    // Observable sources to know if the selection of shipping service is valid or not
    emitUrlOrigenSource = new BehaviorSubject<string>(null);
    // Observable streams to know if the selection of shipping service is valid or not
    urlOrigenEmitida = this.emitUrlOrigenSource.asObservable();

    constructor() {
    }

    // Service message commands for Shipping Service to know if the selection of shipping service is valid or not
    emitirUrlOrigen(change: string) {
        console.log('emitir denuncia: ', change);
        this.emitUrlOrigenSource.next(change);
    }
}

