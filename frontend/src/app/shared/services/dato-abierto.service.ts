import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import {Injectable} from '@angular/core';
import {BaseService} from './base.service';
import {ServiceResponse} from '../domain/service-response';
import {MessageService} from 'primeng/components/common/messageservice';
import {Subject} from "rxjs/index";

@Injectable({
    providedIn: 'root'
})
export class DatoAbiertoService extends BaseService {

    private baseUrlAbml = '/opendata';

    constructor(private http: HttpClient, public m_s: MessageService) {
        super(http, m_s);
    }

    setArchivoActualizable(body: any): Observable<ServiceResponse> {
        return this.post(this.baseUrlAbml + '/archivo/set', body);
    }

    getDatosActualizables(): Observable<ServiceResponse> {
        return this.get(this.baseUrlAbml + '/listar');
    }

    getDatoAbierto(codigo: string): Observable<ServiceResponse> {
        return this.get(this.baseUrlAbml + '/get', {codigo: codigo});
    }

    getCalleTarifadaIds(): Observable<number[]> {
        const sub = new Subject<number[]>();
        this.get('/opendata/get', {codigo: 'calles_tarifadas'}).subscribe(rta => {
            if (rta.success) {
                sub.next(rta.result);
            }
            sub.complete();
        }, () => sub.complete());
        return sub.asObservable();
    }

    setCalleTarifadaIds(ids: number[]): Observable<ServiceResponse> {
        return this.post(this.baseUrlAbml + '/calles/tarifadas/set', {ids: ids});
    }

}