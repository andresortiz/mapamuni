import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Dataset} from '../domain/dataset';
import {Observable} from 'rxjs/Rx';
import {Injectable} from '@angular/core';
import {BaseService} from './base.service';
import {ServiceResponse} from '../domain/service-response';
import {MessageService} from 'primeng/components/common/messageservice';

@Injectable()
export class DatasetService extends BaseService{

    private baseUrlAbml = '/abml/dataset';

    constructor(private http: HttpClient, public m_s: MessageService) {
        super(http, m_s);
    }

    saveDataset(body: Dataset): Observable<ServiceResponse> {
        return this.post(this.baseUrlAbml + '/guardar', body);
    }

    getDatasets(): Observable<ServiceResponse> {
        return this.get(this.baseUrlAbml + '/listar');
    }

}