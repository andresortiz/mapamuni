import {Attribute, Component} from '@angular/core';

@Component({
    selector: 'app-now',
    template: `<span>{{datex | date: 'HH:mm:ss dd/MM/yyyy'}}</span>`
})
export class NowComponent {
    public datex;

    constructor() {
        this.datex = new Date();

        setInterval(() => {
            this.datex = new Date();
        }, 1000);
    }

}
