import {LocationPickerComponent} from './components/location-picker/location-picker.component';
import {NgModule} from '@angular/core';
import {MangolModule} from 'mangol';
import {FeaturesService} from './services/features.service';
import {NowComponent} from "./components/now.component";
import {CommonModule} from "@angular/common";

@NgModule({
    imports: [
        // SomeModule
        MangolModule,
        CommonModule
    ],
    declarations: [
        LocationPickerComponent, NowComponent
    ],
    exports: [
        LocationPickerComponent, NowComponent
    ],
    providers: [FeaturesService]
})

export class SharedModule {
}
