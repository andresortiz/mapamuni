import {Injectable} from "@angular/core";

@Injectable({
    providedIn: 'root'
})
export class PERMISOS_T {
    VER_PERMISOS = 'seePerm'
    EDITAR_PERMISOS = 'editPerm'
    CREAR_ROLES = 'crearRol'
    BORRAR_ROLES = 'delRol'

    VER_DATASETS = 'listarDataset'
    EXPORTAR_DATASETS = 'exportarDataset'
    GUARDAR_DATASETS = 'guardarDataset'

    SET_CALLES_TARIFADAS = 'setTarifadas'
    UPLOAD_OPENDATA = 'uploadOpenData'

    LISTAR_DENUNCIAS = 'listDenuncia'
    CREAR_DENUNCIA_TELEFONICA = 'crearDenunciaTelefonica'
    VER_SEGUIMIENTO_DENUNCIA = 'verSeguimientoDenuncia'
    SEGUIR_DENUNCIA = 'seguirDenuncia'
    FINALIZAR_DENUNCIA = 'finalizarDenuncia'

    LISTAR_INTERVENCION = 'listIntervencion'
    CREAR_INTERVENCION = 'intervenir'
    FINALIZAR_INTERVENCION = 'finalizarIntervencion'
}