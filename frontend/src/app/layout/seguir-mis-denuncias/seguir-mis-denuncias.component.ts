import {Component, OnDestroy, OnInit} from '@angular/core';
import {EmiterDenunciaService} from '../../shared/services/emiter-denuncia.service';
import {Denuncia, SeguimientoDenuncia} from '../../shared/domain/denuncia';
import {DenunciaService} from '../../shared/services/denuncia.service';
import {Subject} from 'rxjs/Subject';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Subscription} from 'rxjs/Subscription';
import {Router} from '@angular/router';

@Component({
    selector: 'app-seguir-mis-denuncias',
    templateUrl: './seguir-mis-denuncias.component.html',
    styleUrls: ['./seguir-mis-denuncias.component.scss']
})
export class SeguirMisDenunciasComponent implements OnInit, OnDestroy {

    denuncia: Denuncia;
    nombre: string;
    descripcion: string;
    estado: string;
    fechaCreacion: any;
    motivo: string;
    tipo: string;

    listaSeguimiento: SeguimientoDenuncia[];
    incoming: boolean;
    denunciante: any;

    nuevo_comentario = '';

    private seguirDenunciaSuscripcion: Subscription;
    private seguimientoDenunciaSuscripcion: Subscription;

    constructor(private emiterDenunciaService: EmiterDenunciaService,
                private denunciaService: DenunciaService, private router: Router) {
        this.denuncia = null;
        this.listaSeguimiento = [];
        this.denunciante = {nombre: '', apellido: ''};
        this.incoming = false;
    }

    ngOnInit() {
        console.log('oninit');
        this.seguirDenunciaSuscripcion = this.emiterDenunciaService.denunciaEmitida.subscribe(
            resp => {
                console.log('=> denuncia recibida: ', resp);
                if (resp != null) {
                    this.denuncia = resp;
                    this.nombre = this.denuncia.nombre;
                    this.denunciante = this.denuncia.denunciante;
                    this.descripcion = this.denuncia.descripcion;
                    this.estado = this.denuncia.estado.nombre;
                    this.fechaCreacion = this.denuncia.fecha_creacion;
                    this.motivo = this.denuncia.motivo.nombre;
                    this.tipo = this.denuncia.motivo.tipo.nombre;
                    this.obtenerSeguimiento(this.denuncia.id);
                } else {
                    this.router.navigate(['/listar-mis-denuncias']);
                }
            }
        );
    }

    ngOnDestroy() {
        if (this.seguirDenunciaSuscripcion != null) {
            this.seguirDenunciaSuscripcion.unsubscribe();
        }
        if (this.seguimientoDenunciaSuscripcion != null) {
            this.seguimientoDenunciaSuscripcion.unsubscribe();
        }
    }

    agregarSeguimiento() {
        console.log('=> seguir denuncia llamado ');
        this.denunciaService.seguirMiDenuncia(this.nuevo_comentario, this.denuncia.id)
            .subscribe(rta => {
                if (rta.success) {
                    this.nuevo_comentario = '';
                    this.obtenerSeguimiento(this.denuncia.id);
                }
            });
    }


    obtenerSeguimiento(idDenuncia: number) {
        console.log('=> obtenerSeguimiento de la denuncia: ', idDenuncia);
        this.seguimientoDenunciaSuscripcion = this.denunciaService.getMiSeguimientoByDenuncia(idDenuncia).subscribe(
            resp => {
                console.log('=> seguimiento recibido: ', resp);
                if (resp != null && resp.success === true) {
                    (<SeguimientoDenuncia[]>resp.result).forEach(seg => {
                        seg.fecha = new Date(seg.fecha);
                    });
                    this.listaSeguimiento = resp.result;

                }
            }
        );
    }

    cancelar() {
        // Redirigir al listado
        this.router.navigate(['/listar-mis-denuncias']);
    }

    finalizarDenuncia() {
        this.denunciaService.finalizarDenuncia(this.denuncia.id).subscribe(
            rta => {
                if (rta.success) {
                    console.log('finalizó con exito');
                }
            }
        );
    }

}
