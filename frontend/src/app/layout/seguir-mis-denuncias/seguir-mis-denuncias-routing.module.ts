import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SeguirMisDenunciasComponent} from './seguir-mis-denuncias.component';

const routes: Routes = [
    {
        path: '', component: SeguirMisDenunciasComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [RouterModule]
})
export class SeguirMisDenunciasRoutingModule {
}
