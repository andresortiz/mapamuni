import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {SeguirMisDenunciasComponent} from './seguir-mis-denuncias.component';

describe('SeguirDenunciaComponent', () => {
  let component: SeguirMisDenunciasComponent;
  let fixture: ComponentFixture<SeguirMisDenunciasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeguirMisDenunciasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeguirMisDenunciasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
