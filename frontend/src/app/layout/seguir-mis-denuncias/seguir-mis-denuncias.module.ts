import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DenunciaService} from '../../shared/services/denuncia.service';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../../shared/shared.module';
import {SeguirMisDenunciasComponent} from './seguir-mis-denuncias.component';
import {SeguirMisDenunciasRoutingModule} from './seguir-mis-denuncias-routing.module';

@NgModule({
    imports: [CommonModule, SeguirMisDenunciasRoutingModule, FormsModule, SharedModule],
    declarations: [SeguirMisDenunciasComponent],
    providers: [DenunciaService]
})
export class SeguirMisDenunciasModule {
}
