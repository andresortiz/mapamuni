import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DetalleDenunciaComponent} from './detalle-denuncia.component';

const routes: Routes = [
    {
        path: '', component: DetalleDenunciaComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DetalleDenunciaRoutingModule { }
