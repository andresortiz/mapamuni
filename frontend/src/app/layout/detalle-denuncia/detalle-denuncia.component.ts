import {Component, OnDestroy, OnInit} from '@angular/core';
import {EmiterDenunciaService} from '../../shared/services/emiter-denuncia.service';
import {Subscription} from 'rxjs/Subscription';
import {EmiterUrlOrigenService} from '../../shared/services/emiter-url-origen.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-detalle-denuncia',
    templateUrl: './detalle-denuncia.component.html',
    styleUrls: ['./detalle-denuncia.component.scss']
})
export class DetalleDenunciaComponent implements OnInit, OnDestroy {

    nombre: string;
    descripcion: string;
    tipoDenuncia: string;
    motivoDenuncia: string;
    denunciante: string;
    estado: string;
    fechaCreacion: string;

    private getDenunciaSuscripcion: Subscription;

    constructor(private emiterDenunciaService: EmiterDenunciaService,
                private emiterUrlOrigenService: EmiterUrlOrigenService,
                private router: Router) {
    }

    ngOnInit() {
        // Recibir denuncia
        this.getDenunciaSuscripcion = this.emiterDenunciaService.denunciaEmitida.subscribe(
            rta => {
                console.log('=> denuncia recibida: ', rta);
                if (rta != null) {
                    this.tipoDenuncia = rta.motivo.tipo.nombre;
                    this.motivoDenuncia = rta.motivo.nombre;
                    this.nombre = rta.nombre;
                    this.descripcion = rta.descripcion;
                    this.denunciante = rta.denunciante.nombre + ' ' + rta.denunciante.apellido;
                    this.estado = rta.estado.nombre;
                    this.fechaCreacion = rta.fecha_creacion;
                    // this.obtenerSeguimiento(this.denuncia.id);
                }
            },
            error => {
                console.log(error);
            }
        );
    }

    ngOnDestroy() {
        if (this.getDenunciaSuscripcion != null) {
            this.getDenunciaSuscripcion.unsubscribe();
        }
    }

    volver() {
        // redirigir a la pagina de donde vino (crear-editar-mi-intervencion o detalle-intervencion)
        this.emiterUrlOrigenService.urlOrigenEmitida.subscribe(
            rta => {
                if (rta != null) {
                    this.router.navigate([rta]);
                } else {
                    this.router.navigate(['']);
                }
            },
            error => {
                console.log(error)
            }
        );
        // this.emiterIntervencionService.emitirIntervencion(intervencion);
        // this.router.navigate(['/crear-editar-mi-intervencion']);
    }

}
