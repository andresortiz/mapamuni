import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MangolModule} from 'mangol';
import {DenunciaService} from '../../shared/services/denuncia.service';
import {FormsModule} from '@angular/forms';
import {FileUploadService} from '../../shared/services/file-upload.service';
import {FileUploadModule} from 'primeng/primeng';
import {SharedModule} from '../../shared/shared.module';
import {DetalleDenunciaComponent} from './detalle-denuncia.component';
import {DetalleDenunciaRoutingModule} from './detalle-denuncia-routing.module';

@NgModule({
    imports: [CommonModule, DetalleDenunciaRoutingModule, MangolModule, FormsModule, FileUploadModule, SharedModule],
    declarations: [DetalleDenunciaComponent],
    providers: [DenunciaService, FileUploadService]
})
export class DetalleDenunciaModule {
}
