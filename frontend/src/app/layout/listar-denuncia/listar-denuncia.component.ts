import {Component, OnDestroy, OnInit} from '@angular/core';
import {DenunciaService} from '../../shared/services/denuncia.service';
import {Router} from '@angular/router';
import {Denuncia, TipoDenuncia} from '../../shared/domain/denuncia';
import {EmiterDenunciaService} from '../../shared/services/emiter-denuncia.service';
import {RtService} from "../../shared/services/rt.service";
import {Subject} from "rxjs/Subject";
import {PushService} from "../../shared/services/push.service";

@Component({
    selector: 'app-listar-denuncia',
    templateUrl: './listar-denuncia.component.html',
    styleUrls: ['./listar-denuncia.component.scss']
})
export class ListarDenunciaComponent implements OnInit, OnDestroy {

    denuncias: Denuncia[];

    // Paginación
    // Total de registros
    cnt_denuncias = 1;

    // Filtros
    tiposDenuncia: any[];
    motivosDenuncia: any[];
    estados: any[];

    lastlazy = null;

    constructor(private router: Router,
                private denunciaService: DenunciaService,
                private emiterDenunciaService: EmiterDenunciaService, private rt: RtService, private pushService: PushService) {
    }

    ngOnInit() {
        this.denuncias = [];
        // Obtener todas las denuncias paginadas con parámetros por defecto incialmente
        this.getDenuncias(1, 10);

        // Cargar los dropdown de filtros
        this.tiposDenuncia = [];
        this.getTiposDenuncia();
        this.motivosDenuncia = [];
        this.estados = [];
        this.getEstadosDenuncia();

        const sub = new Subject<any>();
        sub.subscribe(msg => {
            if (msg.reload) {
                if (this.lastlazy) {
                    this.loadDenunciasLazy(this.lastlazy);
                } else {
                    this.getDenuncias(1, 10);
                }
            }
        });
        this.lastlazy = null;
        this.rt.notifyMe('nuevas-denuncias', sub);
        this.pushService.subscribeToNotifications();
    }

    ngOnDestroy() {
        this.rt.unNotifyMe('nuevas-denuncias');
    }

    getDenuncias(page, xpag, filters?) {
        this.denunciaService.getDenuncias(page, xpag, filters).subscribe(
            resp => {
                if (resp != null && resp.success === true) {
                    console.log(resp);
                    this.denuncias = resp.result.objects;
                    this.cnt_denuncias = resp.result.count;
                }
            }
        );
    }

    agregarDenunciaTelefonica() {
        this.router.navigate(['/crear-denuncia-telefonica']);
    }

    verDetalleDenunciaTelefonica(denuncia: Denuncia) {
        this.router.navigate(['/seguir-denuncia']);
        // Emitir a otro componente
        this.emiterDenunciaService.emitirDenuncia(denuncia);
    }

    loadDenunciasLazy(event) {
        const filters = {};
        if ((<any>event.filters).id_tipo) {
            (<any>filters).id_tipo = event.filters.id_tipo.value;
            this.getMotivosByTipo((<any>filters).id_tipo);
        }
        if ((<any>event.filters).id_motivo) {
            (<any>filters).id_motivo = event.filters.id_motivo.value;
        }
        if ((<any>event.filters).nombre) {
            (<any>filters).nombre = event.filters.nombre.value;
        }
        if ((<any>event.filters).id_estado) {
            (<any>filters).id_estado = event.filters.id_estado.value;
        }
        console.log('event de paginacion: ', event);
        this.getDenuncias(((event.first / event.rows) >> 0) + 1, event.rows, filters);
        this.lastlazy = event;
    }

    intervenir(denuncia) {
        // emitir la denuncia a la cual quiero vincular la intervencion luego al crear este último
        this.emiterDenunciaService.emitirDenuncia(denuncia);
        // redirigir a crear intervencion
        this.router.navigate(['/crear-editar-mi-intervencion']);
    }


    getTiposDenuncia() {
        this.denunciaService.getTiposDenuncia().subscribe(
            resp => {
                if (resp.success) {
                    this.tiposDenuncia = resp.result;
                    this.tiposDenuncia.unshift({nombre: 'TODOS'});
                }
            }
        );
    }

    getMotivosByTipo(tipoSeleccionado: number): void {
        this.denunciaService.getMotivosByTipo(tipoSeleccionado).subscribe(
            resp => {
                if (resp.success) {
                    this.motivosDenuncia = resp.result;
                    this.motivosDenuncia.unshift({nombre: 'TODOS'});
                }
            }
        );
    }

    getEstadosDenuncia() {
        this.denunciaService.getEstadosDenuncia().subscribe(
            resp => {
                if (resp.success) {
                    this.estados = resp.result;
                    this.estados.unshift({nombre: 'TODOS'});
                }
            }
        );
    }

}
