import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListarDenunciaComponent} from './listar-denuncia.component';

const routes: Routes = [
    {
        path: '', component: ListarDenunciaComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [RouterModule]
})
export class ListarDenunciaRoutingModule {
}
