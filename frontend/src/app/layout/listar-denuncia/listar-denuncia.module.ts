import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {DenunciaService} from '../../shared/services/denuncia.service';
import {ListarDenunciaRoutingModule} from './listar-denuncia-routing.module';
import {ListarDenunciaComponent} from './listar-denuncia.component';
import {DataTableModule, PaginatorModule} from 'primeng/primeng';
import {DataViewModule} from 'primeng/dataview';
import {TableModule} from 'primeng/table';
import {RtService} from '../../shared/services/rt.service';

@NgModule({
    imports: [
        CommonModule, ListarDenunciaRoutingModule, FormsModule, NgbModule.forRoot(),
        PaginatorModule, DataViewModule, DataTableModule
    ],
    declarations: [ListarDenunciaComponent],
    providers: [DenunciaService, RtService]
})
export class ListarDenunciaModule {
}
