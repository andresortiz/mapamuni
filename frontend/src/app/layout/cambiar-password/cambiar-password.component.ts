import {Component, OnInit} from '@angular/core';
import {routerTransition} from '../../router.animations';
import {HttpClient} from '@angular/common/http';
import * as proj4x from 'proj4';
import {sha256} from 'js-sha256';
import {AccesoService} from '../../shared/services/acceso.service';
import {Router} from '@angular/router';

const proj4 = (proj4x as any).default;

@Component({
    selector: 'app-cambiar-password',
    templateUrl: './cambiar-password.component.html',
    styleUrls: ['./cambiar-password.component.scss'],
    animations: [routerTransition()]
})
export class CambiarPasswordComponent implements OnInit {

    passwordActual: string;
    passwordNuevo: string;
    passwordRepetido: string;
    password: string;

    constructor(public http: HttpClient,
                private accesoService: AccesoService,
                private router: Router) {
    }

    ngOnInit() {
    }

    actualizarPassword() {
        this.accesoService.cambiarPassword({currentp: sha256(this.passwordActual), newp: this.password}).subscribe(
            rta => {
                // En caso de éxito,
                if (rta.success) {
                    console.log('cambio exitosamente');
                }
            });
    }

    cancelar() {
        this.router.navigate(['']);
    }

    /**
     * Cuando el usuario introduce una contraseña nuevamente.
     * @param {string} value
     */
    onKeyPasswordAgain(value: string) {
        if (value === this.passwordNuevo) {
            // Si es vacio manda null sino manda el password hasheado.
            this.password = value === '' ? null : sha256(value);
        }
    }

}
