import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PageHeaderModule} from './../../shared';
import {MangolModule} from 'mangol';
import {CambiarPasswordRoutingModule} from './cambiar-password-routing.module';
import {CambiarPasswordComponent} from './cambiar-password.component';
import {FormsModule} from '@angular/forms';

@NgModule({
    imports: [CommonModule, CambiarPasswordRoutingModule, PageHeaderModule, MangolModule, FormsModule],
    declarations: [CambiarPasswordComponent]
})
export class CambiarPasswordModule {
}
