import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeguirDenunciaComponent } from './seguir-denuncia.component';

describe('SeguirDenunciaComponent', () => {
  let component: SeguirDenunciaComponent;
  let fixture: ComponentFixture<SeguirDenunciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeguirDenunciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeguirDenunciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
