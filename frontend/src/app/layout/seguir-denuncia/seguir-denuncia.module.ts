import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DenunciaService} from '../../shared/services/denuncia.service';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../../shared/shared.module';
import {SeguirDenunciaRoutingModule} from './seguir-denuncia-routing.module';
import {SeguirDenunciaComponent} from './seguir-denuncia.component';

@NgModule({
    imports: [CommonModule, SeguirDenunciaRoutingModule, FormsModule, SharedModule],
    declarations: [SeguirDenunciaComponent],
    providers: [DenunciaService]
})
export class SeguirDenunciaModule {
}
