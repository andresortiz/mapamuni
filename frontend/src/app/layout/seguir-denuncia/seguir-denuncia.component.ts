import {Component, OnDestroy, OnInit} from '@angular/core';
import {DenunciaService} from '../../shared/services/denuncia.service';
import {Denuncia, SeguimientoDenuncia} from '../../shared/domain/denuncia';
import {Router} from '@angular/router';
import {EmiterDenunciaService} from '../../shared/services/emiter-denuncia.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-seguir-denuncia',
  templateUrl: './seguir-denuncia.component.html',
  styleUrls: ['./seguir-denuncia.component.scss']
})
export class SeguirDenunciaComponent implements OnInit, OnDestroy {

    denuncia: Denuncia;
    nombre: string;
    descripcion: string;
    estado: string;
    fechaCreacion: any;
    motivo: string;
    tipo: string;

    listaSeguimiento: SeguimientoDenuncia[];
    incoming: boolean;
    denunciante: any;

    nuevo_comentario = '';

    private seguirDenunciaSuscripcion: Subscription;
    private seguimientoDenunciaSuscripcion: Subscription;

    constructor(private emiterDenunciaService: EmiterDenunciaService,
                private denunciaService: DenunciaService, private router: Router) {
        this.denuncia = null;
        this.listaSeguimiento = [];
        this.denunciante = {nombre: '', apellido: ''};
        this.incoming = false;
    }

    ngOnInit() {
        this.seguirDenunciaSuscripcion = this.emiterDenunciaService.denunciaEmitida.subscribe(
            resp => {
                console.log('denuncia recibida: ', resp);
                debugger;
                if (resp != null) {
                    this.denuncia = resp;
                    /* Verificar si ya se encuentra en el estado correcto, sino pasar al estado 'Revisado'
                     por entrar en esta página por primera vez*/
                    this.denunciaService.revisarDenuncia(this.denuncia.id);

                    this.nombre = this.denuncia.nombre;
                    this.denunciante = this.denuncia.denunciante;
                    this.descripcion = this.denuncia.descripcion;
                    this.estado = this.denuncia.estado.nombre;
                    this.fechaCreacion = this.denuncia.fecha_creacion;
                    this.motivo = this.denuncia.motivo.nombre;
                    this.tipo = this.denuncia.motivo.tipo.nombre;
                    this.obtenerSeguimiento(this.denuncia.id);
                } else {
                    this.router.navigate(['/listar-denuncia']);
                }
            }
        );
    }

    ngOnDestroy() {
        if (this.seguirDenunciaSuscripcion != null) {
            this.seguirDenunciaSuscripcion.unsubscribe();
        }
        if (this.seguimientoDenunciaSuscripcion != null) {
            this.seguimientoDenunciaSuscripcion.unsubscribe();
        }
    }

    agregarSeguimiento() {
        console.log('=> seguir denuncia llamado ');
        this.denunciaService.seguirDenuncia(this.nuevo_comentario, this.denuncia.id)
            .subscribe(rta => {
                if (rta.success) {
                    this.nuevo_comentario = '';
                    this.obtenerSeguimiento(this.denuncia.id);
                }
            });
    }


    obtenerSeguimiento(idDenuncia: number) {
        console.log('=> obtenerSeguimiento de la denuncia: ', idDenuncia);
        this.seguimientoDenunciaSuscripcion = this.denunciaService.getSeguimientoByDenuncia(idDenuncia).subscribe(
            resp => {
                console.log('=> seguimiento recibido: ', resp);
                if (resp != null && resp.success === true) {
                    (<SeguimientoDenuncia[]>resp.result).forEach(seg => {
                        seg.fecha = new Date(seg.fecha);
                    });
                    this.listaSeguimiento = resp.result;

                }
            }
        );
    }

    cancelar() {
        // Redirigir al listado
        this.router.navigate(['/listar-denuncia']);
    }

    finalizarDenuncia() {
        this.denunciaService.finalizarDenuncia(this.denuncia.id).subscribe(
            rta => {
                if (rta.success) {
                    console.log('finalizó con exito');
                }
            }
        );
    }

}
