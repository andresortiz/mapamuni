import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SeguirDenunciaComponent} from './seguir-denuncia.component';

const routes: Routes = [
    {
        path: '', component: SeguirDenunciaComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [RouterModule]
})
export class SeguirDenunciaRoutingModule {
}
