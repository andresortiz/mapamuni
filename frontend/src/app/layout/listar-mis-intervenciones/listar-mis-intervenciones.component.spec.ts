import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarMisIntervencionesComponent } from './listar-mis-intervenciones.component';

describe('ListarMisIntervencionesComponent', () => {
  let component: ListarMisIntervencionesComponent;
  let fixture: ComponentFixture<ListarMisIntervencionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarMisIntervencionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarMisIntervencionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
