import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListarMisIntervencionesComponent} from './listar-mis-intervenciones.component';

const routes: Routes = [
    {
        path: '', component: ListarMisIntervencionesComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [RouterModule]
})
export class ListarMisIntervencionesRoutingModule {
}
