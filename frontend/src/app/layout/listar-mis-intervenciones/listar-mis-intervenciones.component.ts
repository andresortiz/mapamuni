import { Component, OnInit } from '@angular/core';
import {Intervencion} from '../../shared/domain/intervencion';
import {IntervencionService} from '../../shared/services/intervencion.service';
import {Router} from '@angular/router';
import {EmiterIntervencionService} from '../../shared/services/emiter-intervencion.service';

@Component({
  selector: 'app-listar-mis-intervenciones',
  templateUrl: './listar-mis-intervenciones.component.html',
  styleUrls: ['./listar-mis-intervenciones.component.scss']
})
export class ListarMisIntervencionesComponent implements OnInit {

    intervenciones: any[];

    // Paginación
    // Total de registros
    cnt_denuncias = 1;
    // Filas
    xpag = 10;

    constructor(private emiterIntervencionService: EmiterIntervencionService,
                private intervencionService: IntervencionService,
                private router: Router) {}

    ngOnInit() {
        this.intervenciones = [];
        // Obtener mis intervenciones paginadas con parámetros por defecto incialmente
        this.getIntervenciones(1, 10);
    }

    getIntervenciones(page, xpag) {
        this.intervencionService.getMisIntervenciones(page, xpag).subscribe(
            resp => {
                if (resp != null && resp.success === true) {
                    this.intervenciones = resp.result.objects;
                    this.cnt_denuncias = resp.result.count;

                    this.intervenciones.forEach(i => {
                        if (i.denuncia == null) {
                            i.denuncia = {nombre: ''};
                        }
                    });
                }
            }
        );
    }

    paginate(event) {
        console.log('paginate desde vista: ', event);
        // Obtener mis intervenciones paginadas
        this.getIntervenciones(event.page + 1, event.rows);
    }

    agregarMiIntervencion() {
        // Emitir que limpie el formulario antes de redirigirse al mismo
        this.emiterIntervencionService.emitirIntervencion(null);
        this.router.navigate(['/crear-editar-mi-intervencion']);
    }

    editarMiIntervencion(intervencion: Intervencion) {
        this.emiterIntervencionService.emitirIntervencion(intervencion);
        this.router.navigate(['/crear-editar-mi-intervencion']);
    }
}
