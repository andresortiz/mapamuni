import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ListarMisIntervencionesComponent} from './listar-mis-intervenciones.component';
import {ListarMisIntervencionesRoutingModule} from './listar-mis-intervenciones-routing.module';
import {IntervencionService} from '../../shared/services/intervencion.service';
import {PaginatorModule} from 'primeng/primeng';

@NgModule({
    imports: [
        CommonModule, ListarMisIntervencionesRoutingModule, FormsModule, NgbModule.forRoot(), PaginatorModule
    ],
    declarations: [ListarMisIntervencionesComponent],
    providers: [IntervencionService]
})
export class ListarMisIntervencionesModule {
}
