import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListarIntervencionComponent} from './listar-intervencion.component';

const routes: Routes = [
    {
        path: '', component: ListarIntervencionComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ListarIntervencionRoutingModule { }
