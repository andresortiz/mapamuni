import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {routerTransition} from '../../router.animations';
import {Router} from '@angular/router';
import {IntervencionService} from '../../shared/services/intervencion.service';
import {Intervencion} from '../../shared/domain/intervencion';
import {EmiterIntervencionService} from '../../shared/services/emiter-intervencion.service';
import {debug} from 'util';
import {Subject} from "rxjs/Subject";
import {RtService} from "../../shared/services/rt.service";

@Component({
    selector: 'app-listar-intervencion',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './listar-intervencion.component.html',
    styleUrls: ['./listar-intervencion.component.scss'],
    animations: [routerTransition()]
})
export class ListarIntervencionComponent implements OnInit, OnDestroy {

    intervenciones: any[];

    // Paginación
    // Total de registros
    cnt_intervenciones = 1;

    // Filtros
    tiposIntervencion: any[];
    motivosIntervencion: any[];
    estados: any[];

    lastlazy = null;

    constructor(private emiterIntervencionService: EmiterIntervencionService,
                private intervencionService: IntervencionService,
                private router: Router, private rt: RtService) {
    }

    ngOnInit() {
        this.intervenciones = [];
        // Obtener todas las intervenciones paginadas con parámetros por defecto incialmente
        this.getIntervenciones(1, 10);

        // Cargar los dropdown de filtros
        this.tiposIntervencion = [];
        this.getTiposIntervencion();
        this.motivosIntervencion = [];
        this.estados = [];
        this.getEstadosIntervencion();

        const sub = new Subject<any>();
        sub.subscribe(msg => {
            if (msg.reload) {
                if (this.lastlazy) {
                    this.loadIntervencionesLazy(this.lastlazy);
                } else {
                    this.getIntervenciones(1, 10);
                }
            }
        });
        this.lastlazy = null;
        this.rt.notifyMe('nuevas-intervenciones', sub);
    }

    ngOnDestroy() {
        this.rt.unNotifyMe('nuevas-intervenciones');
    }

    getIntervenciones(page, xpag, filters?) {
        this.intervencionService.getIntervenciones(page, xpag, filters).subscribe(
            resp => {
                if (resp != null && resp.success === true) {
                    this.intervenciones = resp.result.objects;
                    this.cnt_intervenciones = resp.result.count;
                }
            }
        );
    }

    detalleIntervencion(intervencion: Intervencion) {
        this.emiterIntervencionService.emitirIntervencion(intervencion);
        this.router.navigate(['/detalle-intervencion']);
    }

    getTiposIntervencion() {
        this.intervencionService.getTiposIntervencion().subscribe(
            resp => {
                if (resp.success) {
                    this.tiposIntervencion = resp.result;
                    this.tiposIntervencion.unshift({nombre: 'TODOS'});
                }
            }
        );
    }

    getMotivosByTipo(tipoSeleccionado: number): void {
        this.intervencionService.getMotivosByTipo(tipoSeleccionado).subscribe(
            resp => {
                if (resp.success) {
                    this.motivosIntervencion = resp.result;
                    this.motivosIntervencion.unshift({nombre: 'TODOS'});
                }
            }
        );
    }

    getEstadosIntervencion() {
        this.intervencionService.getEstadosIntervencion().subscribe(
            resp => {
                if (resp.success) {
                    this.estados = resp.result;
                    this.estados.unshift({nombre: 'TODOS'});
                }
            }
        );
    }

    loadIntervencionesLazy(event) {
        const filters = {};
        if ((<any>event.filters).id_tipo) {
            (<any>filters).id_tipo = event.filters.id_tipo.value;
            this.getMotivosByTipo((<any>filters).id_tipo);
        }
        if ((<any>event.filters).id_motivo) {
            (<any>filters).id_motivo = event.filters.id_motivo.value;
        }
        if ((<any>event.filters).nombre) {
            (<any>filters).nombre = event.filters.nombre.value;
        }
        if ((<any>event.filters).id_estado) {
            (<any>filters).id_estado = event.filters.id_estado.value;
        }
        console.log('event de paginacion: ', event);
        this.getIntervenciones(((event.first / event.rows) >> 0) + 1, event.rows, filters);
        this.lastlazy = event;
    }
}
