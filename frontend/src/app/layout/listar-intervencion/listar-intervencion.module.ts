import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PageHeaderModule} from './../../shared';
import {ListarIntervencionComponent} from './listar-intervencion.component';
import {ListarIntervencionRoutingModule} from './listar-intervencion-routing.module';
import {IntervencionService} from '../../shared/services/intervencion.service';
import {DataTableModule, PaginatorModule} from 'primeng/primeng';
import {RtService} from '../../shared/services/rt.service';

@NgModule({
    imports: [CommonModule, ListarIntervencionRoutingModule, PageHeaderModule, PaginatorModule, DataTableModule],
    declarations: [ListarIntervencionComponent],
    providers: [IntervencionService, RtService]
})
export class ListarIntervencionModule {
}
