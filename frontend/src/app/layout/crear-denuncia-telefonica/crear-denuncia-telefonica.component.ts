import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {DenunciaService} from '../../shared/services/denuncia.service';
import {Router} from '@angular/router';
import {Denuncia, MotivoDenuncia, TipoDenuncia} from '../../shared/domain/denuncia';
import {MangolConfig, MangolReady} from 'mangol';
import * as ol from 'openlayers';
import {FileUploadService} from '../../shared/services/file-upload.service';
import * as proj4x from 'proj4';
import {IMqttMessage, MqttService} from 'ngx-mqtt';
import {UUID} from 'angular2-uuid';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';

const proj4 = (proj4x as any).default;

@Component({
    selector: 'app-crear-denuncia-telefonica',
    templateUrl: './crear-denuncia-telefonica.component.html',
    styleUrls: ['./crear-denuncia-telefonica.component.scss']
})
export class CrearDenunciaTelefonicaComponent implements OnInit {

    // Los posibles motivos se cargan del back-end por tipo de categoría del reclamo/denuncia.
    motivos: MotivoDenuncia[];
    motivo: MotivoDenuncia;

    tiposDenuncia: TipoDenuncia[];
    tipoSeleccionado: TipoDenuncia;
    ci: string = null;
    listaSugerenciasCi: string[];

    // notificaciones
    mensajeAlert: string;
    success: boolean;

    denuncia: Denuncia;
    nombre: string;
    descripcion: string;
    direccion: string;
    motivoSeleccionado: MotivoDenuncia;

    uploadedFiles: any[] = [];
    uploadedFilesIds: any[] = [];

    ciSubject: Subject<string>;

    miCurrentCoord: ol.Coordinate;

    barrio: string;

    topicPub: string;
    topicSub: string;


    //readonly markerPaths = ['assets/images/marker_l.png', 'assets/images/car_marker_c.png', 'assets/images/car_marker.png'];

    miCurrentMarkerPath;

    constructor(public http: HttpClient,
                private router: Router, private _mqttService: MqttService,
                private denunciaService: DenunciaService, public uploadService: FileUploadService) {
        this.tiposDenuncia = [];
        this.miCurrentMarkerPath = 'assets/images/marker_l.png';
    }

    ngOnInit() {
        this.miCurrentCoord = [null, null];

        // Obtener los tipos de denuncias
        this.getTiposDenuncia();

        const mycomp_uuid = UUID.UUID();
        this.topicPub = 'mapamuni/' + this._mqttService.clientId + '/get-personas/uuid/' + mycomp_uuid;
        this.topicSub = 'mapamuni/' + this._mqttService.clientId + '/get-personas/uuid/' + mycomp_uuid + '/r';

        this.ciSubject = new Subject<string>();
        this.ciSubject.asObservable().debounceTime(200).subscribe(value => {
            this.unsafePublish(this.topicPub, value);
        });

        this._mqttService.observe(this.topicSub).subscribe((message: IMqttMessage) => {
            const msg = JSON.parse(message.payload.toString());
            this.listaSugerenciasCi = (<any[]>msg).map(persona => {
                return persona.ci + ' - ' + persona.nombre + ', ' + persona.apellido;
            });
        });
    }

    locationPicked(coords) {
        // console.log(coords, this.miCurrentCoord);
        this.miCurrentCoord = coords;
    }


    onUpload(event) {
        const ids = this.uploadService.procesarUpload(event.xhr);
        for (const idf of ids) {
            this.uploadedFilesIds.push(idf);

        }
        for (const file of event.files) {
            this.uploadedFiles.push(file);
        }

    }

    onClearUploads() {
        console.log(this.uploadedFiles);
    }

    formatSize(bytes) {
        if (bytes === 0) {
            return '0 B';
        }
        const k = 1000,
            dm = 3,
            sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
            i = Math.floor(Math.log(bytes) / Math.log(k));

        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }

    removeFile(event, file) {
        const idx = this.uploadedFiles.indexOf(file);
        this.uploadedFiles.splice(idx, 1);
        const rem_id = this.uploadedFilesIds[idx];
        this.uploadedFilesIds.splice(idx, 1);
        this.uploadService.cancelarSubidos({ids: [rem_id]});
        console.log(this.uploadedFiles, this.uploadedFilesIds);
    }

    sugerirPersonas(event) {
        this.ciSubject.next(this.ci);
    }

    getTiposDenuncia() {
        this.denunciaService.getTiposDenuncia().subscribe(
            resp => {
                if (resp != null && resp.success === true) {
                    this.tiposDenuncia = resp.result;
                }
            }
        );
    }

    getMotivosByTipo(tipoSeleccionado: TipoDenuncia): void {
        this.miCurrentMarkerPath = tipoSeleccionado.icono;
        this.denunciaService.getMotivosByTipo(tipoSeleccionado.id).subscribe(
            resp => {
                if (resp != null && resp.success === true) {
                    this.motivos = resp.result;
                }
            }
        );
    }

    getMotivoSeleccionado(motivo: MotivoDenuncia): void {
        this.motivoSeleccionado = motivo;
    }

    guardarDenuncia() {
        this.denuncia = new Denuncia(null, this.nombre, this.descripcion, null, null, this.motivoSeleccionado,
            this.miCurrentCoord[1], this.miCurrentCoord[0], this.uploadedFilesIds, null, true, this.ci, false);
        this.denunciaService.saveDenuncia(this.denuncia).subscribe(
            rta => {
                if (rta.success) {
                    // redirigir a la lista
                    this.router.navigate(['/listar-denuncia']);
                }
            }
        );
    }

    cancelar() {
        if (this.uploadedFilesIds.length > 0) {
            this.uploadService.cancelarSubidos({ids: this.uploadedFilesIds});
        }
        // Redirigir al listado
        this.router.navigate(['/listar-denuncia']);
    }

    public unsafePublish(topic: string, message: string): void {
        this._mqttService.unsafePublish(topic, message, {qos: 1, retain: false});
    }

}
