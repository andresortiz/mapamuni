import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearDenunciaTelefonicaComponent } from './crear-denuncia-telefonica.component';

describe('CrearDenunciaTelefonicaComponent', () => {
  let component: CrearDenunciaTelefonicaComponent;
  let fixture: ComponentFixture<CrearDenunciaTelefonicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearDenunciaTelefonicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearDenunciaTelefonicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
