import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CrearDenunciaTelefonicaComponent} from './crear-denuncia-telefonica.component';

const routes: Routes = [
    {
        path: '', component: CrearDenunciaTelefonicaComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CrearDenunciaTelefonicaRoutingModule { }
