import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MangolModule} from 'mangol';
import {DenunciaService} from '../../shared/services/denuncia.service';
import {FormsModule} from '@angular/forms';
import {FileUploadService} from '../../shared/services/file-upload.service';
import {AutoCompleteModule, FileUploadModule} from 'primeng/primeng';
import {SharedModule} from '../../shared/shared.module';
import {CrearDenunciaTelefonicaRoutingModule} from './crear-denuncia-telefonica-routing.module';
import {CrearDenunciaTelefonicaComponent} from './crear-denuncia-telefonica.component';

@NgModule({
    imports: [
        CommonModule, CrearDenunciaTelefonicaRoutingModule, MangolModule, FormsModule, FileUploadModule, SharedModule, AutoCompleteModule
    ],
    declarations: [CrearDenunciaTelefonicaComponent],
    providers: [DenunciaService, FileUploadService]
})
export class CrearDenunciaTelefonicaModule {
}
