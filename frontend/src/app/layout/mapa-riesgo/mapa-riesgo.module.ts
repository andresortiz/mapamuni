import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MapaRiesgoRoutingModule} from './mapa-riesgo-routing.module';
import {MapaRiesgoComponent} from './mapa-riesgo.component';
import {DropdownModule, FileUploadModule, SliderModule} from 'primeng/primeng';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MangolModule} from 'mangol';
import {LoadingModule} from 'ngx-loading';
import {IntervencionService} from '../../shared/services/intervencion.service';

@NgModule({
    imports: [
        CommonModule,
        MapaRiesgoRoutingModule,
        MangolModule, FormsModule, ReactiveFormsModule, FileUploadModule, LoadingModule, DropdownModule, SliderModule
    ],
    declarations: [
        MapaRiesgoComponent
    ],
    providers: [IntervencionService]
})
export class MapaRiesgoModule {
}
