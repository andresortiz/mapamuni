import {Component, EventEmitter, OnInit} from '@angular/core';
import * as ol from 'openlayers';
import {MapBrowserEvent} from 'openlayers';
import {MangolConfig, MangolReady} from 'mangol';
import * as proj4x from 'proj4';
import {FeaturesService} from '../../shared/services/features.service';
import {Intervencion} from '../../shared/domain/intervencion';
import {Router} from '@angular/router';
import {IntervencionService} from '../../shared/services/intervencion.service';
import {RtService} from '../../shared/services/rt.service';
import {EmiterIntervencionService} from '../../shared/services/emiter-intervencion.service';
import * as moment from 'moment';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

const proj4 = (proj4x as any).default;

@Component({
    selector: 'app-mapa-riesgo',
    templateUrl: './mapa-riesgo.component.html',
    styleUrls: ['./mapa-riesgo.component.scss']
})
export class MapaRiesgoComponent implements OnInit {

    config = {} as MangolConfig;

    markerFeature: ol.Feature = null;

    view: ol.View;

    public viewChange = new EventEmitter<ol.View>();
    public locationClicked = new EventEmitter<ol.Coordinate>();

    public mapa: ol.Map;

    public loading = false;

    // Filtros
    tiposIntervencion: any[];
    motivosIntervencion: any[];
    estados: any[];
    tipoIntervencionSeleccionado: any;
    motivoIntervencionSeleccionado: any;
    estadoSeleccionado: any;

    filtrosTemporales: any[];
    filtroTemporalSeleccionado: any;
    rangeValues: number[];
    rangeMin: number;
    rangeMax: number;
    showRange: boolean;
    intervenciones: any[];
    from;
    to;
    mapN;

    heatFeatures;
    sourceFeatures;
    layerFeatures;
    wifiStyle;

    constructor(private featuresService: FeaturesService, private emiterIntervencionService: EmiterIntervencionService,
                private intervencionService: IntervencionService,
                private router: Router, private rt: RtService) {
        this.filtrosTemporales = [
            {nombre: 'Ultima semana', t: 'd', lims: [1, 7]},
            {nombre: 'Ultimo mes', t: 'm', lims: [1, 31]},
            {nombre: 'Ultimos 6 meses', t: '6m', lims: [1, 31 * 6]},
            {nombre: 'Ultimo año', t: 'y', lims: [1, 366]},
            {nombre: 'Todas', t: 'a'},
        ];
        this.filtroTemporalSeleccionado = this.filtrosTemporales[0];

        this.mapN = function (in_num, in_min, in_max, out_min, out_max) {
            return (in_num - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
        };
        this.setupRangeControl();
    }

    // ngOnChanges(changes: SimpleChanges) {
    //     console.log(changes);
    //     if (changes.markerCoord != null) {
    //         console.log('ngChange', this.markerCoord);
    //         if (this.markerFollowsClick && this.markerFeature != null ) {
    //             (<any>this.markerFeature.getGeometry()).setCoordinates(this.markerCoord);
    //         }
    //     }
    //     if (changes.markerImagePath != null) {
    //         console.log('ngChange', this.markerImagePath);
    //         if (this.markerStyleConfig != null ) {
    //             this.markerStyleConfig.src = this.markerImagePath;
    //             this.markerStyle.setImage(new ol.style.Icon(this.markerStyleConfig));
    //             //this.mapa.render();
    //             this.markerSource.changed();
    //         }
    //     }
    // }

    onMapReady(evt: MangolReady) {
        this.mapa = evt.mapService.getMaps()[0];
        const me = this;
        this.mapa.on('singleclick', function (event: MapBrowserEvent) {
            const coords = event.coordinate;
            // me.markerCoord = coords;
            me.locationClicked.emit(coords);
            // if (me.markerFollowsClick) {
            //     (<any>me.markerFeature.getGeometry()).setCoordinates(coords);
            // }
        });
        this.view = this.mapa.getView();
        this.view.on('change:center', event => {
            this.viewChange.emit(this.view);
        });
    }

    ngOnInit() {
        this.intervenciones = [];

        // Cargar los dropdown de filtros
        this.tiposIntervencion = [];
        this.getTiposIntervencion();
        this.motivosIntervencion = [];
        this.estados = [];
        this.getEstadosIntervencion();


        this.sourceFeatures = new ol.source.Vector();
        this.heatFeatures = new ol.source.Vector();
        this.layerFeatures = new ol.layer.Vector({source: this.sourceFeatures});
        //
        // const markerStyleConfig = {
        //     scale: this.markerScale,
        //     rotateWithView: false,
        //     anchor: [0.5, 1],
        //     anchorXUnits: 'fraction',
        //     anchorYUnits: 'fraction',
        //     opacity: 1,
        //     src: this.markerImagePath
        // } as olx.style.IconOptions;
        //
        //
        // this.markerStyle = new ol.style.Style({
        //     image: new ol.style.Icon(this.markerStyleConfig),
        //     zIndex: 5
        // });
        // this.markerFeature = new ol.Feature({
        //     geometry: new ol.geom.Point([-57.5885059, -25.3119575])
        // });
        // const me = this;
        // const markerStyleFunction = function (feature) {
        //     me.markerStyle.getImage().setOpacity(me.markerCoord !== null ? 1 : 0);
        //     return me.markerStyle;
        // };
        // this.markerFeature.setStyle(markerStyleFunction);
        // sourceFeatures.addFeature(this.markerFeature);


        this.wifiStyle = new ol.style.Style({
            image: new ol.style.Icon(({
                scale: 0.2,
                rotateWithView: false,
                anchor: [0.5, 0.5],
                anchorXUnits: 'fraction',
                anchorYUnits: 'fraction',
                opacity: 0.7,
                src: 'assets/images/wifi-icon-l.png'
            })),
            zIndex: 5
        });

        const asuStyle = new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: 'black',
                lineDash: [4],
                width: 3
            }),
        });

        this.featuresService.getMuniFeature().subscribe(feature => {
            feature.setStyle(asuStyle);
            this.sourceFeatures.addFeature(feature);
        });

        // console.log('load tarifada');
        // this.loading = true;
        // this.featuresService.getCalleTarifadasFeatures().subscribe(feature => {
        //     feature.setStyle(tarifaStyle);
        //     console.log('feature ', feature);
        //     sourceFeatures.addFeature(feature);
        // }, () => {
        //     this.loading = false;
        // }, () => {
        //     this.loading = false;
        // });


        // const calleStyle = new ol.style.Style({
        //     stroke: new ol.style.Stroke({
        //         color: '#000000',
        //         width: 1
        //     })
        // });
        //
        // this.featuresService.getCalleFeatures().subscribe(feature => {
        //     console.log(feature);
        //     feature.setStyle(calleStyle);
        //     sourceFeatures.addFeature(feature);
        // });

        const heatmapLayer = new ol.layer.Heatmap({
            source: this.heatFeatures,
            opacity: 0.9,
            weight: '0.5',
            // blur: 10,
            radius: 10,
        });

        this.config = {
            map: {
                renderer: 'canvas',
                target: 'mapa-tarifadas',
                view: {
                    projection: 'EPSG:4326',
                    center: [-57.5885059, -25.3119575] as ol.Coordinate,
                    zoom: 12
                },
                layertree: {
                    layers: [
                        {
                            name: 'OpenStreetMap layer',
                            description: 'Capa base OSM',
                            visible: true,
                            opacity: 1,
                            layer: new ol.layer.Tile({
                                source: new ol.source.OSM()
                            })
                        },
                        {
                            name: 'marker layer',
                            description: 'Capa del marcador',
                            visible: true,
                            opacity: 1,
                            layer: this.layerFeatures,
                        },
                        {
                            name: 'heat layer',
                            description: 'Capa del marcador',
                            visible: true,
                            opacity: 1,
                            layer: heatmapLayer,
                        }
                    ],
                },
                controllers: {
                    tileLoad: true
                },
                // sidebar: {
                //     collapsible: true,
                //     opened: true,
                //     toolbar: {
                //         // Miminal configuration
                //         featureinfo: {}
                //     }
                // }
            }
        } as MangolConfig;
    }


    detalleIntervencion(intervencion: Intervencion) {
        this.emiterIntervencionService.emitirIntervencion(intervencion);
        this.router.navigate(['/detalle-intervencion']);
    }

    getTiposIntervencion() {
        this.intervencionService.getTiposIntervencion().subscribe(
            resp => {
                if (resp.success) {
                    this.tiposIntervencion = resp.result;
                    this.tiposIntervencion.unshift({nombre: 'TODOS'});
                    this.tipoIntervencionSeleccionado = this.tiposIntervencion[0];

                    this.motivosIntervencion = [{nombre: 'TODOS'}];
                    this.motivoIntervencionSeleccionado = this.motivosIntervencion[0];
                }
            }
        );
    }

    getMotivosByTipo(tipoSeleccionado): void {
        // console.log(event);
        if (tipoSeleccionado.id) {
            this.intervencionService.getMotivosByTipo(tipoSeleccionado.id).subscribe(
                resp => {
                    if (resp.success) {
                        this.motivosIntervencion = resp.result;
                        this.motivosIntervencion.unshift({nombre: 'TODOS'});
                        this.motivoIntervencionSeleccionado = this.motivosIntervencion[0];
                    }
                }
            );
        } else {
            this.motivosIntervencion = [{nombre: 'TODOS'}];
            this.motivoIntervencionSeleccionado = this.motivosIntervencion[0];
        }
        this.filterIntervenciones();
    }

    getEstadosIntervencion() {
        this.intervencionService.getEstadosIntervencion().subscribe(
            resp => {
                if (resp.success) {
                    this.estados = resp.result;
                    this.estados.unshift({nombre: 'TODOS'});
                    this.estadoSeleccionado = this.estados[0];
                }
            }
        );
    }

    setupRangeControl() {
        console.log();
        const s = this.filtroTemporalSeleccionado;
        if (s.lims) {
            this.showRange = true;
            this.rangeMin = s.lims[0];
            this.rangeMax = s.lims[1];
            this.rangeValues = s.lims;
        } else {
            this.showRange = false;
            this.rangeMin = this.rangeMax = 0;
            this.rangeValues = [0, 0];
        }
        this.rangeChange();
        this.filterIntervenciones();
    }

    rangeChange() {
        const days_from = this.mapN(this.rangeValues[0], this.rangeMin, this.rangeMax, this.rangeMax, this.rangeMin);
        const days_to = this.mapN(this.rangeValues[1], this.rangeMin, this.rangeMax, this.rangeMax, this.rangeMin);
        this.from = moment().subtract(days_from - 1, 'days').toDate();
        this.to = moment().subtract(days_to - 1, 'days').toDate();
    }


    filterIntervenciones() {
        const base_filter = {
            'id_tipo': this.tipoIntervencionSeleccionado ? this.tipoIntervencionSeleccionado.id : undefined,
            'id_motivo': this.motivoIntervencionSeleccionado ? this.motivoIntervencionSeleccionado.id : undefined,
            'id_estado': this.estadoSeleccionado ? this.estadoSeleccionado.id : undefined,
        };
        if (this.showRange) {
            (<any>base_filter).from = moment(this.from).format('DD-MM-YYYY');
            (<any>base_filter).to = moment(this.to).format('DD-MM-YYYY');
        }
        if (this.heatFeatures)
            this.heatFeatures.clear();
        this.featuresService.getIntervencionesMapa(base_filter).subscribe(feature => {
            const feature2 = feature.clone();
            feature.setStyle(this.wifiStyle);
            //this.sourceFeatures.addFeature(feature);
            this.heatFeatures.addFeature(feature2);
        });

    }

}
