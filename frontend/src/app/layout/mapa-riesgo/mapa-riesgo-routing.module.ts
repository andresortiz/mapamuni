import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MapaRiesgoComponent} from './mapa-riesgo.component';

const routes: Routes = [
    {
        path: '', component: MapaRiesgoComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MapaRiesgoRoutingModule {
}
