import {Component, OnInit} from '@angular/core';
import {routerTransition} from '../../router.animations';
import * as proj4x from 'proj4';
import {Dataset} from '../../shared/domain/dataset';
import {ColumnaDiccionario} from '../../shared/domain/columna-diccionario';
import {DatasetService} from '../../shared/services/dataset.service';
import {EmiterDatasetService} from '../../shared/services/emiter-dataset.service';
import {Router} from '@angular/router';
import {ServiceResponse} from '../../shared/domain/service-response';

const proj4 = (proj4x as any).default;

@Component({
    selector: 'app-crear-editar-dataset',
    templateUrl: './crear-editar-dataset.component.html',
    styleUrls: ['./crear-editar-dataset.component.scss'],
    animations: [routerTransition()]
})
export class CrearEditarDatasetComponent implements OnInit {

    columnasDiccionario: ColumnaDiccionario[];
    colDic: ColumnaDiccionario;
    nombreColumna: string;
    descripcionColumna: string;
    dataset: Dataset;
    nombre: string;
    descripcion: string;
    query: string;
    serviceResponse: ServiceResponse;
    response: any;
    mensajeAlert: string;
    success: boolean;

    constructor(private datasetService: DatasetService,
                private emiterDatasetService: EmiterDatasetService,
                private router: Router) {
    }

    ngOnInit() {
        this.columnasDiccionario = [];
        this.nombreColumna = '';
        this.descripcionColumna = '';

        this.mensajeAlert = '';

        // Recibir emisiones de dataset enviadas desde el componente exportar-dataset que llega acá con la finalidad de editar el mismo
        this.emiterDatasetService.datasetEmitido.subscribe(
            resp => {
                this.dataset = resp;
                // Cargar formulario de la vista de este componente
                if (this.dataset != null) {
                    this.cargarFormulario();
                } else {
                    // Sino limpiar el formulario
                    this.limpiarFormulario();
                }
            },
            error => {
                console.log(error);
            }
        );
    }

    agregarColumna() {
        this.colDic = new ColumnaDiccionario(this.nombreColumna, this.descripcionColumna);
        this.columnasDiccionario.push(this.colDic);
        // Una vez cargado en la tabla limpiar campos del apartado de diccionario del dataset
        this.limpiarInputsDiccionario();
    }

    /**
     * Cargar formulario.
     */
    cargarFormulario() {
        this.nombre = this.dataset.nombre;
        this.descripcion = this.dataset.descripcion;
        this.query = this.dataset.query;
        this.columnasDiccionario = this.dataset.descripciones;
    }

    /**
     * Cargar formulario.
     */
    limpiarFormulario() {
        this.nombre = '';
        this.descripcion = '';
        this.query = '';
        this.columnasDiccionario = [];
        this.limpiarInputsDiccionario();
    }

    limpiarInputsDiccionario() {
        this.nombreColumna = '';
        this.descripcionColumna = '';
    }

    /**
     * Persistir conjunto de datos.
     */
    guardarDataset() {
        // Verificar si se trata de la creación de un nuevo dataset o la edición de uno existente
        if (this.dataset == null) {
            // Se trata de la creación de un nuevo dataset, entonces formar el objeto a partir de los campos de la vista de este componente
            this.dataset = new Dataset(null, this.nombre, this.descripcion, this.query, this.columnasDiccionario);
        } else {
            // Se trata de la edición de un dataset existente (el pedido de edición viene desde el componente exportar-dataset,
            // entonces esta cargada la variable dataset, ya que fue cargada en la suscripcion)
            // Actualizar campos a enviar al back-end.
            this.dataset.nombre = this.nombre;
            this.dataset.descripcion = this.descripcion;
            this.dataset.query = this.query;
            this.dataset.descripciones = this.columnasDiccionario;
        }

        // Llamar a un servicio que se comunique con el back-end
        // this.response = this.datasetService.saveDataset(this.dataset);
        this.response = this.datasetService.saveDataset(this.dataset).subscribe(
            respuesta => {
                this.serviceResponse = respuesta;
                if (this.serviceResponse != null && this.serviceResponse.success === true) {
                    // Limpiar dataset después de haber guardado los cambios de nuevo dataset o edición de dataset
                    this.dataset = null;
                    this.emiterDatasetService.emitirDataset(this.dataset);
                    // redirigir a la lista
                    this.router.navigate(['/listar-dataset']);
                }
            }
        );
    }

    /**
     * Borrar columna del diccionario del dataset.
     */
    borrarColumna(columnaDiccionarioABorrar: ColumnaDiccionario) {
        for (let i = 0; i < this.columnasDiccionario.length; i++) {
            const cd = this.columnasDiccionario[i];
            if (cd === columnaDiccionarioABorrar) {
                // Borrar elemento de la lista de columnas del diccionario: splice(indice, cantidad)
                this.columnasDiccionario.splice(i, 1);
                break;
            }
        }
    }

    cancelar() {
        this.limpiarFormulario();
        // Redirigir al listado
        this.router.navigate(['/listar-dataset']);
    }

}