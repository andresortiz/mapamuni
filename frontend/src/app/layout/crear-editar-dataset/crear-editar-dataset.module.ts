import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CrearEditarDatasetComponent} from './crear-editar-dataset.component';
import {CrearEditarDatasetRoutingModule} from './crear-editar-dataset-routing.module';
import {FormsModule} from '@angular/forms';
import {DatasetService} from '../../shared/services/dataset.service';

@NgModule({
    imports: [CommonModule, CrearEditarDatasetRoutingModule, FormsModule],
    declarations: [CrearEditarDatasetComponent],
    providers: [DatasetService]
})
export class CrearEditarDatasetModule {
}
