import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CrearEditarDatasetComponent} from './crear-editar-dataset.component';

const routes: Routes = [
    {
        path: '', component: CrearEditarDatasetComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [RouterModule]
})
export class CrearEditarDatasetRoutingModule {
}
