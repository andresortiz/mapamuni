import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {CrearEditarMiIntervencionComponent} from './crear-editar-mi-intervencion.component';

describe('CrearEditarMiIntervencionComponent', () => {
  let component: CrearEditarMiIntervencionComponent;
  let fixture: ComponentFixture<CrearEditarMiIntervencionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearEditarMiIntervencionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearEditarMiIntervencionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
