import {Component, OnInit} from '@angular/core';
import {EmiterIntervencionService} from '../../shared/services/emiter-intervencion.service';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Intervencion, MotivoIntervencion, TipoIntervencion} from '../../shared/domain/intervencion';
import {IntervencionService} from '../../shared/services/intervencion.service';
import {IMqttMessage, MqttService} from 'ngx-mqtt';
import {UUID} from 'angular2-uuid';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';
import {PersonaSugerida} from '../../shared/domain/persona-sugerida';
import {FileUploadService} from '../../shared/services/file-upload.service';
import {Denuncia} from '../../shared/domain/denuncia';
import {EmiterDenunciaService} from '../../shared/services/emiter-denuncia.service';
import {EmiterUrlOrigenService} from '../../shared/services/emiter-url-origen.service';

@Component({
    selector: 'app-crear-editar-mi-intervencion',
    templateUrl: './crear-editar-mi-intervencion.component.html',
    styleUrls: ['./crear-editar-mi-intervencion.component.scss']
})
export class CrearEditarMiIntervencionComponent implements OnInit {

    nombre: string;
    descripcion: string;
    // tipoIntervencion: TipoIntervencion;
    nombreDenuncia: string;
    tiposIntervencion: TipoIntervencion[];
    motivos: MotivoIntervencion[];
    motivoSeleccionado: MotivoIntervencion;
    tipoIntervencionSeleccionado: TipoIntervencion;
    estado: string;

    intervencion: Intervencion;

    // Para involucrados
    listaSugerencias: any[];
    ciSubject: Subject<string>;
    ci: PersonaSugerida = null;
    involucrados: any[];
    involucrado: PersonaSugerida;

    // ubicacion
    miCurrentCoord: ol.Coordinate;
    lat: number = null;
    lon: number = null;

    // Para Subir archivos
    uploadedFiles: any[] = [];
    uploadedFilesIds: any[] = [];

    denunciaPreliminar: Denuncia;

    topicPub: string;
    topicSub: string;

    nueva: boolean;

    motivo: MotivoIntervencion;

    constructor(private emiterIntervencionService: EmiterIntervencionService,
                private intervencionService: IntervencionService,
                private emiterDenunciaService: EmiterDenunciaService,
                private emiterUrlOrigenService: EmiterUrlOrigenService,
                private router: Router,
                public http: HttpClient,
                private _mqttService: MqttService,
                public uploadService: FileUploadService) {
        this.tiposIntervencion = [];
        this.motivos = [];
        this.involucrados = [];
        this.denunciaPreliminar = null;
    }

    ngOnInit() {
        this.nueva = true;
        this.miCurrentCoord = [null, null];

        // Recibir la intervencion
        this.emiterIntervencionService.intervencionEmitida.subscribe(
            resp => {
                console.log('recibo la intervencion: ', resp);
                this.intervencion = resp;
                // Cargar formulario de la vista de este componente
                if (this.intervencion != null) {
                    this.cargarFormulario();
                } else {
                    // Sino limpiar el formulariothis.estado = this.intervencion.estado.nombre;
                    this.limpiarFormulario();
                    this.getTiposIntervencion();
                }
            },
            error => {
                console.log(error);
            }
        );

        // Obtener la de sugerencias de ci
        const mycomp_uuid = UUID.UUID();
        this.topicPub = 'mapamuni/' + this._mqttService.clientId + '/get-personas/uuid/' + mycomp_uuid;
        this.topicSub = 'mapamuni/' + this._mqttService.clientId + '/get-personas/uuid/' + mycomp_uuid + '/r';

        this.ciSubject = new Subject<string>();
        this.ciSubject.asObservable().debounceTime(200).subscribe(value => {
            this.unsafePublish(this.topicPub, value);
        });
        this._mqttService.observe(this.topicSub).subscribe((message: IMqttMessage) => {
            const msg = JSON.parse(message.payload.toString());
            const sugerencias = (<PersonaSugerida[]>msg);
            sugerencias.forEach(persona => {
                (<any>persona).str = persona.ci + ' - ' + persona.nombre + ', ' + persona.apellido;
            });
            this.listaSugerencias = sugerencias;
        });

        // Recibir la denuncia para vincular a la intervencion que se va a crear (en caso de venir de listar todas las denuncias)
        this.emiterDenunciaService.denunciaEmitida.subscribe(
            resp => {
                console.log('recibo la denuncia: ', resp);
                this.denunciaPreliminar = resp;
            },
            error => {
                console.log(error);
            }
        );
    }

    /**
     * Cargar formulario.
     */
    cargarFormulario() {
        console.log('intervencion en cargarForm: ', this.intervencion);
        this.nombre = this.intervencion.nombre;
        this.descripcion = this.intervencion.descripcion;
        //this.tipoIntervencion = this.intervencion.motivo.tipo;
        //this.motivo = this.intervencion.motivo;
        //this.tipoIntervencion.id = this.intervencion.motivo.tipo.id;
        this.estado = this.intervencion.estado.nombre;
        this.involucrados = this.intervencion.involucrados;
        this.miCurrentCoord = [this.intervencion.lon, this.intervencion.lat];
        this.denunciaPreliminar = this.intervencion.denuncia_preliminar;
        this.getTiposIntervencion(this.intervencion.motivo.tipo, this.intervencion.motivo);
        this.nueva = false;
    }

    /**
     * Cargar formulario.
     */
    limpiarFormulario() {
        this.nombre = '';
        this.descripcion = '';
        this.estado = 'Pendiente';
        this.nueva = true;
        this.intervencion = null;
    }

    findObjInListById(lista: any[], objeto: any): any {
        return lista.find(obj => obj.id === objeto.id);
    }

    getTiposIntervencion(intervencion?, motivo?) {
        this.intervencionService.getTiposIntervencion().subscribe(
            rta => {
                console.log('rta tipos: ', rta);
                if (rta != null && rta.success) {
                    this.tiposIntervencion = rta.result;
                    if (intervencion) {
                        this.tipoIntervencionSeleccionado = this.findObjInListById(this.tiposIntervencion, intervencion);
                        this.getMotivosByTipo(this.tipoIntervencionSeleccionado, motivo);
                    } else {
                        this.tipoIntervencionSeleccionado = this.tiposIntervencion[0];
                        this.getMotivosByTipo(this.tipoIntervencionSeleccionado);
                    }
                }
            }
        );
    }

    getMotivosByTipo(tipoSeleccionado, motivo?): void {
        console.log('tipo seleccionado', tipoSeleccionado);
        this.intervencionService.getMotivosByTipo(tipoSeleccionado.id).subscribe(
            resp => {
                if (resp != null && resp.success) {
                    this.motivos = resp.result;
                    if (motivo) {
                        this.motivoSeleccionado = this.findObjInListById(this.motivos, motivo);
                    } else {
                        this.motivoSeleccionado = this.motivos[0];
                    }
                }
            }
        );
    }

    setMotivoSeleccionado(motivo: MotivoIntervencion): void {
        console.log('=====> getMotivoSeleccionado: ', motivo);
        this.motivoSeleccionado = motivo;
    }


    guardarIntervencion() {
        // Verificar si se trata de la creación de un nuevo dataset o la edición de uno existente
        if (this.nueva) {
            // Se trata de la creación de un nuevo dataset, entonces formar el objeto a partir de los campos de la vista de este componente
            this.intervencion = new Intervencion(null, this.nombre, this.descripcion,
                this.denunciaPreliminar, null, null, this.motivoSeleccionado,
                this.miCurrentCoord[1], this.miCurrentCoord[0], this.uploadedFilesIds, null, this.involucrados);
        } else {
            // Actualizar campos a enviar al back-end.
            this.intervencion.nombre = this.nombre;
            this.intervencion.descripcion = this.descripcion;
            this.intervencion.denuncia_preliminar = this.denunciaPreliminar;
            this.intervencion.motivo = this.motivoSeleccionado;
            this.intervencion.motivo.tipo = this.tipoIntervencionSeleccionado;
            this.intervencion.lat = this.miCurrentCoord[1];
            this.intervencion.lon = this.miCurrentCoord[0];
            this.intervencion.archivos = this.uploadedFilesIds;
            this.intervencion.involucrados = this.involucrados;
        }
        this.intervencion.archivos = this.intervencion.archivos ? this.intervencion.archivos : [];
        this.intervencion.involucrados = this.intervencion.involucrados ? this.intervencion.involucrados : [];
        this.intervencion.lat = this.intervencion.lat ? this.intervencion.lat : null;
        this.intervencion.lon = this.intervencion.lon ? this.intervencion.lon : null;
        this.intervencion.denuncia_preliminar = this.intervencion.denuncia_preliminar ? this.intervencion.denuncia_preliminar : null;
        this.intervencion.nombre = this.intervencion.nombre ? this.intervencion.nombre : '';
        this.intervencion.descripcion = this.intervencion.descripcion ? this.intervencion.descripcion : '';

        // Llamar a un servicio que se comunique con el back-end
        // this.response = this.datasetService.saveDataset(this.dataset);
        this.intervencionService.saveMiIntervencion(this.intervencion).subscribe(
            rta => {
                if (rta != null && rta.success === true) {
                    // redirigir a la lista
                    if (this.nueva) {
                        this.router.navigate(['/listar-mis-intervenciones']);
                    }
                }
            }
        );
    }

    cancelar() {
        // Redirigir al listado
        this.router.navigate(['/listar-mis-intervenciones']);
    }

    public unsafePublish(topic: string, message: string): void {
        this._mqttService.unsafePublish(topic, message, {qos: 1, retain: false});
    }

    sugerirPersonas(event) {
        this.ci = null;
        this.ciSubject.next(event.query);
    }

    agregarInvolucrado(personaSugerida: PersonaSugerida) {
        // this.involucrado = new PersonaSugerida(this.nombreColumna, this.descripcionColumna);
        if (this.findObjInListById(this.involucrados, personaSugerida) == null) {
            this.involucrados.push(personaSugerida);
        }
        // Una vez cargado en la tabla limpiar campo de sugerencia
        this.ci = null;
    }

    /**
     * Borrar columna de involucrados.
     */
    borrarColumna(personaSugeridaABorrar: PersonaSugerida) {
        for (let i = 0; i < this.involucrados.length; i++) {
            const inv = this.involucrados[i];
            if (inv === personaSugeridaABorrar) {
                // Borrar elemento de la lista de involucrados: splice(indice, cantidad)
                this.involucrados.splice(i, 1);
                break;
            }
        }
    }

    onClear() {
        this.ci = null;
    }

    locationPicked(coords) {
        this.miCurrentCoord = coords;
    }

    /**
     * Subir Archivo
     * */
    onUpload(event) {
        const ids = this.uploadService.procesarUpload(event.xhr);
        for (const idf of ids) {
            this.uploadedFilesIds.push(idf);

        }
        for (const file of event.files) {
            this.uploadedFiles.push(file);
        }

    }

    onClearUploads() {
        console.log(this.uploadedFiles);
    }

    formatSize(bytes) {
        if (bytes === 0) {
            return '0 B';
        }
        const k = 1000,
            dm = 3,
            sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
            i = Math.floor(Math.log(bytes) / Math.log(k));

        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }

    removeFile(event, file) {
        const idx = this.uploadedFiles.indexOf(file);
        this.uploadedFiles.splice(idx, 1);
        const rem_id = this.uploadedFilesIds[idx];
        this.uploadedFilesIds.splice(idx, 1);
        this.uploadService.cancelarSubidos({ids: [rem_id]});
        console.log(this.uploadedFiles, this.uploadedFilesIds);
    }


    /**
     * Ir a detalles de la denuncia
     */
    verDenuncia() {
        // Emitir 'denunciaPreliminar' al componente detalle denuncia
        this.emiterDenunciaService.emitirDenuncia(this.denunciaPreliminar);
        // Emitir la url de retorno
        this.emiterUrlOrigenService.emitirUrlOrigen('/crear-editar-mi-intervencion');
        // redirigir a la pagina de detalle denuncia
        this.router.navigate(['/detalle-denuncia']);
    }

}
