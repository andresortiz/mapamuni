import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CrearEditarMiIntervencionComponent} from './crear-editar-mi-intervencion.component';

const routes: Routes = [
    {
        path: '', component: CrearEditarMiIntervencionComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [RouterModule]
})
export class CrearEditarMiIntervencionRoutingModule {
}
