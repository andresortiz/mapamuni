import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FormsModule} from '@angular/forms';
import {IntervencionService} from '../../shared/services/intervencion.service';
import {AutoCompleteModule, FileUploadModule} from 'primeng/primeng';
import {SharedModule} from '../../shared/shared.module';
import {CrearEditarMiIntervencionComponent} from './crear-editar-mi-intervencion.component';
import {CrearEditarMiIntervencionRoutingModule} from './crear-editar-mi-intervencion-routing.module';

@NgModule({
    imports: [CommonModule, CrearEditarMiIntervencionRoutingModule, FormsModule, AutoCompleteModule, SharedModule, FileUploadModule],
    declarations: [CrearEditarMiIntervencionComponent],
    providers: [IntervencionService]
})
export class CrearEditarMiIntervencionModule {
}
