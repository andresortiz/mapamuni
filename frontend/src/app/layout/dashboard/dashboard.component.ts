import {Component, OnInit} from '@angular/core';
import {routerTransition} from '../../router.animations';
import {DashboardService} from "../../shared/services/dashboard.service";

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    animations: [routerTransition()]
})
export class DashboardComponent implements OnInit {
    public alerts: Array<any> = [];
    public sliders: Array<any> = [];

    // Bar Chart
    public barChartOptions: any = {
        scaleShowVerticalLines: false,
        responsive: true,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    };
    public barChartLabels: string[] = [
        'Accidentes',
        'Infracciones'
    ];

    public barChartType = 'bar';
    public barChartLegend = true;

    /**
     - data: [cantidad de accidente, cantidad de infracciones]
     - label: nombre del estado
     */
    public barChartData: any[] = [
        {data: [65, 59], label: 'Inicial'},
        {data: [28, 48], label: 'Recibido'},
        {data: [28, 48], label: 'Atendido'},
        {data: [28, 48], label: 'Finalizado'}
    ];

    //    USUARIOS
    // Bar Chart
    // public barChartOptions: any = {
    //     scaleShowVerticalLines: false,
    //     responsive: true
    // };

    public u_barChartLabels: string[] = [
        'Usuarios nuevos del sistema',
        'Usuarios diferentes logueados al sistema'
    ];

    private tipos_labels = [
        {prop: 'today', label: 'Hoy'},
        {prop: 'this_week', label: 'Esta semana'},
        {prop: 'this_month', label: 'Este mes'},
        {prop: 'this_year', label: 'Este año'},
        {prop: 'overall', label: 'Desde el inicio'},
    ];

    public u_barChartData: any[] = null;
    public i_barChartData: any[] = null;
    public d_barChartData: any[] = null;

    constructor(public dashboardService: DashboardService) {
        console.log('====> dashboard constructor');
    }

    ngOnInit() {
        console.log('====> dashboard ngOnInit');
        this.loadUserChart();
        this.loadDenunciaChart();
        this.loadIntervencionChart();
    }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }

    chartHovered(event) {

    }

    chartClicked(event) {

    }

    loadUserChart() {
        this.dashboardService.getUserStats().subscribe(rta => {
                if (rta.success) {
                    const tpl: any[] = [
                        {data: [0, 0], label: 'se'},
                        {data: [0, 0], label: ''},
                        {data: [0, 0], label: ''},
                        {data: [0, 0], label: ''},
                        {data: [0, 0], label: ''}
                    ];
                    let idx_s = 0;
                    this.tipos_labels.forEach((val, idx) => {
                        tpl[idx].label = val.label;
                        tpl[idx].data[idx_s] = (<any>rta.result).registered[val.prop];
                    });
                    idx_s = 1;
                    this.tipos_labels.forEach((val, idx) => {
                        tpl[idx].label = val.label;
                        tpl[idx].data[idx_s] = (<any>rta.result).logged[val.prop];
                    });
                    this.u_barChartData = tpl;
                    // console.log(this.u_barChartData);
                }
            }
        );
    }

    loadDenunciaChart() {
        this.dashboardService.getDenunciaStats().subscribe(rta => {
            if (rta.success) {
                //this.d_barChartLabels = rta.result.labels;
                const data = [];
                rta.result.data.forEach((o_tipo, idx_o) => {
                    // por cada tipo
                    const data_tipo = [{data: [], label: ''},
                        {data: [], label: ''},
                        {data: [], label: ''},
                        {data: [], label: ''},
                        {data: [], label: ''}];
                    o_tipo.creadas.forEach((o_estado, idx2) => {
                        this.tipos_labels.forEach((val, idx) => {
                            data_tipo[idx].label = val.label;
                            data_tipo[idx].data.push(o_estado.stats[val.prop]);
                        });
                    });

                    //data.push(data_tipo);
                    data.push({
                        labels: rta.result.labels[idx_o],
                        data: data_tipo,
                        tipo: rta.result.tipos[idx_o],
                    });
                });
                // console.log(data);
                this.d_barChartData = data;
                // console.log(rta.result);

            }
        });
    }

    loadIntervencionChart() {
        this.dashboardService.getIntervencionStats().subscribe(rta => {
            if (rta.success) {
                const data = [];
                rta.result.data.forEach((o_tipo, idx_o) => {
                    // por cada tipo
                    const data_tipo = [{data: [], label: ''},
                        {data: [], label: ''},
                        {data: [], label: ''},
                        {data: [], label: ''},
                        {data: [], label: ''}];
                    o_tipo.creadas.forEach((o_estado, idx2) => {
                        this.tipos_labels.forEach((val, idx) => {
                            data_tipo[idx].label = val.label;
                            data_tipo[idx].data.push(o_estado.stats[val.prop]);
                        });
                    });

                    data.push({
                        labels: rta.result.labels[idx_o],
                        data: data_tipo,
                        tipo: rta.result.tipos[idx_o],
                    });
                });
                // console.log(data);
                this.i_barChartData = data;
                // console.log(rta.result);

            }
        });
    }
}
