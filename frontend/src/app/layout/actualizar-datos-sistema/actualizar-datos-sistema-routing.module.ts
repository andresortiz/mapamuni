import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ActualizarDatosSistemaComponent} from './actualizar-datos-sistema.component';

const routes: Routes = [
    {
        path: '', component: ActualizarDatosSistemaComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [RouterModule]
})
export class ActualizarDatosSistemaRoutingModule {
}
