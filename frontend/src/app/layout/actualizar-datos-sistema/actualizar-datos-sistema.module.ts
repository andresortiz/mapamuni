import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PageHeaderModule} from './../../shared';
import {MangolModule} from 'mangol';
import {FormsModule} from '@angular/forms';
import {ActualizarDatosSistemaRoutingModule} from './actualizar-datos-sistema-routing.module';
import {ActualizarDatosSistemaComponent} from './actualizar-datos-sistema.component';
import {FileUploadModule} from 'primeng/primeng';
import {FileUploadService} from '../../shared/services/file-upload.service';
import {DatoAbiertoService} from '../../shared/services/dato-abierto.service';

@NgModule({
    imports: [CommonModule, ActualizarDatosSistemaRoutingModule, PageHeaderModule, MangolModule, FormsModule, FileUploadModule],
    declarations: [ActualizarDatosSistemaComponent],
    providers: [FileUploadService, DatoAbiertoService]
})
export class ActualizarDatosSistemaModule {
}
