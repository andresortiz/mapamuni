import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizarDatosSistemaComponent } from './actualizar-datos-sistema.component';

describe('ActualizarDatosSistemaComponent', () => {
  let component: ActualizarDatosSistemaComponent;
  let fixture: ComponentFixture<ActualizarDatosSistemaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActualizarDatosSistemaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualizarDatosSistemaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
