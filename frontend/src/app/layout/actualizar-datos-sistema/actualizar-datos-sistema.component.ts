import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DatoAbiertoService} from '../../shared/services/dato-abierto.service';
import {DatoAbiertoSistema} from '../../shared/domain/dato-abierto-sistema';
import {FileUploadService} from '../../shared/services/file-upload.service';

@Component({
    selector: 'app-actualizar-datos-sistema',
    templateUrl: './actualizar-datos-sistema.component.html',
    styleUrls: ['./actualizar-datos-sistema.component.scss']
})
export class ActualizarDatosSistemaComponent implements OnInit {

    datosActualizables: DatoAbiertoSistema[];

    constructor(private datoAbiertoService: DatoAbiertoService,
                private router: Router, public uploadService: FileUploadService) {
        this.datosActualizables = [];
    }

    ngOnInit() {
        this.getDatosActualizables();
    }

    getDatosActualizables() {
        this.datoAbiertoService.getDatosActualizables().subscribe(
            resp => {
                console.log('==> resp: ', resp);
                if (resp.success) {
                    this.datosActualizables = resp.result;
                }
            }
        );
    }

    onBasicUpload(event: any, dato) {
        const ids: number[] = this.uploadService.procesarUpload(event.xhr);
        console.log('=> event: ', event.files, ids);
        if (ids.length > 0) {
            console.log(dato);
            this.datoAbiertoService.setArchivoActualizable({codigo: dato.codigo, id: ids[0]}).subscribe(rta => {
                if (!rta.success) {
                    this.uploadService.cancelarSubidos({ids: ids});
                } else {
                    this.getDatosActualizables();
                }
            });
        }
    }


}
