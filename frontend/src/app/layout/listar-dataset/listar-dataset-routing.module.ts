import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListarDatasetComponent} from './listar-dataset.component';

const routes: Routes = [
    {
        path: '', component: ListarDatasetComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [RouterModule]
})
export class ListarDatasetRoutingModule {
}
