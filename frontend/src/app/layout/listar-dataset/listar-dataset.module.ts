import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FormsModule} from '@angular/forms';
import {DatasetService} from '../../shared/services/dataset.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ListarDatasetComponent} from './listar-dataset.component';
import {ListarDatasetRoutingModule} from './listar-dataset-routing.module';
import {GrowlModule} from 'primeng/growl';

@NgModule({
    imports: [
        CommonModule, ListarDatasetRoutingModule, FormsModule, NgbModule.forRoot(), GrowlModule
    ],
    declarations: [ListarDatasetComponent],
    providers: [DatasetService]
})
export class ListarDatasetModule {
}
