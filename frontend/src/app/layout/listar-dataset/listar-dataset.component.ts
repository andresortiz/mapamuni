import {Component, OnInit} from '@angular/core';
import {routerTransition} from '../../router.animations';
import * as proj4x from 'proj4';
import {DatasetService} from '../../shared/services/dataset.service';
import {Dataset} from '../../shared/domain/dataset';
import {Router} from '@angular/router';
import {EmiterDatasetService} from '../../shared/services/emiter-dataset.service';

const proj4 = (proj4x as any).default;

@Component({
    selector: 'app-listar-dataset',
    templateUrl: './listar-dataset.component.html',
    styleUrls: ['./listar-dataset.component.scss'],
    animations: [routerTransition()]
})
export class ListarDatasetComponent implements OnInit {

    datasets: Dataset[];
    // paginacion
    defaultPagination: number;
    advancedPagination: number;
    paginationSize: number;
    disabledPagination: number;
    isDisabled: boolean;
    page: number;

    constructor(public datasetService: DatasetService,
                private emiterDatasetService: EmiterDatasetService,
                private router: Router) {
        this.datasets = [];

        // paginacion
        this.defaultPagination = 1;
        this.advancedPagination = 1;
        this.paginationSize = 1;
        this.disabledPagination = 1;
        this.isDisabled = true;
        this.page = 20;
    }

    ngOnInit() {
        this.getDatasetsDisponibles();
    }

    getDatasetsDisponibles() {
        this.datasetService.getDatasets().subscribe(
            resp => {
                if (resp != null && resp.success === true) {
                    this.datasets = resp.result;
                }
            }
        );
    }

    agregarDataset() {
        // Emitir que limpie el formulario antes de redirigirse al mismo
        this.emiterDatasetService.emitirDataset(null);
        this.router.navigate(['/crear-editar-dataset']);
    }

    editarDataset(dataset: Dataset) {
        this.emiterDatasetService.emitirDataset(dataset);
        this.router.navigate(['/crear-editar-dataset']);
    }

}