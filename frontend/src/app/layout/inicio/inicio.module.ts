import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';

import { PageHeaderModule } from '../../shared';
import {InicioComponent} from './inicio.component';
import {InicioRoutingModule} from './inicio-routing.module';

@NgModule({
    imports: [CommonModule, Ng2Charts, InicioRoutingModule, PageHeaderModule],
    declarations: [InicioComponent]
})
export class InicioModule {}
