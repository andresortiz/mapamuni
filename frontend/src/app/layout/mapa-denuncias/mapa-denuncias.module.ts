import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MapaDenunciasRoutingModule} from './mapa-denuncias-routing.module';
import {MapaDenunciasComponent} from './mapa-denuncias.component';
import {DropdownModule, FileUploadModule, SliderModule} from 'primeng/primeng';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MangolModule} from 'mangol';
import {LoadingModule} from 'ngx-loading';
import {IntervencionService} from '../../shared/services/intervencion.service';
import {DenunciaService} from "../../shared/services/denuncia.service";

@NgModule({
    imports: [
        CommonModule,
        MapaDenunciasRoutingModule,
        MangolModule, FormsModule, ReactiveFormsModule, FileUploadModule, LoadingModule, DropdownModule, SliderModule
    ],
    declarations: [
        MapaDenunciasComponent
    ],
    providers: [DenunciaService]
})
export class MapaDenunciasModule {
}
