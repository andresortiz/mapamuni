import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MapaDenunciasComponent} from './mapa-denuncias.component';

const routes: Routes = [
    {
        path: '', component: MapaDenunciasComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MapaDenunciasRoutingModule {
}
