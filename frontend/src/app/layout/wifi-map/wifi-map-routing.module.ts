import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {WifiMapComponent} from './wifi-map.component';

const routes: Routes = [
    {
        path: '', component: WifiMapComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class WifiMapRoutingModule { }
