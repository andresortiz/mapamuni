import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PageHeaderModule } from './../../shared';
import {WifiMapComponent} from './wifi-map.component';
import {WifiMapRoutingModule} from './wifi-map-routing.module';
import {MangolModule} from 'mangol';

@NgModule({
    imports: [CommonModule, WifiMapRoutingModule, PageHeaderModule, MangolModule],
    declarations: [WifiMapComponent]
})
export class WifiMapModule {}
