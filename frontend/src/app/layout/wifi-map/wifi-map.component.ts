import {Component, OnInit} from '@angular/core';
import {routerTransition} from '../../router.animations';
import {MangolConfig, MangolReady} from 'mangol';
import * as ol from 'openlayers';
import {MapBrowserEvent} from 'openlayers';
import {HttpClient} from '@angular/common/http';
import * as proj4x from 'proj4';
import {MangolMap} from 'mangol/src/app/classes/map.class';
import {FeaturesService} from "../../shared/services/features.service";
import {MessageService} from "primeng/components/common/messageservice";

const proj4 = (proj4x as any).default;

@Component({
    selector: 'app-wifi-map',
    templateUrl: './wifi-map.component.html',
    styleUrls: ['./wifi-map.component.scss'],
    animations: [routerTransition()]
})
export class WifiMapComponent implements OnInit {
    config = {} as MangolConfig;
    apSelected: any = null;
    callesSource = new ol.source.Vector({features: []});
    barriosSource = new ol.source.Vector({features: []});
    wifiSource = new ol.source.Vector({features: []});
    wifiSource2 = new ol.source.Vector({features: []});


    constructor(public http: HttpClient, private featuresService: FeaturesService, public messageService: MessageService) {
    }

    onMapReady(evt: MangolReady) {
        const mapa: MangolMap = evt.mapService.getMaps()[0];
        const me = this;
        mapa.on('singleclick', function (event: MapBrowserEvent) {
            const coords = event.coordinate;
            let item = null;
            mapa.forEachFeatureAtPixel(mapa.getPixelFromCoordinate(coords), function (feature) {
                const props = feature.getProperties() as any;
                if (props.properties) {
                    item = props.properties.item;
                }
            });
            me.apSelected = item;
            if (item) {
                me.messageService.add({
                    severity: 'info', summary: 'Red: SENATICs Gobierno Nacional',
                    detail: item.descripcion + '<br />' + item.detalle
                });
            }
        });
        // console.log(mapa);
    }

    ngOnInit() {
        const sourceFeatures = new ol.source.Vector();
        const layerFeatures = new ol.layer.Vector({source: sourceFeatures});


        const wifiStyle = new ol.style.Style({
            image: new ol.style.Icon(({
                scale: 0.2,
                rotateWithView: false,
                anchor: [0.5, 0.5],
                anchorXUnits: 'fraction',
                anchorYUnits: 'fraction',
                opacity: 0.7,
                src: 'assets/images/wifi-icon-l.png'
            })),
            zIndex: 5
        });

        // this.http.get('assets/wifi-map-2016.json').subscribe((data: any) => {
        //     //console.log(data);
        //     data.forEach(function (item) {
        //         // if (item.fecha_reclamo.startsWith('2017')) {
        //         const coord = ol.proj.transform([parseFloat(item.longitud), parseFloat(item.latitud)], 'EPSG:4326', 'EPSG:900913')
        //         const feature = new ol.Feature({
        //             name: item.detalle,
        //             properties: {'item': item},
        //             geometry: new ol.geom.Point(coord)
        //         });
        //
        //         this.wifiSource.addFeature(feature);
        //
        //         const feature2 = new ol.Feature({
        //             name: item.detalle,
        //             properties: {'item': item},
        //             geometry: new ol.geom.Point(coord)
        //         });
        //
        //         //this.wifiSource.addFeature(feature);
        //         this.wifiSource2.addFeature(feature2);
        //         feature2.setStyle(markerStyle);
        //         //}
        //         //console.log([item.latitud, item.longitud], coord);
        //
        //     }, this);
        // });

        /*$http({method: 'GET', url: 'assets/calles.geojson'}).then(function (response) {
            angular.forEach(response.data, function (item) {

                const feature = (new ol.format.GeoJSON()).readFeature(item, {
                    dataProjection: 'utm',
                    featureProjection: 'EPSG:900913'
                });

                vectorSource.addFeature(feature);
            });
        });*/

        // const vectorLayer = new ol.layer.Vector({
        //     source: vectorSource,
        //     style: styleFunction
        // });
        //

        const asuStyle = new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: 'red',
                lineDash: [4],
                width: 3
            }),
            // fill: new ol.style.Fill({
            //     color: 'rgba(0, 0, 255, 0.1)'
            // })
        });

        this.featuresService.getMuniFeature().subscribe(feature => {
            feature.setStyle(asuStyle);
            sourceFeatures.addFeature(feature);
        });

        this.featuresService.getWifiFeatures().subscribe(feature => {
            feature.setStyle(wifiStyle);
            sourceFeatures.addFeature(feature);
        });


        const heatmapLayer = new ol.layer.Heatmap({
            source: this.wifiSource,
            opacity: 0.9,
            weight: '0.5',
             blur: 10,
            radius: 20,
        });

        this.config = {
            map: {
                renderer: 'canvas',
                target: 'location-picker',
                view: {
                    projection: 'EPSG:4326',
                    center: [-57.5885059, -25.3119575] as ol.Coordinate,
                    zoom: 12
                },
                layertree: {
                    layers: [
                        {
                            name: 'OpenStreetMap layer',
                            description: 'Capa base OSM',
                            visible: true,
                            opacity: 1,
                            layer: new ol.layer.Tile({
                                source: new ol.source.OSM()
                            })
                        },
                        {
                            name: 'marker layer',
                            description: 'Capa del marcador',
                            visible: true,
                            opacity: 1,
                            layer: layerFeatures,
                        }
                    ],
                },
                controllers: {
                    tileLoad: true
                },
                // sidebar: {
                //     collapsible: true,
                //     opened: true,
                //     toolbar: {
                //         // Miminal configuration
                //         featureinfo: {}
                //     }
                // }
            }
        } as MangolConfig;

        // this.config = {
        //     map: {
        //         renderer: 'canvas',
        //         target: 'blank-page',
        //         view: {
        //             projection: 'EPSG:900913',
        //             center: ol.proj.fromLonLat(
        //                 [-57.5885059, -25.3119575],
        //                 'EPSG:900913'
        //             ),
        //             zoom: 13
        //         },
        //         layertree: {
        //             layers: [
        //                 {
        //                     name: 'OpenStreetMap layer',
        //                     description: 'A sample description',
        //                     visible: true,
        //                     opacity: 1,
        //                     layer: new ol.layer.Tile({
        //                         source: new ol.source.OSM()
        //                     })
        //                 },
        //
        //                 {
        //                     name: 'calles layer',
        //                     description: 'A sample description',
        //                     visible: true,
        //                     opacity: 1,
        //                     layer: layerFeatures,
        //                 },
        //                 // {
        //                 //     name: 'heat layer',
        //                 //     description: 'A saddmple description',
        //                 //     visible: true,
        //                 //     opacity: 1,
        //                 //     layer: heatmapLayer,
        //                 // },
        //                 // {
        //                 //     name: 'calles layer',
        //                 //     description: 'A sample description',
        //                 //     visible: true,
        //                 //     opacity: 1,
        //                 //     layer: new ol.layer.Vector({
        //                 //         source: this.wifiSource2,
        //                 //         // style: styleFunction
        //                 //     })
        //                 // },
        //             ],
        //         },
        //         controllers: {
        //             // mousePosition: {},
        //             scaleLine: {
        //                 units: 'metric'
        //             },
        //             /*quickSearch: {
        //                 items: [
        //                     {
        //                         text: 'Budapest',
        //                         details: 'Capital of Hungary',
        //                         extent: [2108491, 6010126, 2134556, 6039783]
        //                     },
        //                     {
        //                         text: 'London',
        //                         details: 'Capital of England & UK',
        //                         coordinates: [-13664, 6711101]
        //                     },
        //                     {
        //                         text: 'Paris',
        //                         details: 'Capital of France',
        //                         extent: [250839, 6235856, 272853, 6263067]
        //                     }
        //                 ]
        //             },*/
        //             fullScreen: {},
        //             tileLoad: true
        //         },
        //         sidebar: {
        //             collapsible: true,
        //             opened: true,
        //             toolbar: {
        //                 // Miminal configuration
        //                 featureinfo: {}
        //             }
        //         }
        //     }
        // } as MangolConfig;
    }
}