import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CrearGrupoUsuariosComponent} from './crear-grupo-usuarios.component';

const routes: Routes = [
    {
        path: '', component: CrearGrupoUsuariosComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [RouterModule]
})
export class CrearGrupoUsuariosRoutingModule {
}
