import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearGrupoUsuariosComponent } from './crear-grupo-usuarios.component';

describe('CrearGrupoUsuariosComponent', () => {
  let component: CrearGrupoUsuariosComponent;
  let fixture: ComponentFixture<CrearGrupoUsuariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearGrupoUsuariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearGrupoUsuariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
