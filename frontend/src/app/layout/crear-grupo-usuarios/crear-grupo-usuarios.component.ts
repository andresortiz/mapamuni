import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {RolesService} from '../../shared/services/roles.service';
import {Rol} from '../../shared/domain/rol';

@Component({
    selector: 'app-crear-grupo-usuarios',
    templateUrl: './crear-grupo-usuarios.component.html',
    styleUrls: ['./crear-grupo-usuarios.component.scss']
})
export class CrearGrupoUsuariosComponent implements OnInit {

    // para notificaciones
    response: any;
    mensajeAlert: string;
    success: boolean;

    nombre: string;
    descripcion: string;
    grupo: Rol;

    constructor(private router: Router,
                private rolesService: RolesService) {
    }

    ngOnInit() {
        this.mensajeAlert = '';
        this.grupo = null;
    }

    crearGrupo() {
        this.grupo = new Rol(null, this.nombre, this.descripcion, [], []);
        this.rolesService.crearRol(this.grupo).subscribe(
            rta => {
                if (rta.success) {
                    // redirigir a la lista
                    this.router.navigate(['/roles']);
                }
            }
        );
    }

    cancelar() {
        // Redirigir al listado
        this.router.navigate(['/roles']);
    }

}
