import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FormsModule} from '@angular/forms';
import {CrearGrupoUsuariosComponent} from './crear-grupo-usuarios.component';
import {CrearGrupoUsuariosRoutingModule} from './crear-grupo-usuarios-routing.module';
import {RolesService} from '../../shared/services/roles.service';

@NgModule({
    imports: [CommonModule, CrearGrupoUsuariosRoutingModule, FormsModule],
    declarations: [CrearGrupoUsuariosComponent],
    providers: [RolesService]
})
export class CrearGrupoUsuariosModule {
}
