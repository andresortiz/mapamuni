import {Component, OnInit} from '@angular/core';
import {routerTransition} from '../../router.animations';
import * as ol from 'openlayers';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Denuncia, MotivoDenuncia, TipoDenuncia} from '../../shared/domain/denuncia';
import {DenunciaService} from '../../shared/services/denuncia.service';
import {FileUploadService} from '../../shared/services/file-upload.service';


@Component({
    selector: 'app-crear-mi-denuncia',
    templateUrl: './crear-mi-denuncia.component.html',
    styleUrls: ['./crear-mi-denuncia.component.scss'],
    animations: [routerTransition()]
})

export class CrearMiDenunciaComponent implements OnInit {
    // Los posibles motivos se cargan del back-end por tipo de categoría del reclamo/denuncia.
    motivos: MotivoDenuncia[];
    motivo: MotivoDenuncia;
    tiposDenuncia: TipoDenuncia[];
    tipoSeleccionado: TipoDenuncia;

    // notificaciones
    success: boolean;

    denuncia: Denuncia;
    nombre: string;
    descripcion: string;
    direccion: string;
    motivoSeleccionado: MotivoDenuncia;

    miCurrentCoord: ol.Coordinate;
    miCurrentMarkerPath;

    uploadedFiles: any[] = [];
    uploadedFilesIds: any[] = [];

    barrio: string;

    constructor(public http: HttpClient,
                private router: Router,
                private denunciaService: DenunciaService, public uploadService: FileUploadService) {
        this.tiposDenuncia = [];
        this.miCurrentMarkerPath = 'assets/images/marker_l.png';
    }

    ngOnInit() {
        this.miCurrentCoord = [null, null];
        // Obtener los tipos de denuncias
        this.getTiposDenuncia();
    }

    onUpload(event) {
        const ids = this.uploadService.procesarUpload(event.xhr);
        for (const idf of ids) {
            this.uploadedFilesIds.push(idf);

        }
        for (const file of event.files) {
            this.uploadedFiles.push(file);
        }

    }

    onClearUploads() {
        console.log(this.uploadedFiles);
    }

    formatSize(bytes) {
        if (bytes === 0) {
            return '0 B';
        }
        const k = 1000,
            dm = 3,
            sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
            i = Math.floor(Math.log(bytes) / Math.log(k));

        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }

    removeFile(event, file) {
        const idx = this.uploadedFiles.indexOf(file);
        this.uploadedFiles.splice(idx, 1);
        const rem_id = this.uploadedFilesIds[idx];
        this.uploadedFilesIds.splice(idx, 1);
        this.uploadService.cancelarSubidos({ids: [rem_id]});
        console.log(this.uploadedFiles, this.uploadedFilesIds);
    }


    locationPicked(coords) {
        // console.log(coords, this.miCurrentCoord);
        this.miCurrentCoord = coords;
    }

    getTiposDenuncia() {
        this.denunciaService.getTiposDenuncia().subscribe(
            resp => {
                if (resp != null && resp.success === true) {
                    this.tiposDenuncia = resp.result;
                }
            }
        );
    }

    getMotivosByTipo(tipoSeleccionado: TipoDenuncia): void {
        this.miCurrentMarkerPath = tipoSeleccionado.icono;
        this.denunciaService.getMotivosByTipo(tipoSeleccionado.id).subscribe(
            resp => {
                if (resp != null && resp.success === true) {
                    this.motivos = resp.result;
                }
            }
        );
    }

    getMotivoSeleccionado(motivo: MotivoDenuncia): void {
        console.log('=====> getMotivoSeleccionado: ', motivo);
        this.motivoSeleccionado = motivo;
    }

    guardarDenuncia() {
        this.denuncia = new Denuncia(null, this.nombre, this.descripcion, null, null, this.motivoSeleccionado,
            this.miCurrentCoord[1], this.miCurrentCoord[0], this.uploadedFilesIds, null, false, null, false);
        this.denunciaService.saveMiDenuncia(this.denuncia).subscribe(
            rta => {
                if (rta.success) {
                    // redirigir a la lista
                    this.router.navigate(['/listar-mis-denuncias']);
                }
            }
        );
    }

    cancelar() {
        if (this.uploadedFilesIds.length > 0) {
            this.uploadService.cancelarSubidos({ids: this.uploadedFilesIds});
        }
        // Redirigir al listado
        this.router.navigate(['/listar-mis-denuncias']);
    }
}

