import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MangolModule} from 'mangol';
import {DenunciaService} from '../../shared/services/denuncia.service';
import {FormsModule} from '@angular/forms';
import {FileUploadService} from '../../shared/services/file-upload.service';
import {FileUploadModule} from 'primeng/primeng';
import {SharedModule} from '../../shared/shared.module';
import {CrearMiDenunciaRoutingModule} from './crear-mi-denuncia-routing.module';
import {CrearMiDenunciaComponent} from './crear-mi-denuncia.component';

@NgModule({
    imports: [CommonModule, CrearMiDenunciaRoutingModule, MangolModule, FormsModule, FileUploadModule, SharedModule],
    declarations: [CrearMiDenunciaComponent],
    providers: [DenunciaService, FileUploadService]
})
export class CrearMiDenunciaModule {
}
