import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CrearMiDenunciaComponent} from './crear-mi-denuncia.component';

const routes: Routes = [
    {
        path: '', component: CrearMiDenunciaComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CrearMiDenunciaRoutingModule { }
