import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EditarMapaZonaTarifadaComponent} from './editar-mapa-zona-tarifada.component';

const routes: Routes = [
    {
        path: '', component: EditarMapaZonaTarifadaComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EditarMapaZonaTarifadaRoutingModule {
}
