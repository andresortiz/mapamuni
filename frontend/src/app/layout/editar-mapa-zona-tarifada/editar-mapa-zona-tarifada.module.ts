import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {EditarMapaZonaTarifadaRoutingModule} from './editar-mapa-zona-tarifada-routing.module';
import {EditarMapaZonaTarifadaComponent} from './editar-mapa-zona-tarifada.component';
import {FileUploadModule} from 'primeng/primeng';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MangolModule} from 'mangol';
import {FileUploadService} from '../../shared/services/file-upload.service';
import {LoadingModule} from 'ngx-loading';
import {MessageService} from "primeng/components/common/messageservice";
import {FeaturesService} from "../../shared/services/features.service";
import {DatoAbiertoService} from "../../shared/services/dato-abierto.service";

@NgModule({
    imports: [
        CommonModule,
        EditarMapaZonaTarifadaRoutingModule,
        MangolModule, FormsModule, ReactiveFormsModule, FileUploadModule,
        LoadingModule
    ],
    declarations: [
        EditarMapaZonaTarifadaComponent
    ],
    providers: [FileUploadService, FeaturesService, DatoAbiertoService]
})
export class EditarMapaZonaTarifadaModule {}
