import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ActualizarDatosUsuarioComponent} from './actualizar-datos-usuario.component';

const routes: Routes = [
    {
        path: '', component: ActualizarDatosUsuarioComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [RouterModule]
})
export class ActualizarDatosUsuarioRoutingModule {
}
