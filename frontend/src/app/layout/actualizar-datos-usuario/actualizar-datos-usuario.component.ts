import {Component, OnInit} from '@angular/core';
import {routerTransition} from '../../router.animations';
import {HttpClient} from '@angular/common/http';
import * as proj4x from 'proj4';
import {AccesoService} from '../../shared/services/acceso.service';
import {Router} from '@angular/router';

const proj4 = (proj4x as any).default;

@Component({
    selector: 'app-actualizar-datos-usuario',
    templateUrl: './actualizar-datos-usuario.component.html',
    styleUrls: ['./actualizar-datos-usuario.component.scss'],
    animations: [routerTransition()]
})
export class ActualizarDatosUsuarioComponent implements OnInit {

    nombre: string;
    apellido: string;
    nombreUsuario: string;
    direccion: string;
    ctaCatastral: string;
    telefono: string;
    correo: string;
    ci: string;
    //lat: any;
    //lon: any;

    miCurrentCoord: ol.Coordinate = null;

    usuario: any; // Usuario;

    constructor(public http: HttpClient,
                private accesoService: AccesoService,
                private router: Router) {
    }

    ngOnInit() {
        this.inicializarCampos();
        this.accesoService.getPerfilUsuarioLogueado({silent: true}).subscribe(
            rta => {
                this.usuario = rta;
                if (rta != null && rta.success) {
                    this.nombre = rta.result.nombre ? rta.result.nombre : '';
                    this.apellido = rta.result.apellido ? rta.result.apellido : '';
                    this.direccion = rta.result.direccion ? rta.result.direccion : '';
                    this.ctaCatastral = rta.result.cta_catastral ? rta.result.cta_catastral : '';
                    this.telefono = rta.result.telefono ? rta.result.telefono : '';
                    this.correo = rta.result.email ? rta.result.email : '';
                    this.ci = rta.result.ci ? rta.result.ci : '';
                    this.nombreUsuario = rta.result.user_id ? rta.result.user_id : '';
                    // this.lat = rta.result.lat ? rta.result.lat : null;
                    // this.lon = rta.result.lon ? rta.result.lon : null;
                    const lat = rta.result.lat ? parseFloat(rta.result.lat) : null;
                    const lon = rta.result.lon ? parseFloat(rta.result.lon) : null;
                    this.miCurrentCoord = [lon, lat];
                    // console.log(this.miCurrentCoord);
                }
            }
        );
    }

    inicializarCampos() {
        this.nombre = '';
        this.apellido = '';
        this.nombreUsuario = '';
        this.direccion = '';
        this.correo = '';
        this.telefono = '';
        this.ctaCatastral = '';
        this.ci = '';
        this.miCurrentCoord = null;
    }

    actualizarDatosUsuario() {
        // Formar el parametro a enviar al back-end para actualizar los datos del usuario.
        this.usuario.nombre = this.nombre;
        this.usuario.apellido = this.apellido;
        this.usuario.direccion = this.direccion;
        this.usuario.cta_catastral = this.ctaCatastral;
        this.usuario.telefono = this.telefono;
        this.usuario.email = this.correo;
        this.usuario.ci = this.ci;
        this.usuario.user_id = this.nombreUsuario;
        this.usuario.lat = this.miCurrentCoord[1];
        this.usuario.lon = this.miCurrentCoord[0];
        console.log(this.usuario);

        this.accesoService.cambiarDatosUsuario(this.usuario).subscribe(
            rta => {
                // En caso de éxito,
                if (rta.success) {
                    console.log('cambio exitosamente');
                }
            });
    }

    locationPicked(coords) {
        console.log(coords, this.miCurrentCoord);
        this.miCurrentCoord = coords;
    }


    cancelar() {
        this.router.navigate(['']);
    }

}
