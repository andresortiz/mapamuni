import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PageHeaderModule} from './../../shared';
import {MangolModule} from 'mangol';
import {ActualizarDatosUsuarioRoutingModule} from './actualizar-datos-usuario-routing.module';
import {ActualizarDatosUsuarioComponent} from './actualizar-datos-usuario.component';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
    imports: [CommonModule, ActualizarDatosUsuarioRoutingModule, PageHeaderModule, MangolModule, FormsModule, SharedModule],
    declarations: [ActualizarDatosUsuarioComponent]
})
export class ActualizarDatosUsuarioModule {
}
