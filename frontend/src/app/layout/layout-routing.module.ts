import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LayoutComponent} from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            // { path: '', redirectTo: 'dashboard' },
            {path: '', redirectTo: 'inicio'},

            // dasboard para info importante para analizar a simple vista
            {path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule'},
            {path: 'inicio', loadChildren: './inicio/inicio.module#InicioModule'},

            // datos del usuario
            {
                path: 'actualizar-datos-usuario',
                loadChildren: './actualizar-datos-usuario/actualizar-datos-usuario.module#ActualizarDatosUsuarioModule'
            },
            {
                path: 'cambiar-password',
                loadChildren: './cambiar-password/cambiar-password.module#CambiarPasswordModule'
            },

            // datos del sistema
            {
                path: 'actualizar-datos-sistema',
                loadChildren: './actualizar-datos-sistema/actualizar-datos-sistema.module#ActualizarDatosSistemaModule'
            },

            // denuncias en general
            {path: 'listar-denuncia', loadChildren: './listar-denuncia/listar-denuncia.module#ListarDenunciaModule'},
            {
                path: 'crear-denuncia-telefonica',
                loadChildren: './crear-denuncia-telefonica/crear-denuncia-telefonica.module#CrearDenunciaTelefonicaModule'
            },
            {path: 'seguir-denuncia', loadChildren: './seguir-denuncia/seguir-denuncia.module#SeguirDenunciaModule'},
            {
                path: 'registro-accidente',
                loadChildren: './registro-accidente/registro-accidente.module#RegistroAccidenteModule'
            },
            // denuncias del ciudadano
            {
                path: 'listar-mis-denuncias',
                loadChildren: './listar-mis-denuncias/listar-mis-denuncias.module#ListarMisDenunciasModule'
            },
            {
                path: 'crear-mi-denuncia',
                loadChildren: './crear-mi-denuncia/crear-mi-denuncia.module#CrearMiDenunciaModule'
            },
            {
                path: 'seguir-mis-denuncias',
                loadChildren: './seguir-mis-denuncias/seguir-mis-denuncias.module#SeguirMisDenunciasModule'
            },

            // intervencion de todos los PMTs
            {
                path: 'listar-intervencion',
                loadChildren: './listar-intervencion/listar-intervencion.module#ListarIntervencionModule'
            },
            // intervencion del PMT logueado
            {
                path: 'listar-mis-intervenciones',
                loadChildren: './listar-mis-intervenciones/listar-mis-intervenciones.module#ListarMisIntervencionesModule'
            },
            {
                path: 'crear-editar-mi-intervencion',
                loadChildren: './crear-editar-mi-intervencion/crear-editar-mi-intervencion.module#CrearEditarMiIntervencionModule'
            },

            // detalle de denuncia para ver desde intervencion la denuncia vinculada
            {
                path: 'detalle-denuncia',
                loadChildren: './detalle-denuncia/detalle-denuncia.module#DetalleDenunciaModule'
            },

            // detalle de intervencion para ver desde listado de todas las intervenciones de los funcionarios
            {
                path: 'detalle-intervencion',
                loadChildren: './detalle-intervencion/detalle-intervencion.module#DetalleIntervencionModule'
            },

            // funcionario admin de la Municipalidad
            {path: 'roles', loadChildren: './roles/roles.module#RolesModule'},
            {
                path: 'crear-grupo-usuarios',
                loadChildren: './crear-grupo-usuarios/crear-grupo-usuarios.module#CrearGrupoUsuariosModule'
            },

            // datos agregados (info de otro dataset del portal de datos abiertos, creacion de datasets de Muni)
            {path: 'wifi-map', loadChildren: './wifi-map/wifi-map.module#WifiMapModule'},
            {
                path: 'crear-editar-dataset',
                loadChildren: './crear-editar-dataset/crear-editar-dataset.module#CrearEditarDatasetModule'
            },
            {path: 'listar-dataset', loadChildren: './listar-dataset/listar-dataset.module#ListarDatasetModule'},

            // ejemplos del template:
            // charts sería nuestra página de la muni (Trabajando para vos)
            // { path: 'tables', loadChildren: './tables/tables.module#TablesModule' },
            {path: 'charts', loadChildren: './charts/charts.module#ChartsModule'},
            // blank-page sería nuestra página de zonas de riesgos
            // {path: 'blank-page', loadChildren: './blank-page/blank-page.module#BlankPageModule'},

            {path: 'mapa-riesgo', loadChildren: './mapa-riesgo/mapa-riesgo.module#MapaRiesgoModule'},
            {path: 'mapa-denuncias', loadChildren: './mapa-denuncias/mapa-denuncias.module#MapaDenunciasModule'},
            {
                path: 'mapa-zona-tarifada',
                loadChildren: './mapa-zona-tarifada/mapa-zona-tarifada.module#MapaZonaTarifadaModule'
            },
            {
                path: 'editar-mapa-zona-tarifada',
                loadChildren: './editar-mapa-zona-tarifada/editar-mapa-zona-tarifada.module#EditarMapaZonaTarifadaModule'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {
}
