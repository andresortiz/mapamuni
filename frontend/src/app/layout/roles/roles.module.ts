import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PageHeaderModule} from './../../shared';
import {RolesComponent} from './roles.component';
import {RolesRoutingModule} from './roles-routing.module';
import {ContextMenuModule, PickListModule} from 'primeng/primeng';
import {TreeModule} from 'primeng/tree';
import {RolesService} from '../../shared/services/roles.service';
import {GrowlModule} from 'primeng/growl';
import {FormsModule} from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        RolesRoutingModule,
        PageHeaderModule,
        PickListModule,
        TreeModule,
        ContextMenuModule,
        GrowlModule,
        FormsModule
    ],
    declarations: [RolesComponent],
    providers: [
        RolesService
    ]
})
export class RolesModule {
}
