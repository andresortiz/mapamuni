import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {MenuItem, Message, TreeNode} from 'primeng/api';
import {Tree} from 'primeng/tree';
import {RolesService} from '../../shared/services/roles.service';
import {ServiceResponse} from '../../shared/domain/service-response';
import {Router} from '@angular/router';
import {Rol} from '../../shared/domain/rol';

@Component({
    selector: 'app-roles',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './roles.component.html',
    styleUrls: ['./roles.component.scss']
})
export class RolesComponent implements OnInit {

    // Para el select de Grupos de Usarios
    grupos: Rol[];

    // Para el PickList de Usuarios
    usuariosAll: any[];
    usuariosDisponibles: any[];
    usuariosSeleccionados: any[];
    // item: any [];

    // Para el Tree de Permisos
    serviceResponse: ServiceResponse;
    success: boolean;
    msgs: Message[];

    @ViewChild('expandingTree')
    expandingTree: Tree;

    permisosTree: TreeNode[];
    selectedPermisos: TreeNode[];
    // items: MenuItem[];
    loading: boolean;

    grupoSeleccionado: Rol;
    rol: Rol;


    constructor(private rolesService: RolesService,
                private router: Router) {
    }

    ngOnInit() {
        this.rol = null;
        // Para el select de Grupos de Usarios
        this.getGrupos();

        // Para el PickList de Usuarios
        // this.usuariosDisponibles =  this.grupos; // initialize list 1
        this.getUsuarios();
        this.usuariosSeleccionados = []; // initialize list 2

        // Para el Tree de Permisos
        this.getPermisos();

        this.loading = true;
        this.selectedPermisos = [];

        // this.items = [
        //     {label: 'View', icon: 'fa fa-search', command: (event) => this.viewFile(this.selectedFile)},
        //     {label: 'Unselect', icon: 'fa fa-close', command: (event) => this.unselectFile()}
        // ];
    }

    // viewFile(file: TreeNode) {
    //     this.msgs = [];
    //     this.msgs.push({severity: 'info', summary: 'Node Selected with Right Click', detail: file.label});
    // }
    //
    // unselectFile() {
    //     this.selectedFile = null;
    // }

    agregarGrupoUsuario() {
        this.router.navigate(['/crear-grupo-usuarios']);
    }

    getGrupos() {
        this.rolesService.getGrupos().subscribe(
            respuesta => {
                this.serviceResponse = respuesta;
                if (this.serviceResponse != null && this.serviceResponse.success === true) {

                    this.grupos = this.serviceResponse.result;
                }
            }
        );
    }

    getUsuarios() {
        this.rolesService.getUsuarios().subscribe(
            respuesta => {
                this.serviceResponse = respuesta;
                if (this.serviceResponse != null && this.serviceResponse.success === true) {
                    this.usuariosAll = this.serviceResponse.result;
                }
            }
        );
    }

    getPermisos() {
        this.rolesService.getPermisos().subscribe(
            respuesta => {
                this.serviceResponse = respuesta;
                if (this.serviceResponse != null && this.serviceResponse.success === true) {

                    this.permisosTree = this.serviceResponse.result;
                }

            }
        );
    }

    getSelNodes(lids: number[], tree: TreeNode[], selected: TreeNode[]): number {
        let c = 0;
        tree.forEach(node => {
            if (lids.includes(node.data)) {
                selected.push(node);
                c++;
            }
            if (!node.leaf && node.children.length > 0) {
                const cSelChildren = this.getSelNodes(lids, node.children, selected);
                node.partialSelected = (cSelChildren > 0);
            }
        });
        return c;
    }

    getGrupoSeleccionado(grupoSeleccionado: Rol): void {
        console.log('=====> getGrupoSeleccionado: ', grupoSeleccionado);
        this.grupoSeleccionado = grupoSeleccionado;

        this.usuariosDisponibles = this.usuariosAll.filter(usuario => {
            return (grupoSeleccionado.usuarios.indexOf(usuario.id) === -1);
        });
        this.usuariosSeleccionados = this.usuariosAll.filter(usuario => {
            return (grupoSeleccionado.usuarios.indexOf(usuario.id) !== -1);
        });
        const newSelPermisos: TreeNode[] = [];
        this.getSelNodes(this.grupoSeleccionado.permisos, this.permisosTree, newSelPermisos);
        this.selectedPermisos = newSelPermisos;
        //console.log(this.selectedPermisos, this.grupoSeleccionado.permisos);
    }

    guardarRol() {
        //console.log('==> parametros a guardar: ', this.grupoSeleccionado, ' - ', this.usuariosSeleccionados, ' - ', this.selectedFiles);
        const uids = this.usuariosSeleccionados.map(u => u.id);
        console.log(this.usuariosSeleccionados, uids);
        const pids = this.selectedPermisos.filter(p => p.leaf).map(p => p.data);
        console.log(this.selectedPermisos, pids);
        //console.log('==> parametros a guardar: ', this.grupoSeleccionado, ' - ', uids, ' - ', pids);
        this.rol = new Rol(this.grupoSeleccionado.id, this.grupoSeleccionado.nombre, this.grupoSeleccionado.descripcion,
            uids, pids);
        this.rolesService.saveRol(this.rol).subscribe(
            rta => {
                if (rta.success) {
                    // recargar la lista de permisos asignadas al rol
                    this.getGrupos();
                }
            }
        );
    }

    cancelar() {
        this.router.navigate(['/']);
    }

}
