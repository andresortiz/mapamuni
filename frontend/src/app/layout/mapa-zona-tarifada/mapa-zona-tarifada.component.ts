import {Component, EventEmitter, OnInit} from '@angular/core';
import * as ol from 'openlayers';
import {MapBrowserEvent} from 'openlayers';
import {MangolConfig, MangolReady} from 'mangol';
import * as proj4x from 'proj4';
import {FeaturesService} from '../../shared/services/features.service';

const proj4 = (proj4x as any).default;

@Component({
    selector: 'app-mapa-zona-tarifada',
    templateUrl: './mapa-zona-tarifada.component.html',
    styleUrls: ['./mapa-zona-tarifada.component.scss']
})
export class MapaZonaTarifadaComponent implements OnInit {

    config = {} as MangolConfig;

    markerFeature: ol.Feature = null;

    view: ol.View;

    public viewChange = new EventEmitter<ol.View>();
    public locationClicked = new EventEmitter<ol.Coordinate>();

    public mapa: ol.Map;

    public loading = false;

    constructor(private featuresService: FeaturesService) {
    }

    // ngOnChanges(changes: SimpleChanges) {
    //     console.log(changes);
    //     if (changes.markerCoord != null) {
    //         console.log('ngChange', this.markerCoord);
    //         if (this.markerFollowsClick && this.markerFeature != null ) {
    //             (<any>this.markerFeature.getGeometry()).setCoordinates(this.markerCoord);
    //         }
    //     }
    //     if (changes.markerImagePath != null) {
    //         console.log('ngChange', this.markerImagePath);
    //         if (this.markerStyleConfig != null ) {
    //             this.markerStyleConfig.src = this.markerImagePath;
    //             this.markerStyle.setImage(new ol.style.Icon(this.markerStyleConfig));
    //             //this.mapa.render();
    //             this.markerSource.changed();
    //         }
    //     }
    // }

    onMapReady(evt: MangolReady) {
        this.mapa = evt.mapService.getMaps()[0];
        const me = this;
        this.mapa.on('singleclick', function (event: MapBrowserEvent) {
            const coords = event.coordinate;
            // me.markerCoord = coords;
            me.locationClicked.emit(coords);
            // if (me.markerFollowsClick) {
            //     (<any>me.markerFeature.getGeometry()).setCoordinates(coords);
            // }
        });
        this.view = this.mapa.getView();
        this.view.on('change:center', event => {
            this.viewChange.emit(this.view);
        });
    }

    ngOnInit() {
        const sourceFeatures = new ol.source.Vector();
        const layerFeatures = new ol.layer.Vector({source: sourceFeatures});
        //
        // const markerStyleConfig = {
        //     scale: this.markerScale,
        //     rotateWithView: false,
        //     anchor: [0.5, 1],
        //     anchorXUnits: 'fraction',
        //     anchorYUnits: 'fraction',
        //     opacity: 1,
        //     src: this.markerImagePath
        // } as olx.style.IconOptions;
        //
        //
        // this.markerStyle = new ol.style.Style({
        //     image: new ol.style.Icon(this.markerStyleConfig),
        //     zIndex: 5
        // });
        // this.markerFeature = new ol.Feature({
        //     geometry: new ol.geom.Point([-57.5885059, -25.3119575])
        // });
        // const me = this;
        // const markerStyleFunction = function (feature) {
        //     me.markerStyle.getImage().setOpacity(me.markerCoord !== null ? 1 : 0);
        //     return me.markerStyle;
        // };
        // this.markerFeature.setStyle(markerStyleFunction);
        // sourceFeatures.addFeature(this.markerFeature);

        const asuStyle = new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: 'black',
                lineDash: [4],
                width: 3
            }),
        });

        const tarifaStyle = new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: 'red',
                width: 3
            }),
        });

        this.featuresService.getMuniFeature().subscribe(feature => {
            feature.setStyle(asuStyle);
            sourceFeatures.addFeature(feature);
        });

        console.log('load tarifada');
        this.loading = true;
        this.featuresService.getCalleTarifadasFeatures().subscribe(feature => {
            feature.setStyle(tarifaStyle);
            console.log('feature ', feature);
            sourceFeatures.addFeature(feature);
        }, () => {
            this.loading = false;
        }, () => {
            this.loading = false;
        });



        // const calleStyle = new ol.style.Style({
        //     stroke: new ol.style.Stroke({
        //         color: '#000000',
        //         width: 1
        //     })
        // });
        //
        // this.featuresService.getCalleFeatures().subscribe(feature => {
        //     console.log(feature);
        //     feature.setStyle(calleStyle);
        //     sourceFeatures.addFeature(feature);
        // });

        this.config = {
            map: {
                renderer: 'canvas',
                target: 'mapa-tarifadas',
                view: {
                    projection: 'EPSG:4326',
                    center: [-57.5885059, -25.3119575] as ol.Coordinate,
                    zoom: 12
                },
                layertree: {
                    layers: [
                        {
                            name: 'OpenStreetMap layer',
                            description: 'Capa base OSM',
                            visible: true,
                            opacity: 1,
                            layer: new ol.layer.Tile({
                                source: new ol.source.OSM()
                            })
                        },
                        {
                            name: 'marker layer',
                            description: 'Capa del marcador',
                            visible: true,
                            opacity: 1,
                            layer: layerFeatures,
                        }
                    ],
                },
                controllers: {
                    tileLoad: true
                },
                // sidebar: {
                //     collapsible: true,
                //     opened: true,
                //     toolbar: {
                //         // Miminal configuration
                //         featureinfo: {}
                //     }
                // }
            }
        } as MangolConfig;
    }

}
