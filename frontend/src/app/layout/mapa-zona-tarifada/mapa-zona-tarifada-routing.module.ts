import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MapaZonaTarifadaComponent} from './mapa-zona-tarifada.component';

const routes: Routes = [
    {
        path: '', component: MapaZonaTarifadaComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MapaZonaTarifadaRoutingModule {
}
