import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {MapaZonaTarifadaRoutingModule} from './mapa-zona-tarifada-routing.module';
import {MapaZonaTarifadaComponent} from './mapa-zona-tarifada.component';
import {FileUploadModule} from 'primeng/primeng';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MangolModule} from 'mangol';
import {FileUploadService} from '../../shared/services/file-upload.service';
import {LoadingModule} from 'ngx-loading';

@NgModule({
    imports: [
        CommonModule,
        MapaZonaTarifadaRoutingModule,
        MangolModule, FormsModule, ReactiveFormsModule, FileUploadModule,
        LoadingModule
    ],
    declarations: [
        MapaZonaTarifadaComponent
    ],
    providers: [FileUploadService]
})
export class MapaZonaTarifadaModule {}
