import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AccesoService} from '../../../shared/services/acceso.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    pushRightClass = 'push-right';
    usuariouid: string = null;

    constructor(private translate: TranslateService, public router: Router, private accessoService: AccesoService) {
       // super(m);
        this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
        this.translate.setDefaultLang('en');
        const browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'en');


        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });
    }

    // messaging(rta: any) {
    //     rta.messages.forEach(msg => {
    //         this.messageService.add({severity: msg.type, summary: '', detail: msg.msg});
    //     });
    // }
    //
    // emitErrorMessage(error) {
    //     console.log(error);
    //     this.messaging({
    //         messages: [{
    //             type: 'error',
    //             msg: 'Error de comunicación con el servidor. Favor intente nuevamente y si persiste comuniquese con el administrador.'
    //         }]
    //     });
    // }


    ngOnInit() {
        this.usuariouid = localStorage.getItem('usuario_uid');
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    onLoggedout() {
        this.accessoService.logout({}).subscribe(rta => {
            //console.log(rta);
            //this.messaging(rta);
            if (rta.success) {
                setTimeout(() => {
                    this.router.navigate(['/login']);
                }, 2000);
            }
        });
    }

    changeLang(language: string) {
        this.translate.use(language);
    }
}
