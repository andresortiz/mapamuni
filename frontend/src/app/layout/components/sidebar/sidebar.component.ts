import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AccesoService} from '../../../shared/services/acceso.service';
import {LocalStorageService} from '../../../shared/services/local-storage.service';
import {SwUpdate} from '@angular/service-worker';
import {PERMISOS_T} from '../../../shared/permisos_t';


@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
    isActive = false;
    showMenu = '';
    pushRightClass = 'push-right';
    perfilCiudadano = true;
    perfilFuncionario = false;

    permisosUsuarioLogueado: string[];

    constructor(private translate: TranslateService, public router: Router, private accessoService: AccesoService,
                public localS: LocalStorageService, private updates: SwUpdate, public PERMISOS: PERMISOS_T) {
        this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de']);
        this.translate.setDefaultLang('en');
        const browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de/) ? browserLang : 'en');

        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });
    }

    ngOnInit() {
        this.permisosUsuarioLogueado = this.localS.get('perms') ? this.localS.get('perms') : [];
        console.log('permisosUsuarioLogueado: ', this.permisosUsuarioLogueado);
        this.perfilCiudadano = localStorage.getItem('perfil-ciudadano') === 'true';
        this.perfilFuncionario = !this.perfilCiudadano;
    }

    eventCalled() {
        this.isActive = !this.isActive;
    }

    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    changeLang(language: string) {
        this.translate.use(language);
    }

    onLoggedout() {
        this.accessoService.logout({}).subscribe(rta => {
            if (rta.success) {
                setTimeout(() => {
                    this.router.navigate(['/login']);
                }, 2000);
            }
        });
    }

    checkU() {
        console.log('active', this.updates.isEnabled);
        this.updates.checkForUpdate().then(algo => {
            console.log(algo);
        });
    }
}
