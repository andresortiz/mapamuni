import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {AccesoService} from '../shared/services/acceso.service';
import {SwPush} from '@angular/service-worker';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class LayoutComponent implements OnInit {

    constructor(private  accesoService: AccesoService, private swPush: SwPush,) {
    }

    ngOnInit() {
        this.accesoService.getPerfilUsuarioLogueado({silent: false});
    }

    abrirNuevoTabSenatics() {
        window.open('https://www.senatics.gov.py/', 'Customer Notes', 'width=800,height=600');
    }

    abrirNuevoTabInnovandoPyHackaton() {
        window.open('https://hackathon.innovando.gov.py/edicion-2018', 'Customer Notes', 'width=800,height=600');
    }

    // subscribeToNotifications() {
    //
    //     this.swPush.subscription.requestSubscription({
    //         serverPublicKey: this.VAPID_PUBLIC_KEY
    //     })
    //         .then(sub => this.newsletterService.addPushSubscriber(sub).subscribe())
    //         .catch(err => console.error('Could not subscribe to notifications', err));
    // }
}
