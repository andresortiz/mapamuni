import {Component, OnInit} from '@angular/core';
import {routerTransition} from '../../router.animations';
import {MangolConfig, MangolReady} from 'mangol';
import * as ol from 'openlayers';
import {MapBrowserEvent} from 'openlayers';
import {HttpClient} from '@angular/common/http';
import * as proj4x from 'proj4';
import {MangolMap} from 'mangol/src/app/classes/map.class';
import {isUndefined} from 'util';

const proj4 = (proj4x as any).default;

@Component({
    selector: 'app-registro-accidente',
    templateUrl: './registro-accidente.component.html',
    styleUrls: ['./registro-accidente.component.scss'],
    animations: [routerTransition()]
})
export class RegistroAccidenteComponent implements OnInit {
    motivos: string[];
    barrio: string = null;
    config = {} as MangolConfig;
    markerFeature = {} as ol.Feature;
    wgs84Sphere = new ol.Sphere(6378137);
    callesSource = new ol.source.Vector({features: []});
    barriosSource = new ol.source.Vector({features: []});
    reclamosSource = new ol.source.Vector({features: []});
    calleSelectedStyle = {} as ol.style.Style;

    getPosiblesMotivos() {
        this.motivos = [
            'ARBOL CAIDO',
            'SEMAFORO DESCOMPUESTO',
            'TRAFICO',
            'VEHICULO MAL ESTACIONADO',
            'OBSTRUCCION DE FRANJA PEATONAL',
            'OBSTACULIZAR EL TRANSITO',
            'DISTRACCION EN EL VOLANTE',
            'NO RESPETAR LA LUZ ROJA DEL SEMAFORO',
            'SISTEMA DE ILUMINACION',
            'NO DISMINUIR VELOCIDAD',
            'MANIOBRA DE RETROCESO SOLO SE REAL.EN PRES DE OBST',
            'EXCESO DE VELOCIDAD',
            'LA INT PROH GIRO IZQ. AVDAS,CALLES',
            'OTROS'
        ];
    }

    constructor(public http: HttpClient) {
    }

    onMapReady1(evt: MangolReady) {
        const mapa: MangolMap = evt.mapService.getMaps()[0];
        console.log('barrio ', this.barrio);
        mapa.on('singleclick', function (event: MapBrowserEvent) {
            const coords = event.coordinate;
            console.log('barrio ', this.barrio);
            let barrion = '';
            mapa.forEachFeatureAtPixel(mapa.getPixelFromCoordinate(coords), function (feature) {
                const props = feature.getProperties() as any;
                if (!isUndefined(props.sentido)) { // calle
                    // console.log('calle ', feature.getProperties().nombre);
                } else { // Barrio
                    barrion = props.nombre;
                    console.log('barrio ', props.nombre);
                }
                // scope.onZonaClick({id: feature.getId()});
            });
            this.barrio = barrion;
            this.marker_coords = coords;
            /*
            const ranking = this.callesSource.getFeatures().map((feature: ol.Feature, idx, arr) => {
                return {
                    'd': this.wgs84Sphere.haversineDistance(feature.getGeometry().getClosestPoint(coords), coords),
                    'f': feature
                };
            }, this);
            ranking.sort((tA, tB) => {
                if (tA.d < tB.d) {
                    return -1;
                } else if (tA.d > tB.d) {
                    return 1;
                } else {
                    return 0;
                }
            });

*/
            this.markerFeature.getGeometry().setCoordinates(coords);
            console.log('coord', coords);

            /*
                        console.log(this.callesSource.getFeatures());
                        console.log(ranking);
                        console.log('calle', ranking[0].f.getProperties().nombre);
                        console.log('fcoord', ranking[0].f.getGeometry().getCoordinates());
                        ranking[0].f.setStyle(this.calleSelectedStyle);


                        console.log('coord', coords);*/
        }, this);
        console.log(mapa);
    }

    ngOnInit() {
        this.getPosiblesMotivos();
        const geojson_p = '+proj=utm +zone=21 +south +datum=WGS84 +units=m +no_defs';
        proj4.defs(
            'UTMx', geojson_p
        );
        ol.proj.setProj4(proj4);

        const sourceFeatures = new ol.source.Vector(),
            layerFeatures = new ol.layer.Vector({source: sourceFeatures});

        this.calleSelectedStyle = new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: 'red',
                lineDash: [4],
                width: 10
            })
        });


        const markerStyle = new ol.style.Style({
            image: new ol.style.Icon(({
                scale: 0.08,
                rotateWithView: false,
                anchor: [0.5, 1],
                anchorXUnits: 'fraction',
                anchorYUnits: 'fraction',
                opacity: 1,
                src: 'assets/images/marker.png'
            })),
            zIndex: 5
        });
        this.markerFeature = new ol.Feature({
            type: 'click',
            desc: 'marker',
            geometry: new ol.geom.Point([-5997554.987368069, 17386060.705633048])
        });
        this.markerFeature.setStyle(markerStyle);
        sourceFeatures.addFeature(this.markerFeature);

        // const vectorSource = new ol.source.Vector({
        //     features: (new ol.format.GeoJSON()).readFeatures(geojsonObject)
        // });

        const image = new ol.style.Circle({
            radius: 5,
            fill: null,
            stroke: new ol.style.Stroke({color: 'red', width: 1})
        });

        const styles = {
            'Point': new ol.style.Style({
                image: image
            }),
            'LineString': new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: '#000000',
                    width: 1
                })
            }),
            'MultiLineString': new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: 'green',
                    width: 1
                })
            }),
            'MultiPoint': new ol.style.Style({
                image: image
            }),
            'MultiPolygon': new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: 'yellow',
                    width: 1
                }),
                fill: new ol.style.Fill({
                    color: 'rgba(255, 255, 0, 0.1)'
                })
            }),
            'Polygon': new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: 'blue',
                    lineDash: [4],
                    width: 3
                }),
                fill: new ol.style.Fill({
                    color: 'rgba(0, 0, 255, 0.1)'
                })
            }),
            'GeometryCollection': new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: 'magenta',
                    width: 2
                }),
                fill: new ol.style.Fill({
                    color: 'magenta'
                }),
                image: new ol.style.Circle({
                    radius: 10,
                    fill: null,
                    stroke: new ol.style.Stroke({
                        color: 'magenta'
                    })
                })
            }),
            'Circle': new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: 'red',
                    width: 2
                }),
                fill: new ol.style.Fill({
                    color: 'rgba(255,0,0,0.2)'
                })
            })
        };

        const styleFunction = function (feature) {
            return styles[feature.getGeometry().getType()];
        };


        this.http.get('assets/calles.geojson').subscribe((data: any) => {
            data.features.forEach(function (item) {

                const feature = (new ol.format.GeoJSON()).readFeature(item, {
                    dataProjection: 'UTMx',
                    featureProjection: 'EPSG:900913'
                });

                this.callesSource.addFeature(feature);
            }, this);
        });

        this.http.get('assets/barrios.geojson').subscribe((data: any) => {
            data.features.forEach(function (item) {

                const feature = (new ol.format.GeoJSON()).readFeature(item, {
                    dataProjection: 'UTMx',
                    featureProjection: 'EPSG:900913'
                });

                this.barriosSource.addFeature(feature);
            }, this);
        });


        this.config = {
            map: {
                renderer: 'canvas',
                target: 'reclamo-page',
                view: {
                    projection: 'EPSG:900913',
                    center: ol.proj.fromLonLat(
                        [-57.5885059, -25.3119575],
                        'EPSG:900913'
                    ),
                    zoom: 13
                },
                layertree: {
                    layers: [
                        {
                            name: 'OpenStreetMap layer',
                            description: 'A sample description',
                            visible: true,
                            opacity: 1,
                            layer: new ol.layer.Tile({
                                source: new ol.source.OSM()
                            })
                        },
                        /*{
                            name: 'calles layer',
                            description: 'A sample description',
                            visible: true,
                            opacity: 1,
                            layer: new ol.layer.Vector({
                                source: this.callesSource,
                                style: styleFunction
                            })
                        },*/
                        {
                            name: 'barrios layer',
                            description: 'A sample description',
                            visible: true,
                            opacity: 0,
                            layer: new ol.layer.Vector({
                                source: this.barriosSource,
                                style: styleFunction
                            })
                        },
                        {
                            name: 'calles layer',
                            description: 'A sample description',
                            visible: true,
                            opacity: 1,
                            layer: layerFeatures,
                        },
                    ],
                },
                controllers: {
                    tileLoad: true
                }
            }
        } as MangolConfig;
    }
}
