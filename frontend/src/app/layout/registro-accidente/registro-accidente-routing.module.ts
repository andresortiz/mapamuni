import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RegistroAccidenteComponent} from './registro-accidente.component';

const routes: Routes = [
    {
        path: '', component: RegistroAccidenteComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [RouterModule]
})
export class RegistroAccidenteRoutingModule {
}
