import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PageHeaderModule} from './../../shared';
import {MangolModule} from 'mangol';
import {RegistroAccidenteComponent} from './registro-accidente.component';
import {RegistroAccidenteRoutingModule} from './registro-accidente-routing.module';

@NgModule({
    imports: [CommonModule, RegistroAccidenteRoutingModule, PageHeaderModule, MangolModule],
    declarations: [RegistroAccidenteComponent]
})
export class RegistroAccidenteModule {
}
