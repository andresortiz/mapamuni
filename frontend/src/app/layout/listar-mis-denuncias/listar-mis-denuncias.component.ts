import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DenunciaService} from '../../shared/services/denuncia.service';
import {EmiterDenunciaService} from '../../shared/services/emiter-denuncia.service';
import {Denuncia} from '../../shared/domain/denuncia';
import {PushService} from "../../shared/services/push.service";

@Component({
    selector: 'app-listar-mis-denuncias',
    templateUrl: './listar-mis-denuncias.component.html',
    styleUrls: ['./listar-mis-denuncias.component.scss']
})
export class ListarMisDenunciasComponent implements OnInit {

    denuncias: any[];

    // Paginación
    // Total de registros
    cnt_denuncias = 1;
    // Filas
    xpag = 10;

    constructor(private router: Router,
                private denunciaService: DenunciaService,
                private emiterDenunciaService: EmiterDenunciaService, private pushService: PushService) {
    }

    ngOnInit() {
        this.denuncias = [];
        // Obtener mis denuncias paginadas con parámetros por defecto incialmente
        this.getDenunciasUsuarioLogueado(1, 10);

    }

    getDenunciasUsuarioLogueado(page, xpag) {
        this.denunciaService.getDenunciasUsuarioLogueado(page, xpag).subscribe(
            resp => {
                if (resp != null && resp.success === true) {
                    this.denuncias = resp.result.objects;
                    this.cnt_denuncias = resp.result.count;
                }
            }
        );
    }

    agregarDenuncia() {
        this.pushService.subscribeToNotifications();
        this.router.navigate(['/crear-mi-denuncia']);
    }

    verDetalle(denuncia: Denuncia) {
        this.router.navigate(['/seguir-mis-denuncias']);
        console.log('=> denuncia a emitir: ', denuncia);
        // emitir a otro componente
        this.emiterDenunciaService.emitirDenuncia(denuncia);
    }

    paginate(event) {
        console.log('paginate desde vista: ', event);
        // Obtener mis denuncias paginadas
        this.getDenunciasUsuarioLogueado(event.page + 1, event.rows);
    }


}
