import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {ListarMisDenunciasComponent} from './listar-mis-denuncias.component';


describe('ListarMisDenunciasComponent', () => {
  let component: ListarMisDenunciasComponent;
  let fixture: ComponentFixture<ListarMisDenunciasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarMisDenunciasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarMisDenunciasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
