import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListarMisDenunciasComponent} from './listar-mis-denuncias.component';

const routes: Routes = [
    {
        path: '', component: ListarMisDenunciasComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [RouterModule]
})
export class ListarMisDenunciasRoutingModule {
}
