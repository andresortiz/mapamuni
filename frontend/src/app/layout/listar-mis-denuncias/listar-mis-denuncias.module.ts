import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {DenunciaService} from '../../shared/services/denuncia.service';
import {ListarMisDenunciasRoutingModule} from './listar-mis-denuncias-routing.module';
import {ListarMisDenunciasComponent} from './listar-mis-denuncias.component';
import {PaginatorModule} from 'primeng/primeng';

@NgModule({
    imports: [
        CommonModule, ListarMisDenunciasRoutingModule, FormsModule, NgbModule.forRoot(), PaginatorModule
    ],
    declarations: [ListarMisDenunciasComponent],
    providers: [DenunciaService]
})
export class ListarMisDenunciasModule {
}
