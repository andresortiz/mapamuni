import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlankPageRoutingModule } from './blank-page-routing.module';
import { BlankPageComponent } from './blank-page.component';
import {MangolModule} from 'mangol';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FileUploadModule} from 'primeng/primeng';
import {FileUploadService} from '../../shared/services/file-upload.service';



@NgModule({
    imports: [CommonModule, BlankPageRoutingModule, MangolModule, FormsModule, ReactiveFormsModule, FileUploadModule],
    declarations: [BlankPageComponent],
    providers: [FileUploadService]
})
export class BlankPageModule {}
