import {Component, OnDestroy, OnInit} from '@angular/core';
import {MangolConfig, MangolReady} from 'mangol';
import * as ol from 'openlayers';
import {MapBrowserEvent} from 'openlayers';
import {HttpClient} from '@angular/common/http';
import * as proj4x from 'proj4';
import {MangolMap} from 'mangol/src/app/classes/map.class';
import {isUndefined} from 'util';
import {IMqttMessage, IMqttServiceOptions, MqttService} from 'ngx-mqtt';
import {Subscription} from 'rxjs/Rx';
import {IOnConnectEvent} from 'ngx-mqtt/src/mqtt.model';
import {MessageService} from 'primeng/components/common/messageservice';
import {Subject} from 'rxjs/Subject';
import {UUID} from 'angular2-uuid';
import 'rxjs/add/operator/debounceTime';
import {FileUploadService} from '../../shared/services/file-upload.service';

const proj4 = (proj4x as any).default;

@Component({
    selector: 'app-blank-page',
    templateUrl: './blank-page.component.html',
    styleUrls: ['./blank-page.component.scss']
})

export class BlankPageComponent implements OnInit, OnDestroy {
    config = {} as MangolConfig;
    markerFeature = {} as ol.Feature;
    carFeature: ol.Feature = null;
    callesSource = new ol.source.Vector({features: []});

    barriosSource = new ol.source.Vector({features: []});
    distritoSource = new ol.source.Vector({features: []});
    reclamosSource = new ol.source.Vector({features: []});
    barrioLayer: ol.layer.Vector = null;
    calleLayer: any = null;
    distritoLayer: any = null;

    calleSelectedStyle = {} as ol.style.Style;

    sampleCoords: Array<any> = [];

    showBarrios = true;
    showCalles = true;

    denunciasMapper: object = {};

    routeindex = 0;
    dir = true;
    mapa: MangolMap = null;
    anho_filter = '';
    mes_filter = '';
    personas: any[] = [];

    private mapClick: Subject<MapBrowserEvent>;
    private mapViewChange: Subject<MapBrowserEvent>;

    // para mqtt
    private subscription: Subscription;
    public message: string;

    public anho_subject: Subject<string>;

    uploadedFiles: any[] = [];
    uploadedFilesIds: any[] = [];

    constructor(public http: HttpClient,
                private _mqttService: MqttService, private messageService: MessageService, private uploadService: FileUploadService) {
        this.subscription = this._mqttService.observe('my/topic').subscribe((message: IMqttMessage) => {
            //  this.message = message.payload.toString();
            this.mes_filter = message.payload.toString();
            console.log(message.payload.toString());
            this.messageService.add({severity: 'info', summary: 'mensaje de mqtt', detail: this.message});
        });
        this._mqttService.onConnect.subscribe((event: IOnConnectEvent) => {

        });
        this.subscription = this._mqttService.observe('my/location').subscribe((message: IMqttMessage) => {
            const msg = message.payload.toString();
            if (this.carFeature) {
                (<any>this.carFeature.getGeometry()).setCoordinates(ol.proj.transform(JSON.parse(msg).c, 'UTMx', 'EPSG:900913'));
            }
        });
        this.mapClick = new Subject<MapBrowserEvent>();
    }

    sendMqttMessage(message) {
        this._mqttService.unsafePublish('my/topic', message);
    }

    onSelectionBarriosChange(val: boolean) {
        console.log(val);
        this.showBarrios = val;
        this.barrioLayer.setVisible(val);
    }

    onSelectionCallesChange(val: boolean) {
        console.log(val);
        this.showCalles = val;
        this.calleLayer.setVisible(val);
    }

    onMapReady(evt: MangolReady) {
        const mapa: MangolMap = evt.mapService.getMaps()[0];
        this.mapa = mapa;

        mapa.on('singleclick', function (event: MapBrowserEvent) {
            const coords = event.coordinate;
            mapa.forEachFeatureAtPixel(mapa.getPixelFromCoordinate(coords), function (feature) {
                const props = feature.getProperties() as any;
                if (!isUndefined(props.sentido)) { // calle
                    console.log('calle ', props.nombre);
                } else if (!isUndefined(props.zona)) { // Barrio
                    console.log('barrio ', props.nombre);
                } else {
                    console.log('zona ', props.DESCRIPCIO);
                }
                // console.log(ol.sphere.WGS84.haversineDistance);
                // scope.onZonaClick({id: feature.getId()});
            });
            /*
            const ranking = this.callesSource.getFeatures().map((feature: ol.Feature, idx, arr) => {
                return {
                    'd': this.wgs84Sphere.haversineDistance(feature.getGeometry().getClosestPoint(coords), coords),
                    'f': feature
                };
            }, this);
            ranking.sort((tA, tB) => {
                if (tA.d < tB.d) {
                    return -1;
                } else if (tA.d > tB.d) {
                    return 1;
                } else {
                    return 0;
                }
            });

*/
            const utmcoords = ol.proj.transform(coords, 'EPSG:900913', 'UTMx');
            console.log('coord', coords, utmcoords);
            this.markerFeature.getGeometry().setCoordinates(coords);
            //this.unsafePublish('my/location', JSON.stringify(utmcoords));
            // ol.proj.transform(coords, 'EPSG:900913', 'UTMx');
            /*
                        console.log(this.callesSource.getFeatures());
                        console.log(ranking);
                        console.log('calle', ranking[0].f.getProperties().nombre);
                        console.log('fcoord', ranking[0].f.getGeometry().getCoordinates());
                        ranking[0].f.setStyle(this.calleSelectedStyle);


                        console.log('coord', coords);*/
        }, this);
        const o: Subject<ol.View> = new Subject();
        const view = mapa.getView();
        view.on('change:center', event => {
            o.next(view);
        });
        o.asObservable().subscribe(vw => {
            const utmcoords = ol.proj.transform(view.getCenter(), 'EPSG:900913', 'UTMx');
            console.log(view.getCenter(), view.getZoom());
            this.unsafePublish('my/location', JSON.stringify({zoom: view.getZoom(), c: utmcoords}));
        });
        console.log(mapa);
    }

    anho_changed(value: string) {
        console.log('logi', value);
        this.anho_subject.next(value);
    }


    onUpload(event) {
        // console.log(event.xhr);

        const ids = this.uploadService.procesarUpload(event.xhr)
        for (const idf of ids) {
            this.uploadedFilesIds.push(idf);

        }
        // console.log(this.uploadedFiles);
        for (const file of event.files) {
            this.uploadedFiles.push(file);
            // console.log(file);
        }

        //this.msgs = [];
        //this.msgs.push({severity: 'info', summary: 'File Uploaded', detail: ''});
    }

    onClearUploads() {
        console.log(this.uploadedFiles);
    }

    formatSize(bytes) {
        if (bytes === 0) {
            return '0 B';
        }
        const k = 1000,
            dm = 3,
            sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
            i = Math.floor(Math.log(bytes) / Math.log(k));

        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }

    removeFile(event, file) {
        const idx = this.uploadedFiles.indexOf(file);
        this.uploadedFiles.splice(idx, 1);
        this.uploadedFilesIds.splice(idx, 1);
        console.log(this.uploadedFiles, this.uploadedFilesIds);
    }

    ngOnInit() {
        const geojson_p = '+proj=utm +zone=21 +south +datum=WGS84 +units=m +no_defs';
        proj4.defs(
            'UTMx', geojson_p
        );
        ol.proj.setProj4(proj4);
        const mycomp_uuid = UUID.UUID();
        this.anho_subject = new Subject<string>();
        this.anho_subject.asObservable().debounceTime(200).subscribe(value => {
            this.unsafePublish('mapamuni/z/get-personas/uuid/' + mycomp_uuid, value);
        });

        this.subscription = this._mqttService.observe('mapamuni/z/get-personas/uuid/' + mycomp_uuid + '/r').subscribe((message: IMqttMessage) => {
            const msg = JSON.parse(message.payload.toString());
            console.log(msg);
            this.personas = msg;
            // if (msg) {
            //     this.mes_filter = msg.fullname;
            // } else {
            //     this.mes_filter = '{not found}';
            // }
        });

        const sourceFeatures = new ol.source.Vector(),
            layerFeatures = new ol.layer.Vector({source: sourceFeatures});

        this.calleSelectedStyle = new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: 'red',
                lineDash: [4],
                width: 10
            })
        });


        const markerStyle = new ol.style.Style({
            image: new ol.style.Icon(({
                scale: 0.08,
                rotateWithView: false,
                anchor: [0.5, 1],
                anchorXUnits: 'fraction',
                anchorYUnits: 'fraction',
                opacity: 1,
                src: 'assets/images/marker.png'
            })),
            zIndex: 5
        });
        this.markerFeature = new ol.Feature({
            type: 'click',
            desc: 'marker',
            geometry: new ol.geom.Point(ol.proj.fromLonLat(
                [-57.5885059, -25.3119575],
                'EPSG:900913'
            ))
        });
        this.markerFeature.setStyle(markerStyle);
        sourceFeatures.addFeature(this.markerFeature);


        const carStyle = new ol.style.Style({
            image: new ol.style.Icon(({
                scale: 0.7,
                rotateWithView: false,
                anchor: [0.5, 0.5],
                anchorXUnits: 'fraction',
                anchorYUnits: 'fraction',
                opacity: 1,
                src: 'assets/images/car3.png'
            })),
            zIndex: 5
        });
        this.carFeature = new ol.Feature({
            type: 'click',
            desc: 'marker',
            geometry: new ol.geom.Point(ol.proj.fromLonLat(
                [-57.5885059, -25.3119575],
                'EPSG:900913'
            ))
        });
        this.carFeature.setStyle(carStyle);
        sourceFeatures.addFeature(this.carFeature);

        // const vectorSource = new ol.source.Vector({
        //     features: (new ol.format.GeoJSON()).readFeatures(geojsonObject)
        // });

        const image = new ol.style.Circle({
            radius: 5,
            fill: null,
            stroke: new ol.style.Stroke({color: 'red', width: 1})
        });

        const styles = {
            'Point': new ol.style.Style({
                image: image
            }),
            'LineString': new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: '#000000',
                    width: 1
                })
            }),
            'MultiLineString': new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: 'green',
                    width: 1
                })
            }),
            'MultiPoint': new ol.style.Style({
                image: image
            }),
            'MultiPolygon': new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: 'yellow',
                    width: 1
                }),
                fill: new ol.style.Fill({
                    color: 'rgba(255, 255, 0, 0.1)'
                })
            }),
            'Polygon': new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: 'blue',
                    lineDash: [4],
                    width: 3
                }),
                fill: new ol.style.Fill({
                    color: 'rgba(0, 0, 255, 0.1)'
                })
            }),
            'GeometryCollection': new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: 'magenta',
                    width: 2
                }),
                fill: new ol.style.Fill({
                    color: 'magenta'
                }),
                image: new ol.style.Circle({
                    radius: 10,
                    fill: null,
                    stroke: new ol.style.Stroke({
                        color: 'magenta'
                    })
                })
            }),
            'Circle': new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: 'red',
                    width: 2
                }),
                fill: new ol.style.Fill({
                    color: 'rgba(255,0,0,0.2)'
                })
            })
        };

        const styleFunction = function (feature) {
            return styles[feature.getGeometry().getType()];
        };

        // const callesSource = new ol.source.Vector({features: []});
        // const barriosSource = new ol.source.Vector({features: []});


        // this.http.get('assets/calles.geojson').subscribe((data: any) => {
        //     // console.log(data);
        //     data.features.forEach(function (item) {
        //
        //         const feature = (new ol.format.GeoJSON()).readFeature(item, {
        //             dataProjection: 'UTMx',
        //             featureProjection: 'EPSG:900913'
        //         });
        //
        //         const props = feature.getProperties() as any;
        //         if (props.nombre === 'AVENIDA MARISCAL FRANCISCO SOLANO LOPEZ') {
        //             this.sampleCoords.push((<any> feature.getGeometry()).getCoordinates()[0]);
        //         }
        //
        //         this.callesSource.addFeature(feature);
        //         // console.log(feature);
        //     }, this);
        //
        //     console.log('sample size ', this.sampleCoords.length);
        //
        //     const cCompare: any = [-6421168.998124516, -2907846.0939102825];
        //     const pCompare: any = new ol.geom.Point(this.sampleCoords[0]);
        //     this.sampleCoords.sort((cA, cB) => {
        //         const da = ol.Sphere.getLength(new ol.geom.LineString([cCompare, cA]));
        //         const db = ol.Sphere.getLength(new ol.geom.LineString([cCompare, cB]));
        //         // const da = pCompare.distanceTo(new ol.geom.Point(cA)), db = pCompare.distanceTo(new ol.geom.Point(cB));
        //         if (da > db) return 1;
        //         if (da < db) return -1;
        //         return 0;
        //     });
        //
        //     const yo = this;
        //
        //     const moveFeature = function (event) {
        //         //console.log('owrking');
        //         // if (this.carFeature == null) {
        //         //    return;
        //         // }
        //         // console.log('xking');
        //         const vectorContext = event.vectorContext;
        //         const frameState = event.frameState;
        //
        //         console.log(this);
        //         // here the trick to increase speed is to jump some indexes
        //         // on lineString coordinates
        //         yo.routeindex += yo.dir ? 1 : -1;
        //
        //         if (yo.dir && yo.routeindex >= yo.sampleCoords.length - 1) {
        //             yo.dir = !yo.dir;
        //         }
        //         if (!yo.dir && yo.routeindex <= 0) {
        //             yo.dir = !yo.dir;
        //         }
        //
        //         //const currentPoint = new ol.geom.Point(yo.sampleCoords[yo.routeindex]);
        //         //console.log((<any> yo.carFeature.getGeometry()).getCoordinates());
        //         console.log(yo.sampleCoords[yo.routeindex], yo.routeindex);
        //
        //         (<any> yo.carFeature.getGeometry()).setCoordinates(yo.sampleCoords[yo.routeindex]);
        //         //var feature = new ol.Feature(currentPoint);
        //         ///vectorContext.drawFeature(feature, styles.geoMarker);
        //
        //         // tell OpenLayers to continue the postcompose animation
        //         yo.mapa.render();
        //     };
        //
        //    // this.mapa.on('postcompose', moveFeature);
        //     this.mapa.render();
        // });


        // this.http.get('assets/barrios.geojson').subscribe((data: any) => {
        //     // console.log(data);
        //     data.features.forEach(function (item) {
        //
        //         const feature = (new ol.format.GeoJSON()).readFeature(item, {
        //             dataProjection: 'UTMx',
        //             featureProjection: 'EPSG:900913'
        //         });
        //
        //         this.barriosSource.addFeature(feature);
        //         // console.log(feature);
        //     }, this);
        // });

        this.subscription = this._mqttService.observe('my/streets').subscribe((message: IMqttMessage) => {
            console.log(message.payload.toString());
            const feature: ol.Feature = (new ol.format.GeoJSON()).readFeature(message.payload.toString(), {
                dataProjection: 'UTMx',
                featureProjection: 'EPSG:900913'
            });
            console.log(feature);
            if (!isUndefined(feature) && !isUndefined(feature.getId())) {
                console.log(feature);
                const feature_d = this.callesSource.getFeatureById(feature.getId());
                if (feature_d) {
                    this.callesSource.removeFeature(feature_d);
                }
            }
            this.callesSource.addFeature(feature);
            // this.message = message.payload.toString();
            // this.mes_filter = message.payload.toString();
            //console.log(message.payload.toString());
            // this.messageService.add({severity: 'info', summary: 'mensaje de mqtt', detail: this.message});
        });

        this.http.get('assets/distritos.geojson').subscribe((data: any) => {
            // console.log(data);
            data.features.forEach(function (item) {

                const feature = (new ol.format.GeoJSON()).readFeature(item, {
                    dataProjection: 'UTMx',
                    featureProjection: 'EPSG:900913'
                });

                this.distritoSource.addFeature(feature);
                // console.log(feature);
            }, this);
        });

        // this.http.get('assets/reclamos_ciudadanos.json').subscribe((data: any) => {
        //     // console.log(data);
        //     data.forEach(function (item) {
        //         if (item.fecha_reclamo.startsWith('2017')) {
        //             const coord = ol.proj.transform([item.longitud, item.latitud], 'EPSG:4326', 'EPSG:900913')
        //             const feature = new ol.Feature({
        //                 name: item.cod_reclamo,
        //                 geometry: new ol.geom.Point(coord
        //                 )
        //             });
        //
        //             this.reclamosSource.addFeature(feature);
        //             this.denunciasMapper[item.fecha_reclamo] = feature;
        //         }
        //         // console.log([item.latitud, item.longitud], coord);
        //     }, this);
        // });

        /*$http({method: 'GET', url: 'assets/calles.geojson'}).then(function (response) {
            angular.forEach(response.data, function (item) {

                const feature = (new ol.format.GeoJSON()).readFeature(item, {
                    dataProjection: 'utm',
                    featureProjection: 'EPSG:900913'
                });

                vectorSource.addFeature(feature);
            });
        });*/

        // const vectorLayer = new ol.layer.Vector({
        //     source: vectorSource,
        //     style: styleFunction
        // });

        const heatmapLayer = new ol.layer.Heatmap({
            source: this.reclamosSource,
            opacity: 0,
            weight: '0.5',
            // blur: 10,
            radius: 3,
        });

        this.barrioLayer = new ol.layer.Vector({
            source: this.barriosSource,
            style: styleFunction
        });

        this.calleLayer = new ol.layer.Vector({
            source: this.callesSource,
            style: styleFunction
        });

        this.distritoLayer = new ol.layer.Vector({
            source: this.distritoSource,
            style: styleFunction
        });

        const viewC = {
            projection: 'EPSG:900913',
            center: ol.proj.fromLonLat(
                [-57.5885059, -25.3119575],
                'EPSG:900913'
            ),
            zoom: 13
        };
        // viewC.on('change:center', event => {
        //     console.log(viewC.getCenter());
        // });


        this.config = {
            map: {
                renderer: 'canvas',
                target: 'blank-page',
                view: viewC,
                // view: {
                //     projection: 'EPSG:900913',
                //     center: ol.proj.fromLonLat(
                //         [-57.5885059, -25.3119575],
                //         'EPSG:900913'
                //     ),
                //     zoom: 13
                // },
                layertree: {
                    layers: [
                        {
                            name: 'OpenStreetMap layer',
                            description: 'A sample description',
                            visible: true,
                            opacity: 1,
                            layer: new ol.layer.Tile({
                                source: new ol.source.OSM()
                            })
                        },
                        {
                            name: 'calles layer',
                            description: 'A sample description',
                            visible: true,
                            opacity: 1,
                            layer: this.calleLayer,
                        },
                        {
                            name: 'barrios layer',
                            description: 'A sample description',
                            visible: true,
                            opacity: 1,
                            layer: this.barrioLayer,
                        },
                        {
                            name: 'marker layer',
                            description: 'A sample description',
                            visible: true,
                            opacity: 1,
                            layer: layerFeatures,
                        },
                        {
                            name: 'distrito layer',
                            description: 'A sample description',
                            visible: true,
                            opacity: 1,
                            layer: this.distritoLayer,
                        },

                        {
                            name: 'heat layer',
                            description: 'A saddmple description',
                            visible: true,
                            opacity: 1,
                            layer: heatmapLayer,
                        },
                    ],
                },
                controllers: {
                    // mousePosition: {},
                    scaleLine: {
                        units: 'metric'
                    },
                    /*quickSearch: {
                        items: [
                            {
                                text: 'Budapest',
                                details: 'Capital of Hungary',
                                extent: [2108491, 6010126, 2134556, 6039783]
                            },
                            {
                                text: 'London',
                                details: 'Capital of England & UK',
                                coordinates: [-13664, 6711101]
                            },
                            {
                                text: 'Paris',
                                details: 'Capital of France',
                                extent: [250839, 6235856, 272853, 6263067]
                            }
                        ]
                    },*/
                    fullScreen: {},
                    tileLoad: true
                },
                sidebar: {
                    collapsible: true,
                    opened: true,
                    toolbar: {
                        // Miminal configuration
                        featureinfo: {}
                    }
                }
            }
        } as MangolConfig;
    }

    public unsafePublish(topic: string, message: string): void {
        this._mqttService.unsafePublish(topic, message, {qos: 1, retain: false});
    }

    public ngOnDestroy() {
        if (this.subscription != null) {
            this.subscription.unsubscribe();
        }
    }
}
