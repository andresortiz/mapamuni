import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {EmiterIntervencionService} from '../../shared/services/emiter-intervencion.service';
import {EmiterUrlOrigenService} from '../../shared/services/emiter-url-origen.service';
import {Denuncia} from '../../shared/domain/denuncia';
import {PersonaSugerida} from '../../shared/domain/persona-sugerida';
import {EmiterDenunciaService} from '../../shared/services/emiter-denuncia.service';
import {Intervencion} from '../../shared/domain/intervencion';
import {interaction} from 'openlayers';
import {IntervencionService} from '../../shared/services/intervencion.service';

@Component({
    selector: 'app-detalle-intervencion',
    templateUrl: './detalle-intervencion.component.html',
    styleUrls: ['./detalle-intervencion.component.scss']
})
export class DetalleIntervencionComponent implements OnInit {

    denunciaPreliminar: Denuncia;
    tipoIntervencion: string;
    motivoIntervencion: string;
    nombre: string;
    descripcion: string;
    involucrados: string[];
    estado: string;
    fechaCreacion: string;

    intervencion: Intervencion;

    private getIntervencionSuscripcion: Subscription;

    constructor(private emiterIntervencionService: EmiterIntervencionService,
                private emiterDenunciaService: EmiterDenunciaService,
                private emiterUrlOrigenService: EmiterUrlOrigenService,
                private intervencionService: IntervencionService,
                private router: Router) {
    }

    ngOnInit() {
        // Recibir denuncia
        this.getIntervencionSuscripcion = this.emiterIntervencionService.intervencionEmitida.subscribe(
            rta => {
                console.log('=> intervencion recibida: ', rta);
                if (rta != null) {
                    this.intervencion = rta;
                    this.denunciaPreliminar = rta.denuncia_preliminar;
                    this.tipoIntervencion = rta.motivo.tipo.nombre;
                    this.motivoIntervencion = rta.motivo.nombre;
                    this.nombre = rta.nombre;
                    this.descripcion = rta.descripcion;
                    this.estado = rta.estado.nombre;
                    this.fechaCreacion = rta.fecha_creacion;
                    this.involucrados = rta.involucrados;
                }
            },
            error => {
                console.log(error);
            }
        );
    }

    ngOnDestroy() {
        if (this.getIntervencionSuscripcion != null) {
            this.getIntervencionSuscripcion.unsubscribe();
        }
    }

    verDetalleDenuncia() {
        // emitir denunciaPreliminar
        this.emiterDenunciaService.emitirDenuncia(this.denunciaPreliminar);
        // Emitir la url de retorno
        this.emiterUrlOrigenService.emitirUrlOrigen('/detalle-intervencion');
        // redirigir al detalle de la denuncia
        this.router.navigate(['/detalle-denuncia']);
    }

    volver() {
        // redirigir a la pagina de donde vino (listar-intervencion)
        // this.emiterUrlOrigenService.urlOrigenEmitida.subscribe(
        //     rta => {
        //         if (rta != null) {
        //             this.router.navigate([rta]);
        //         } else {
        //             this.router.navigate(['']);
        //         }
        //     },
        //     error => {
        //         console.log(error)
        //     }
        // );
        this.router.navigate(['/listar-intervencion']);
    }

    finalizarIntervencion() {
        debugger;
        this.intervencionService.finalizarIntervencion(this.intervencion.id).subscribe(
            rta => {
                debugger;
                if (rta.success) {
                    console.log('finalizó con exito');
                }
            }
        );
    }

    cancelar() {
        // Redirigir al listado
        this.router.navigate(['/listar-intervencion']);
    }

}
