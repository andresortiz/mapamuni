import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleIntervencionComponent } from './detalle-intervencion.component';

describe('DetalleIntervencionComponent', () => {
  let component: DetalleIntervencionComponent;
  let fixture: ComponentFixture<DetalleIntervencionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleIntervencionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleIntervencionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
