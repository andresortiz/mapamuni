import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MangolModule} from 'mangol';
import {DenunciaService} from '../../shared/services/denuncia.service';
import {FormsModule} from '@angular/forms';
import {FileUploadService} from '../../shared/services/file-upload.service';
import {FileUploadModule} from 'primeng/primeng';
import {SharedModule} from '../../shared/shared.module';
import {DetalleIntervencionComponent} from './detalle-intervencion.component';
import {DetalleIntervencionRoutingModule} from './detalle-intervencion-routing.module';
import {IntervencionService} from '../../shared/services/intervencion.service';

@NgModule({
    imports: [CommonModule, DetalleIntervencionRoutingModule, MangolModule, FormsModule, FileUploadModule, SharedModule],
    declarations: [DetalleIntervencionComponent],
    providers: [DenunciaService, FileUploadService, IntervencionService]
})
export class DetalleIntervencionModule {

}
