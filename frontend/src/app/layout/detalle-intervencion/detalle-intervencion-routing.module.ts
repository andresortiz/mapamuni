import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DetalleIntervencionComponent} from './detalle-intervencion.component';

const routes: Routes = [
    {
        path: '', component: DetalleIntervencionComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DetalleIntervencionRoutingModule { }
