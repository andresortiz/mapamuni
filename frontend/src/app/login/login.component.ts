import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {routerTransition} from '../router.animations';
import {AuthService, FacebookLoginProvider, GoogleLoginProvider} from 'angular-6-social-login';
import {AccesoService} from '../shared/services/acceso.service';
import {sha256} from 'js-sha256';
import {EmiterAccesoService} from '../shared/services/emiter-acceso.service';
import {LocalStorageService} from '../shared/services/local-storage.service';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    usuario: any = '';
    password: any = '';
    body: any;
    userData: any;
    userInfoToEmit: any;

    constructor(public router: Router,
                private socialAuthService: AuthService,
                private  accesoService: AccesoService,
                private emiterAccesoService: EmiterAccesoService,
                private localS: LocalStorageService) {
        //  private messageService: MessageService) {
        this.password = '';
    }

    ngOnInit() {
        this.accesoService.getUsuarioLogueado({silent: false});
    }


    onLoggedin() {
        const body = {uid: this.usuario, password: sha256(this.password)};
        this.accesoService.login(body).subscribe(
            rta => {
                if (rta.success) {
                    console.log(rta);
                    this.localS.set('perms', rta.result.perms);
                    this.localS.set('logged', true);
                    if (rta.result.perms != null && rta.result.perms.length > 0) {
                        // el usuario logueado tiene permisos especiales, entonces presentarle la vista de selección de perfiles
                        this.router.navigate(['/perfil']);
                    } else {
                        // sino se trata de un ciudadano con único perfil
                        // redirigir a la aplicación.
                        setTimeout(() => {
                            localStorage.setItem('perfil-ciudadano', '' + (true));
                            this.router.navigate(['']);
                        }, 3000);
                    }
                }
            });
    }

    public socialSignIn(socialPlatform: string) {
        if (this.accesoService.checkOnline()) {
            let socialPlatformProvider;
            if (socialPlatform === 'facebook') {
                socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
            } else if (socialPlatform === 'google') {
                socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
            }

            this.socialAuthService.signIn(socialPlatformProvider).then(
                (userData) => {
                    this.userData = userData;
                    console.log(socialPlatform + ' sign in data : ', userData);
                    // var id_token = userData.getAuthResponse().id_token
                    // Now sign-in with userData
                    // ...;
                    if (userData != null) {
                        if (userData.provider === 'google') {
                            this.body = {token_gg: userData.idToken};
                        } else {
                            this.body = {token_fb: userData.token};
                        }
                        this.accesoService.login(this.body).subscribe(
                            rta => {
                                if (rta.success === false) {
                                    console.log('social', rta);
                                    if (rta.result.google != null) {
                                        // rta.result.google;
                                        this.userInfoToEmit = rta.result.google;
                                    } else if (rta.result.fb != null) {
                                        // enviar datos del usuario logueado pero no registrado al formulario del componente registro
                                        // rta.result.fb
                                        this.userInfoToEmit = rta.result.fb;
                                    }
                                    // emitir
                                    this.emiterAccesoService.emitirUserInfo(this.userInfoToEmit);
                                    // redirigir a la pagina de registro de usuario
                                    setTimeout(() => {
                                        this.router.navigate(['/signup']);
                                    }, 3000);
                                } else {
                                    // redirigir a la aplicación
                                    this.localS.set('perms', rta.result.perms);
                                    this.localS.set('logged', true);
                                    if (rta.result.perms != null && rta.result.perms.length > 0) {
                                        // el usuario logueado tiene permisos especiales, entonces presentarle la vista de selección de perfiles
                                        this.router.navigate(['/perfil']);
                                    } else {
                                        setTimeout(() => {
                                            localStorage.setItem('perfil-ciudadano', '' + (true));
                                            this.router.navigate(['']);
                                        }, 3000);
                                    }
                                }
                            }
                        );
                    }

                }
            );
        }
    }
}
