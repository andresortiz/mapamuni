import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {LoginRoutingModule} from './login-routing.module';
import {LoginComponent} from './login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AccesoService} from '../shared/services/acceso.service';
import {GrowlModule} from 'primeng/growl';
import {MessageService} from 'primeng/components/common/messageservice';
import {AppModule} from '../app.module';
import {SharedModule} from '../shared/shared.module';

@NgModule({
    imports: [CommonModule, LoginRoutingModule, FormsModule,
        ReactiveFormsModule,
        GrowlModule
    ],
    declarations:
        [LoginComponent],
    providers: [AccesoService, MessageService]
})

export class LoginModule {
}
